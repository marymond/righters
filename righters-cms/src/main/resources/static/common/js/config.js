$(function() {
	moment.locale('en');

	// $.fn.dateRangePicker = function(option) {
	// 	var defaultOption = {
	// 		locale : {
	// 			format : 'YYYY-MM-DD'
	// 		},
	// 		autoUpdateInput : false
	// 	};
	//
	// 	$(this).daterangepicker($.extend(option, defaultOption));
	//
	// 	$(this).on('apply.daterangepicker', function(ev, picker) {
	// 		$(this).val(picker.startDate.format('YYYY-MM-DD') + ' ~ ' + picker.endDate.format('YYYY-MM-DD'));
	// 	});
	//
	// 	$(this).on('cancel.daterangepicker', function(ev, picker) {
	// 		$(this).val('');
	// 	});
	// }

	$.fn.pagination = function(totalElements, callback, pageSize) {
		if (!pageSize) {
			pageSize = window.pageable.pageSize;
		}

		if (!callback) {
			callback = function(page) {
				$.changeLocationParam({
					page : page
				});
			}
		}

		var init = false;
		var page = window.pageable.pageNumber + 1;

		this.paging(totalElements, {
			format : '[< nncnn >]',
			perpage : pageSize,
			page : page,
			onSelect : function(page) {
				if (!init) {
					init = true;
					return false;
				}

				callback(page - 1);
			},
			onFormat : function(type) {
				switch (type) {
				case 'block':
					return '<li class="paginate_button' + (page == this.value ? ' active' : '') + '"><a href="javascript:void(0);">' + this.value + '</a></li>';
				case 'prev':
					return '<li class="paginate_button previous"><a href="javascript:void(0);">Prev</a></li>';
				case 'next':
					return '<li class="paginate_button next"><a href="javascript:void(0);">Next</a></li>';
				case 'first':
					return '';
				case 'last':
					return '';
				}
			}
		})
	};

	$.deparam = function(params) {
		var digitTest = /^\d+$/, keyBreaker = /([^\[\]]+)|(\[\])/g, plus = /\+/g, paramTest = /([^?#]*)(#.*)?$/;

		if (!params || !paramTest.test(params)) {
			return {};
		}

		var data = {}, pairs = params.split('&'), current;

		for (var i = 0; i < pairs.length; i++) {
			current = data;
			var pair = pairs[i].split('=');

			if (pair.length != 2) {
				pair = [ pair[0], pair.slice(1).join('=') ]
			}

			var key = decodeURIComponent(pair[0].replace(plus, ' ')), value = decodeURIComponent(pair[1].replace(plus, ' ')), parts = key.match(keyBreaker);

			for (var j = 0; j < parts.length - 1; j++) {
				var part = parts[j];
				if (!current[part]) {
					current[part] = digitTest.test(parts[j + 1]) || parts[j + 1] == '[]' ? [] : {}
				}
				current = current[part];
			}
			lastPart = parts[parts.length - 1];
			if (lastPart == '[]') {
				current.push(value)
			} else {
				current[lastPart] = value;
			}
		}

		return data;
	};

	$.changeLocationParam = function(changeParam) {
		var param = $.deparam(window.location.search.substr(1));
		$.extend(param, changeParam);
		window.location.search = '?' + $.param(param);
	};

	// $.fn.inputImage = function() {
	// 	$.each($(this), function(index, value) {
	// 		var $this = $(value);
	//
	// 		var hidden = $('<input type="hidden">');
	// 		hidden.attr('name', $this.attr('name'));
	// 		hidden.val($this.attr('src'));
	//
	// 		var file = $('<input type="file">');
	// 		file.css({'display': 'none'});
	// 		file.change(function() {
	// 			var uploadForm = new FormData();
	// 			uploadForm.append('file', $(this)[0].files[0]);
	//
	// 			$('body').css('cursor', 'progress');
	// 			$.ajax({
	// 				url : '/file/upload',
	// 				data : uploadForm,
	// 				dataType : 'json',
	// 				processData : false,
	// 				contentType : false,
	// 				type : 'POST'
	// 			}).done(function(res) {
	// 				if (res.success) {
	// 					hidden.val(res.data);
	// 					$this.attr('src', res.data);
	// 				} else {
	// 					alert(res.message);
	// 				}
	// 			}).fail(function(jqXHR, textStatus) {
	// 				alert( "Request failed: " + textStatus );
	// 			});
	// 		});
	//
	// 		$this.addClass('image-input');
	// 		$this.after(hidden);
	// 		$this.after(file);
	// 		$this.click(function() {
	// 			file.click();
	// 		});
	// 	});
	// }
	//
	// $('.image-input').inputImage();
	//
	// // Modal 공통처리
	// $("#myModal").on("show.bs.modal", function(e) {
	// 	var obj = $(e.relatedTarget);
	//     $(this).find(".modal-body").load(obj.attr("href"));
	//     $(this).find(".modal-title").html(obj.attr("data-title"));
	//
	// });
	// $("#myModal").on('hidden.bs.modal', function () {
	// 	//console.log("hide!!!!");
	//     //$(this).find(".modal-body").empty();
	//     //$(this).removeData('modal');
	// });

	//Initialize Select2 Elements
	$('.select2').select2();
	//Money Euro
	$('[data-mask]').inputmask();

	//iCheck for checkbox and radio inputs
	$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
		checkboxClass: 'icheckbox_minimal-blue',
		radioClass   : 'iradio_minimal-blue'
	});
	//Red color scheme for iCheck
	$('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
		checkboxClass: 'icheckbox_minimal-red',
		radioClass   : 'iradio_minimal-red'
	});
	//Flat red color scheme for iCheck
	$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
		checkboxClass: 'icheckbox_flat-green',
		radioClass   : 'iradio_flat-green'
	});



	//VueJS mask -----start-----
	//console.clear();
	var tokens = {
			'#': {pattern: /\d/},
			'S': {pattern: /[a-zA-Z]/},
			'A': {pattern: /[0-9a-zA-Z]/},
			'U': {pattern: /[a-zA-Z]/, transform: v => v.toLocaleUpperCase()},
			'L': {pattern: /[a-zA-Z]/, transform: v => v.toLocaleLowerCase()}
	}
	function applyMask (value, mask, masked = true) {
		value = value || ""
		var iMask = 0
		var iValue = 0
		var output = ''
		while (iMask < mask.length && iValue < value.length) {
			cMask = mask[iMask]
			masker = tokens[cMask]
			cValue = value[iValue]
			if (masker) {
				if (masker.pattern.test(cValue)) {
					output += masker.transform ? masker.transform(cValue) : cValue
					iMask++
				}
				iValue++
			} else {
				if (masked) output += cMask
				if (cValue === cMask) iValue++
				iMask++
			}
		}
		return output
	}

	Vue.directive('mask', {
		bind (el, binding) {
			var value = el.value
			Object.defineProperty(el, 'value', {
				get: function(){
					return value;
				},
				set: function(newValue){
					console.log(newValue)
					el.setAttribute('value', newValue)
				},
				configurable: true
			});
		}
	})

	Vue.component('input-mask', {
		template: `<input v-model="maskedValue" :maxlength="mask.length" :placeholder="mask">`,
		props: {
			'value': String,
			'mask': String,
			'masked': {
				type: Boolean,
				default: false
			}
		},

		data: () => ({
		currentValue: '',
		currentMask: '',
		}),

		computed: {
			maskedValue: {
				get () {
					// fix removing mask character at the end.
					// Pressing backspace after 1.2.3 result in 1.2. instead of 1.2
					return this.value === this.currentValue ? this.currentMask
						: (this.currentMask = applyMask(this.value, this.mask, true))
				},

				set (newValue) {
					var currentPosition = this.$el.selectionEnd
					var lastMask = this.currentMask
					// update the input before restoring the cursor position
					this.$el.value = this.currentMask = applyMask(newValue, this.mask)

					if (this.currentMask.length <= lastMask.length) { // BACKSPACE
						// when chars are removed, the cursor position is already right
						this.$el.setSelectionRange(currentPosition, currentPosition)
					} else { // inserting characters
						// if the substring till the cursor position is the same, don't change position
						if (newValue.substring(0, currentPosition) == this.currentMask.substring(0, currentPosition)) {
							this.$el.setSelectionRange(currentPosition, currentPosition)
						} else { // increment 1 fixed position, but will not work if the mask has 2+ placeholders, like: ##//##
							this.$el.setSelectionRange(currentPosition+1, currentPosition+1)
						}
					}
					this.currentValue = applyMask(newValue, this.mask, this.masked)
					this.$emit('input', this.currentValue)
				}
			}
		}
	})
	//VueJS mask -----end-----

});


