{{<common/layout}}

{{$title}}기부처{{/title}}

{{$content}}

<section class="content-header">
    <h1>기부처 목록<small>기부처 설정</small></h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-user"></i> 기부 관리</li>
        <li class="active">기부처 목록</li>
    </ol>
</section>

<!-- Main content -->
<section id="vApp" class="content container-fluid">
    <!-- 검색 -->
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">검색조건</h3>
        </div>
        <form class="form-horizontal" id="msearch">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="status" class="col-sm-4 control-label">상태</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="status">
                                    <option></option>
                                    {{#statuses}}
                                        {{#isNotEq name 'DELETED'}}
                                            <option value="{{name}}" {{#isEq name search.status}}selected{{/isEq}}>{{desc}}</option>
                                        {{/isNotEq}}
                                    {{/statuses}}
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-info btn-block">검색</button>
            </div>
            <!-- /.box-footer -->
        </form>
    </div>
    <!-- 목록부분 -->
    <div class="box box-danger">
        <div class="box-header with-border">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="box-title">기부처 목록</h3>
                </div>
                <div class="col-md-6">
                    <button type="text" class="btn btn-warning pull-right" @click="getContent('new')">신규등록</button>
                </div>
            </div>
        </div>

        <div class="box-body table-responsive">
            <table class="table table-bordered table-striped dataTable">
                <thead>
                    <tr>
                        <th>기부처명</th>
                        <th>기부처이미지</th>
                        <th>이메일</th>
                        <th>전화번호</th>
                        <th>총적립금액(원)</th>
                        <th>지급남은금액(원)</th>
                        <th>상태</th>
                        <th>작성일</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                {{#contents}}
                <tr>
                    <td>{{name}}</td>
                    <td><img width="50px" height="50px" class="content-image" src="{{imageUrl}}" /></td>
                    <td>{{email}}</td>
                    <td>{{phoneNumber}}</td>
                    <td align="right">{{formatCurrency totalMileage}}</td>
                    <td align="right">{{formatCurrency balanceMileage}}</td>
                    <td>
                        <span class="label {{#isEq status.name 'HIDE'}}label-default{{/isEq}}{{#isEq status.name 'SHOW'}}label-primary{{/isEq}}">
                            {{status.desc}}
                        </span>
                    </td>
                    <td>{{formatDateTime registrationDate pattern="yyyy-MM-dd HH:mm:ss"}}</td>
                    <td>
                        <button type="text" class="btn-xs btn-success" @click="changeStatus('{{id}}')">{{status.btn}}</button>
                        <button type="text" class="btn-xs btn-success" @click="getContent('{{id}}')">수정</button>
                        <button type="text" class="btn-xs btn-success" @click="deleteContent('{{id}}')">삭제</button>
                    </td>
                </tr>
                {{/contents}}
                </tbody>
            </table>
        </div>

        <div class="box-footer">
            <div class="row">
                <div class="col-md-3">
                    <h3 class="box-title"><small>검색결과 총 </small>{{contents.totalElements}}<small> 건</small></h3>
                </div>
                <div class="col-md-9">
                    <div class="pagination-wrapper pull-right">
                        <ul class="pagination">
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- 현재페이지의 저장할 내용 모달 start-->
    <div id="modalContent" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-xlg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="gridSystemModalLabel">기부처 [[content.registrationDate != null ? '수정' : '등록']]</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" v-if="content.id != null">
                                <label for="id">ID</label>
                                <span class="form-control">[[content.id]]</span>
                            </div>
                            <div class="form-group">
                                <label for="loginId">기부처명</label>
                                <input type="text" v-model="content.name" class="form-control" placeholder="기부처명">
                            </div>
                            <div class="form-group" >
                                <label for="imageUrl">기부처이미지</label>
                                <input type="file" name="file" v-on:change="imageUpload"  id="uploadImageFile"/>
                            </div>
                            <div class="form-group">
                                <label>기부처이미지 미리보기</label>
                                <br/>
                                <img :src="content.imageUrl">
                            </div>
                            <div class="form-group">
                                <label for="email">이메일</label>
                                <input type="email" v-model="content.email" class="form-control" placeholder="이메일">
                            </div>
                            <div class="form-group">
                                <label for="mobileNumber">전화번호</label>
                                <input-mask v-model="content.phoneNumber" mask="###-####-####" placeholder="전화번호" class="form-control"></input-mask>
                            </div>
                            <div class="form-group">
                                <label for="status">상태</label>
                                <span class="form-control">
                                    {{#statuses}}
                                        {{#isNotEq name 'DELETED'}}
                                            <input type="radio" name="status" value="{{name}}" v-model="content.status">&nbsp;&nbsp;{{desc}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        {{/isNotEq}}
                                    {{/statuses}}
                                </span>
                            </div>
                            <div class="form-group" v-if="content.registrationDate != null">
                                <label for="mobileNumber">등록일</label>
                                <span class="form-control">[[moment(content.registrationDate).month(1).format('YYYY-MM-DD HH:mm')]]</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
                    <button type="button" class="btn btn-primary" @click.prevent="saveContent('[[content.id]]')">저장</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 현재페이지의 저장할 내용 모달 end-->
</section>
{{/content}}
{{$script}}
<script>

    var app = new Vue({
        el: '#vApp',
        data: {
            content : {}
        },
        methods : {
            getContent : function(id) {
                var self = this; // 현재 Vue객체 내부 접근을 위해
                self.content ={};
                axios.get('{{applicationHost}}/donation/id/' + id)
                        .then(function (response) {
                            if(response.data.content != null){
                                self.content = response.data.content;
                            }
                            $('#modalContent').modal('show');
                        })
                        .catch(function (error) {
                            console.log(JSON.stringify(error.response.data.message));
                            alert(error.response.data.message);
                        })
            },
            saveContent : function(id){
                axios.post('{{applicationHost}}/donation/save', this.content)
                        .then(function (response) {
                            alert("저장완료 되었습니다.");
                            $("#msearch").submit();
                        })
                        .catch(function (error) {
                            console.log(JSON.stringify(error.response.data.message));
                            alert(error.response.data.message);
                        })

            },
            changeStatus : function(id){
                if(!confirm('상태변경 하시겠습니까?')){
                    return false;
                }
                axios.post('{{applicationHost}}/donation/changeStatus/id/'+id)
                        .then(function (response) {
                            alert("상태변경 되었습니다.");
                            $("#msearch").submit();
                        })
                        .catch(function (error) {
                            console.log(JSON.stringify(error.response.data.message));
                            alert(error.response.data.message);
                        })

            },
            deleteContent : function(id){
                if(!confirm('삭제 하시겠습니까?')){
                    return false;
                }
                axios.post('{{applicationHost}}/donation/delete/id/'+ id)
                        .then(function (response) {
                            alert("삭제완료 되었습니다.");
                            $("#msearch").submit();
                        })
                        .catch(function (error) {
                            console.log(JSON.stringify(error.response.data.message));
                            alert(error.response.data.message);
                        })

            },
            imageUpload : function(){
                var self = this;
                const formData = new FormData();
                formData.append('file', $("#uploadImageFile")[0].files[0]);

                axios.post('{{applicationHost}}/fileUpload/donation/' + self.content.id, formData,{
                        headers : {
                            'Content-Type' : 'multipart/form-data'
                            }
                        })
                        .then(function (response) {
                            self.content.imageUrl = response.data.url;
                        })
                        .catch(function (error) {
                            //TODO 공용 모달변경 필요
                            alert(error.response.data.message);
                        })
            }
        }
    });

    $(function() {
        $('.pagination').pagination({{contents.totalElements}});
    });
</script>
{{/script}}
{{/common/layout}}