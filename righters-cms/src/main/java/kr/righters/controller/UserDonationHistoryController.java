package kr.righters.controller;

import kr.righters.controller.common.CommonController;
import kr.righters.controller.common.SecurityController;
import kr.righters.domain.entity.Donation;
import kr.righters.domain.entity.UserDonationHistory;
import kr.righters.domain.search.UserDonationHistorySearch;
import kr.righters.service.DonationService;
import kr.righters.service.UserDonationHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by yj.nam on 20.02.14..
 */

@RestController
@RequestMapping(value = "/donationHistory")
public class UserDonationHistoryController implements CommonController, SecurityController {

    @Autowired
    UserDonationHistoryService userDonationHistoryService;

    @Autowired
    DonationService donationService;

    @GetMapping("")
    public ModelAndView getContents(UserDonationHistorySearch search, @PageableDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable) {

        Page<UserDonationHistory> userDonationHistories = userDonationHistoryService.findBySearch(search, pageable);

        List<Donation> donations = donationService.findAll();

        ModelAndView modelAndView = new ModelAndView("donation/histories");
        modelAndView.addObject("contents",userDonationHistories);
        modelAndView.addObject("donations", donations);
        modelAndView.addObject("search",search);

        return modelAndView;
    }
}