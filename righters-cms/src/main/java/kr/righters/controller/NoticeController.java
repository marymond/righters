package kr.righters.controller;

import com.google.gson.GsonBuilder;
import kr.righters.controller.common.CommonController;
import kr.righters.controller.common.SecurityController;
import kr.righters.domain.common.StatusEnum;
import kr.righters.domain.entity.Inquiry;
import kr.righters.domain.entity.Manager;
import kr.righters.domain.entity.Notice;
import kr.righters.domain.search.ManagerSearch;
import kr.righters.domain.search.NoticeSearch;
import kr.righters.exception.CustomException;
import kr.righters.repository.ManagerRepository;
import kr.righters.repository.NoticeRepository;
import kr.righters.util.GsonAdapterFactory;
import kr.righters.util.PasswordEncoder;
import kr.righters.util.UniqueIdGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by yj.nam on 19.10.29..
 */

@RestController
@RequestMapping(value = "/notice")
public class NoticeController implements CommonController, SecurityController {

    @Autowired
    NoticeRepository noticeRepository;

    @Autowired
    UniqueIdGenerator uniqueIdGenerator;


    @GetMapping("")
    public ModelAndView getContents(NoticeSearch search, @PageableDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable) {

        Page<Notice> notices = noticeRepository.findAll(search.precidate(), pageable);

        ModelAndView modelAndView = new ModelAndView("notice/notices");
        modelAndView.addObject("statuses", Notice.Status.values());
        modelAndView.addObject("contents",notices);
        modelAndView.addObject("search",search);

        return modelAndView;
    }

    @GetMapping("/id/{id}")
    public @ResponseBody ModelMap getContent(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if (StringUtils.isEmpty(id)) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        Notice notice = noticeRepository.findOne(id);

        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(new GsonAdapterFactory());

        resultMap.addAttribute("content", notice);
        return resultMap;
    }

    @PostMapping("/save")
    public @ResponseBody ModelMap setContent(@ModelAttribute ModelMap resultMap, @RequestBody Notice noticeVO) {

        if(null== noticeVO || StringUtils.isEmpty(noticeVO.getTitle()) || StringUtils.isEmpty(noticeVO.getContent())) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        if(noticeVO.getId() == null){
            String id = uniqueIdGenerator.getStringId();
            noticeVO.setId(id);

        }else{
            Notice notice = noticeRepository.findOne(noticeVO.getId());
        }

        noticeRepository.save(noticeVO);

        return resultMap;
    }
}