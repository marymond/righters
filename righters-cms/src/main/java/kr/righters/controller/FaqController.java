package kr.righters.controller;

import com.google.gson.GsonBuilder;
import kr.righters.controller.common.CommonController;
import kr.righters.controller.common.SecurityController;
import kr.righters.domain.common.StatusEnum;
import kr.righters.domain.entity.Faq;
import kr.righters.domain.entity.Notice;
import kr.righters.domain.search.NoticeSearch;
import kr.righters.exception.CustomException;
import kr.righters.repository.FaqRepository;
import kr.righters.repository.NoticeRepository;
import kr.righters.util.GsonAdapterFactory;
import kr.righters.util.UniqueIdGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by yj.nam on 19.10.29..
 */

@RestController
@RequestMapping(value = "/faq")
public class FaqController implements CommonController, SecurityController {

    @Autowired
    FaqRepository faqRepository;

    @Autowired
    UniqueIdGenerator uniqueIdGenerator;


    @GetMapping("")
    public ModelAndView getContents(NoticeSearch search, @PageableDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable) {

        Page<Faq> faqs = faqRepository.findAll(search.precidate(), pageable);

        ModelAndView modelAndView = new ModelAndView("faq/faqs");
        modelAndView.addObject("statuses", Faq.Status.values());
        modelAndView.addObject("contents",faqs);
        modelAndView.addObject("search",search);

        return modelAndView;
    }

    @GetMapping("/id/{id}")
    public @ResponseBody ModelMap getContent(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if (StringUtils.isEmpty(id)) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        Faq faq = faqRepository.findOne(id);

        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(new GsonAdapterFactory());

        resultMap.addAttribute("content", faq);
        return resultMap;
    }

    @PostMapping("/save")
    public @ResponseBody ModelMap setContent(@ModelAttribute ModelMap resultMap, @RequestBody Faq faqVO) {

        if(null== faqVO || StringUtils.isEmpty(faqVO.getTitle()) || StringUtils.isEmpty(faqVO.getContent())) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        if(faqVO.getId() == null){
            String id = uniqueIdGenerator.getStringId();
            faqVO.setId(id);
        }else{
            Faq faq = faqRepository.findOne(faqVO.getId());
        }

        faqRepository.save(faqVO);

        return resultMap;
    }
}