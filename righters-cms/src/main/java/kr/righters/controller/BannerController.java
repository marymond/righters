package kr.righters.controller;

import kr.righters.controller.common.CommonController;
import kr.righters.controller.common.SecurityController;
import kr.righters.domain.common.StatusEnum;
import kr.righters.domain.entity.Banner;
import kr.righters.domain.search.BannerSearch;
import kr.righters.exception.CustomException;
import kr.righters.service.BannerService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by yj.nam on 20.1.02..
 */

@RestController
@RequestMapping(value = "/banner")
public class BannerController implements CommonController, SecurityController {

    @Autowired
    BannerService service;

    @GetMapping("")
    public ModelAndView getContents(BannerSearch search, @PageableDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable) {
        search.setNotStatus(Banner.Status.DELETED);
        Page<Banner> contents = service.findBySearch(search, pageable);

        ModelAndView modelAndView = new ModelAndView("banner/banners");
        modelAndView.addObject("contents",contents);
        modelAndView.addObject("statuses",Banner.Status.values());
        modelAndView.addObject("sites",Banner.Site.values());
        modelAndView.addObject("search",search);

        return modelAndView;
    }

    @GetMapping("/id/{id}")
    public @ResponseBody ModelMap getContent(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if (StringUtils.isEmpty(id)) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        Banner content = service.findOne(id);

        resultMap.addAttribute("content", content);

        return resultMap;
    }

    @PostMapping("/save")
    public @ResponseBody ModelMap setContent(@ModelAttribute ModelMap resultMap, @RequestBody Banner contentVO) {

        if(null== contentVO) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        service.save(contentVO);

        return resultMap;
    }

    @PostMapping("/changeStatus/id/{id}")
    public @ResponseBody ModelMap changeStatus(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if(null== id) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        Banner content = service.findOne(id);

        switch (content.getStatus()){
            case HIDE:
                content.setStatus(Banner.Status.SHOW);
                break;
            case SHOW:
                content.setStatus(Banner.Status.HIDE);
                break;
            default:
                break;
        }
        service.save(content);

        return resultMap;
    }

    @PostMapping("/delete/id/{id}")
    public @ResponseBody ModelMap deleteContent(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if(null== id) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }
        Banner content = service.findOne(id);
        content.setStatus(Banner.Status.DELETED);

        service.save(content);

        return resultMap;
    }
}