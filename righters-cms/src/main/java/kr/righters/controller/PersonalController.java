package kr.righters.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import kr.righters.controller.common.CommonController;
import kr.righters.controller.common.SecurityController;
import kr.righters.domain.common.StatusEnum;
import kr.righters.domain.entity.Manager;
import kr.righters.exception.CustomException;
import kr.righters.repository.ManagerRepository;
import kr.righters.util.GsonAdapterFactory;
import kr.righters.util.ObjectUtils;
import kr.righters.util.PasswordEncoder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by yjnam on 19.10.29..
 */

@RestController
@RequestMapping(value = "/personal")
public class PersonalController implements CommonController, SecurityController {

    @Autowired
    ManagerRepository managerRepository;

    @Autowired
    PasswordEncoder passwordEncoder;


    @GetMapping("/profile")
    public ModelAndView getProfile(@ModelAttribute Manager tokenManager) {

        Manager manager = managerRepository.findOne(tokenManager.getId());
        manager.setId(null); //id 숨김
        manager.setPassword(null); //패스워드 숨김

        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(new GsonAdapterFactory());
        Gson gson = builder.create();

        ModelAndView modelAndView = new ModelAndView("personal/profile");
        modelAndView.addObject("managerJson",gson.toJson(manager));

        return modelAndView;
    }

    @PostMapping("/profile")
    public @ResponseBody ModelMap setProfile(@ModelAttribute ModelMap resultMap,@ModelAttribute Manager tokenManager, @RequestBody Manager managerVO) {

        if(null== managerVO || StringUtils.isEmpty(managerVO.getName()) || StringUtils.isEmpty(managerVO.getNickname())) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);

        }

        Manager manager = managerRepository.findOne(tokenManager.getId());
        ObjectUtils.copyProperties(managerVO, manager);


        if(StringUtils.isNotEmpty(managerVO.getNewPassword()) && StringUtils.isNotEmpty(managerVO.getNewPasswordRetry())) {

            if (!StringUtils.equals(managerVO.getNewPassword(), managerVO.getNewPasswordRetry())) {
                throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
            }

            manager.setPassword(passwordEncoder.encodePassword(managerVO.getNewPassword()));
        }

        managerRepository.save(manager);

        return resultMap;
    }


}