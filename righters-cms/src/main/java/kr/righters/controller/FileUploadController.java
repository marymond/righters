package kr.righters.controller;

import kr.righters.util.FileUploader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class FileUploadController {
	@Autowired
	private FileUploader fileUploader;

	@RequestMapping(value = "fileUpload/{directory}/{id}", method = RequestMethod.POST)
	public @ResponseBody ModelMap upload(@ModelAttribute ModelMap resultMap, MultipartFile file, @PathVariable String directory, @PathVariable String id) {
		String url = fileUploader.upload(file, directory, id);

		resultMap.addAttribute("url", url);
		return resultMap;
	}
}
