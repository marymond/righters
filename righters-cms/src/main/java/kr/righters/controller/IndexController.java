package kr.righters.controller;

import kr.righters.controller.common.CommonController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by yj.nam on 19.10.29..
 */

@RestController
public class IndexController implements CommonController {

    @GetMapping("/")
    public ModelAndView getIndex() {

        ModelAndView modelAndView = new ModelAndView("index");

        return modelAndView;
    }
}