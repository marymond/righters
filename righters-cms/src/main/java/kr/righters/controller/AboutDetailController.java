package kr.righters.controller;

import kr.righters.controller.common.CommonController;
import kr.righters.controller.common.SecurityController;
import kr.righters.domain.common.StatusEnum;
import kr.righters.domain.entity.AboutCategory;
import kr.righters.domain.entity.AboutDetail;
import kr.righters.domain.search.AboutCategorySearch;
import kr.righters.domain.search.AboutDetailSearch;
import kr.righters.exception.CustomException;
import kr.righters.service.AboutCategoryService;
import kr.righters.service.AboutDetailService;
import kr.righters.service.ProductService;
import kr.righters.util.UniqueIdGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by yj.nam on 20.1.02..
 */

@RestController
@RequestMapping(value = "/about")
public class AboutDetailController implements CommonController, SecurityController {

    @Autowired
    UniqueIdGenerator uniqueIdGenerator;

    @Autowired
    AboutDetailService service;

    @Autowired
    AboutCategoryService aboutCategoryService;

    @GetMapping("")
    public ModelAndView getContents(AboutDetailSearch search, @PageableDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable) {
        search.setNotStatus(AboutDetail.Status.DELETED);
        Page<AboutDetail> contents = service.findBySearch(search, pageable);

        AboutCategorySearch aboutCategorySearch = new AboutCategorySearch();
        aboutCategorySearch.setRootFlag(true); //최상위 메뉴만 취득

        List<AboutCategory> categories = aboutCategoryService.findBySearch(aboutCategorySearch, new Sort(Sort.Direction.ASC, "sortNumber"));

        ModelAndView modelAndView = new ModelAndView("about/about");
        modelAndView.addObject("contents",contents);
        modelAndView.addObject("statuses", AboutDetail.Status.values());
        modelAndView.addObject("categories", categories);
        modelAndView.addObject("search",search);

        return modelAndView;
    }

    @GetMapping("/id/{id}")
    public @ResponseBody ModelMap getContent(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if (StringUtils.isEmpty(id)) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        AboutDetail content = service.findOne(id);

        resultMap.addAttribute("content", content);

        return resultMap;
    }

    @PostMapping("/save")
    public @ResponseBody ModelMap setContent(@ModelAttribute ModelMap resultMap, @RequestBody AboutDetail contentVO) {

        if(null== contentVO) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        service.save(contentVO);

        return resultMap;
    }

    @PostMapping("/changeStatus/id/{id}")
    public @ResponseBody ModelMap changeStatus(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if(null== id) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        AboutDetail content = service.findOne(id);

        switch (content.getStatus()){
            case HIDE:
                content.setStatus(AboutDetail.Status.SHOW);
                break;
            case SHOW:
                content.setStatus(AboutDetail.Status.HIDE);
                break;
            default:
                break;
        }
        service.save(content);

        return resultMap;
    }

    @PostMapping("/delete/id/{id}")
    public @ResponseBody ModelMap deleteContent(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if(null== id) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }
        AboutDetail content = service.findOne(id);
        content.setStatus(AboutDetail.Status.DELETED);

        service.save(content);

        return resultMap;
    }
}