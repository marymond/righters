package kr.righters.controller;

import kr.righters.controller.common.CommonController;
import kr.righters.controller.common.SecurityController;
import kr.righters.domain.common.StatusEnum;
import kr.righters.domain.entity.AboutCategory;
import kr.righters.domain.search.AboutCategorySearch;
import kr.righters.exception.CustomException;
import kr.righters.service.AboutCategoryService;
import kr.righters.util.UniqueIdGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by yj.nam on 20.10.14..
 */

@RestController
@RequestMapping(value = "/aboutCategory")
public class AboutCategoryController implements CommonController, SecurityController {

    @Autowired
    UniqueIdGenerator uniqueIdGenerator;

    @Autowired
    AboutCategoryService service;

    @GetMapping("")
    public ModelAndView getContents(AboutCategorySearch search, @PageableDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable, @RequestParam(value = "parentId", required = false) String parentId) {
        search.setNeStatus(AboutCategory.Status.DELETED); //삭제된 목록 제외
        if (StringUtils.isBlank(parentId)) search.setRootFlag(true);
        Page<AboutCategory> contents = service.findBySearch(search, pageable);

        ModelAndView modelAndView = new ModelAndView("about/categories");
        modelAndView.addObject("contents",contents);
        modelAndView.addObject("parent",service.findOne(parentId));
        modelAndView.addObject("statuses",AboutCategory.Status.values());
        modelAndView.addObject("types",AboutCategory.Type.values());
        modelAndView.addObject("childrenFlags",AboutCategory.ChildrenFlag.values());
        modelAndView.addObject("search",search);

        return modelAndView;
    }

    @GetMapping("/id/{id}")
    public @ResponseBody ModelMap getContent(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if (StringUtils.isEmpty(id)) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        AboutCategory content = service.findOne(id);

        resultMap.addAttribute("content", content);
        return resultMap;
    }

    @PostMapping("/save")
    public @ResponseBody ModelMap setContent(@ModelAttribute ModelMap resultMap, @RequestBody AboutCategory contentVO) {

        if(null== contentVO) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        if(contentVO.getId() == null){
            String id = uniqueIdGenerator.getStringId();
            contentVO.setId(id);
        }

        service.save(contentVO);

        return resultMap;
    }

    @PostMapping("/changeStatus/id/{id}")
    public @ResponseBody ModelMap changeStatus(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if(null== id) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        AboutCategory content = service.findOne(id);

        switch (content.getStatus()){
            case HIDE:
                content.setStatus(AboutCategory.Status.SHOW);
                break;
            case SHOW:
                content.setStatus(AboutCategory.Status.HIDE);
                break;
            default:
                break;
        }
        service.save(content);

        return resultMap;
    }

    @PostMapping("/delete/id/{id}")
    public @ResponseBody ModelMap deleteContent(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if(null== id) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }
        AboutCategory content = service.findOne(id);
        content.setStatus(AboutCategory.Status.DELETED);

        service.save(content);

        return resultMap;
    }

    @GetMapping("/parentId/{parentId}")
    public @ResponseBody ModelMap getSubCategories(@ModelAttribute ModelMap resultMap, @PathVariable String parentId) {

        if (StringUtils.isEmpty(parentId)) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }
        AboutCategorySearch aboutCategorySearch = new AboutCategorySearch();
        aboutCategorySearch.setStatus(AboutCategory.Status.SHOW);
        aboutCategorySearch.setParentId(parentId);
        List<AboutCategory> subOptionCategories = service.findBySearch(aboutCategorySearch, new Sort(Sort.Direction.ASC, "id"));

        resultMap.addAttribute("subOptionCategories", subOptionCategories);
        return resultMap;
    }
}