package kr.righters.controller;

import kr.righters.controller.common.CommonController;
import kr.righters.controller.common.SecurityController;
import kr.righters.domain.common.StatusEnum;
import kr.righters.domain.entity.Product;
import kr.righters.domain.entity.ProductCategory;
import kr.righters.domain.entity.Shipping;
import kr.righters.domain.search.ProductCategorySearch;
import kr.righters.domain.search.ProductSearch;
import kr.righters.domain.search.ShippingSearch;
import kr.righters.exception.CustomException;
import kr.righters.service.ProductCategoryService;
import kr.righters.service.ProductService;
import kr.righters.service.ShippingService;
import kr.righters.util.UniqueIdGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yj.nam on 20.1.02..
 */

@RestController
@RequestMapping(value = "/product")
public class ProductController implements CommonController, SecurityController {

    @Autowired
    UniqueIdGenerator uniqueIdGenerator;

    @Autowired
    ProductService service;
    @Autowired
    ProductCategoryService productCategoryService;
    @Autowired
    ShippingService shippingService;

    @GetMapping("")
    public ModelAndView getContents(ProductSearch search, @PageableDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable) {
        search.setNotStatus(Product.Status.DELETED);
        Page<Product> contents = service.findBySearch(search, pageable);

        ModelAndView modelAndView = new ModelAndView("product/products");
        modelAndView.addObject("contents",contents);
        modelAndView.addObject("dealTypes",Product.DealTypeFilter.values());
        modelAndView.addObject("brands",Product.BrandFilter.values());
        modelAndView.addObject("search",search);

        return modelAndView;
    }

    @GetMapping("/id/{id}")
    public @ResponseBody ModelMap getContent(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if (StringUtils.isEmpty(id)) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        Product content = service.findOne(id);

        ProductCategorySearch productCategorySearch = new ProductCategorySearch();
        productCategorySearch.setStatus(ProductCategory.Status.SHOW);

        List<ProductCategory> optionCategories = productCategoryService.findBySearch(productCategorySearch,new Sort(Sort.Direction.ASC, "id"));

        List<ProductCategory> subOptionCategories = new ArrayList<>();
        if(content != null && content.getRegistrationDate() != null ) {
            productCategorySearch.setParentId(content.getCategory().getId());
            subOptionCategories = productCategoryService.findBySearch(productCategorySearch, new Sort(Sort.Direction.ASC, "id"));
        }

        ShippingSearch shippingSearch = new ShippingSearch();
        shippingSearch.setStatus(Shipping.Status.SHOW);

        List<Shipping> shippings = shippingService.findBySearch(shippingSearch,new Sort(Sort.Direction.ASC, "id"));

        resultMap.addAttribute("content", content);
        resultMap.addAttribute("optionCategories", optionCategories);
        resultMap.addAttribute("subOptionCategories", subOptionCategories);
        resultMap.addAttribute("shippings", shippings);

        return resultMap;
    }

    @PostMapping("/save")
    public @ResponseBody ModelMap setContent(@ModelAttribute ModelMap resultMap, @RequestBody Product contentVO) {

        if(null== contentVO) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

//        if(contentVO.getId() == null){
//            String id = uniqueIdGenerator.getStringId();
//            contentVO.setId(id);
//        }

        service.save(contentVO);

        return resultMap;
    }

    @PostMapping("/changeStatus/id/{id}")
    public @ResponseBody ModelMap changeStatus(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if(null== id) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        Product content = service.findOne(id);

        switch (content.getStatus()){
            case HIDE:
                content.setStatus(Product.Status.SHOW);
                break;
            case SHOW:
                content.setStatus(Product.Status.HIDE);
                break;
            default:
                break;
        }
        service.save(content);

        return resultMap;
    }

    @PostMapping("/delete/id/{id}")
    public @ResponseBody ModelMap deleteContent(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if(null== id) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }
        Product content = service.findOne(id);
        content.setStatus(Product.Status.DELETED);

        service.save(content);

        return resultMap;
    }
}