package kr.righters.controller;

import kr.righters.controller.common.CommonController;
import kr.righters.controller.common.SecurityController;
import kr.righters.repository.ManagerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by yjnam on 19.10.29..
 */

@RestController
@RequestMapping(value = "/dashboard")
public class DashboardController implements CommonController, SecurityController {

    @Autowired
    ManagerRepository managerRepository;

    @GetMapping("")
    public ModelAndView getManager() {

        ModelAndView modelAndView = new ModelAndView("dashboard");
        modelAndView.addObject("message","안녕하세요!!!! ");

        return modelAndView;
    }


}