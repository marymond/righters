package kr.righters.controller;

import com.google.gson.GsonBuilder;
import kr.righters.controller.common.CommonController;
import kr.righters.controller.common.SecurityController;
import kr.righters.domain.common.StatusEnum;
import kr.righters.domain.entity.Ordered;
import kr.righters.domain.search.OrderedSearch;
import kr.righters.exception.CustomException;
import kr.righters.service.OrderedService;
import kr.righters.util.GsonAdapterFactory;
import kr.righters.util.UniqueIdGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by yj.nam on 19.10.29..
 */

@RestController
@RequestMapping(value = "/orderd")
public class OrderedController implements CommonController, SecurityController {

    @Autowired
    OrderedService orderedService;

    @Autowired
    UniqueIdGenerator uniqueIdGenerator;


    @GetMapping("")
    public ModelAndView getContents(OrderedSearch search, @PageableDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable) {

        Page<Ordered> ordereds = orderedService.findBySearch(search, pageable);
        orderedService.setUserInfo(ordereds.getContent());

        ModelAndView modelAndView = new ModelAndView("ordered/ordereds");
        modelAndView.addObject("statuses", Ordered.Status.values());
        modelAndView.addObject("contents",ordereds);
        modelAndView.addObject("search",search);

        return modelAndView;
    }

    @GetMapping("/id/{id}")
    public @ResponseBody ModelMap getContent(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if (StringUtils.isEmpty(id)) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        Ordered ordereds = orderedService.findOne(id);

        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(new GsonAdapterFactory());

        resultMap.addAttribute("content", ordereds);
        return resultMap;
    }

    @PostMapping("/save")
    public @ResponseBody ModelMap setContent(@ModelAttribute ModelMap resultMap, @RequestBody Ordered orderedVO) {

        if(null== orderedVO) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        if(orderedVO.getId() == null){
            String id = uniqueIdGenerator.getStringId();
            orderedVO.setId(id);
        }else{
            Ordered ordereds = orderedService.findOne(orderedVO.getId());
        }

        orderedService.save(orderedVO);

        return resultMap;
    }
}