package kr.righters.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import kr.righters.controller.common.CommonController;
import kr.righters.controller.common.SecurityController;
import kr.righters.domain.common.StatusEnum;
import kr.righters.domain.entity.Manager;
import kr.righters.domain.search.ManagerSearch;
import kr.righters.exception.CustomException;
import kr.righters.repository.ManagerRepository;
import kr.righters.util.GsonAdapterFactory;
import kr.righters.util.PasswordEncoder;
import kr.righters.util.UniqueIdGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by yj.nam on 19.10.29..
 */

@RestController
@RequestMapping(value = "/manager")
public class ManagerController implements CommonController, SecurityController {

    @Autowired
    ManagerRepository managerRepository;

    @Autowired
    UniqueIdGenerator uniqueIdGenerator;

    @Autowired
    PasswordEncoder passwordEncoder;

    @GetMapping("")
    public ModelAndView getContents(ManagerSearch search, @PageableDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable) {

        Page<Manager> managers = managerRepository.findAll(search.precidate(), pageable);

        ModelAndView modelAndView = new ModelAndView("manager/managers");
        modelAndView.addObject("contents",managers);
        modelAndView.addObject("search",search);

        return modelAndView;
    }

    @GetMapping("/id/{id}")
    public @ResponseBody ModelMap getContent(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if (StringUtils.isEmpty(id)) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        Manager manager = managerRepository.findOne(id);
        if(manager != null){
            manager.setPassword(null); //화면에서 패스워드숨김
        }

        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(new GsonAdapterFactory());
//        Gson gson = builder.create();
//        resultMap.addAttribute("content", gson.toJson(manager));

        resultMap.addAttribute("content", manager);
        return resultMap;
    }

    @PostMapping("/save")
    public @ResponseBody ModelMap setContent(@ModelAttribute ModelMap resultMap, @RequestBody Manager managerVO) {

        if(null== managerVO || StringUtils.isEmpty(managerVO.getName()) || StringUtils.isEmpty(managerVO.getNickname())) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        if(managerVO.getId() == null){
            String id = uniqueIdGenerator.getStringId();
            managerVO.setId(id);

            //TODO 차후 초기패스워드 랜덤생성하여 메일전송
            managerVO.setPassword(passwordEncoder.encodePassword("marymond"));

        }else{
            Manager manager = managerRepository.findOne(managerVO.getId());
            managerVO.setPassword(manager.getPassword()); // 기존패스워드 설정
        }

        managerRepository.save(managerVO);

        return resultMap;
    }
}