package kr.righters.controller;

import kr.righters.controller.common.CommonController;
import kr.righters.controller.common.SecurityController;
import kr.righters.domain.common.Flag;
import kr.righters.domain.common.StatusEnum;
import kr.righters.domain.entity.User;
import kr.righters.domain.search.UserSearch;
import kr.righters.exception.CustomException;
import kr.righters.repository.UserRepository;
import kr.righters.util.UniqueIdGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by yj.nam on 19.10.29..
 */

@RestController
@RequestMapping(value = "/user")
public class UserController implements CommonController, SecurityController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UniqueIdGenerator uniqueIdGenerator;


    @GetMapping("")
    public ModelAndView getContents(UserSearch search, @PageableDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable) {

        Page<User> contents = userRepository.findAll(search.precidate(), pageable);

        ModelAndView modelAndView = new ModelAndView("user/users");
        modelAndView.addObject("contents",contents);
        modelAndView.addObject("search",search);
        modelAndView.addObject("selfRewardFlags", Flag.values());

        return modelAndView;
    }

    @GetMapping("/id/{id}")
    public @ResponseBody ModelMap getContent(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if (StringUtils.isEmpty(id)) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        User content = userRepository.findOne(id);
        if(content != null){
            content.setPassword(null); //화면에서 패스워드숨김
        }

        resultMap.addAttribute("content", content);

        return resultMap;
    }

    @PostMapping("/save")
    public @ResponseBody ModelMap setContent(@ModelAttribute ModelMap resultMap, @RequestBody User userVO) {

        if(null== userVO ) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        if(userVO.getId() == null){
            String id = uniqueIdGenerator.getStringId();
            userVO.setId(id);
        }else{
            User user = userRepository.findOne(userVO.getId());
            userVO.setPassword(user.getPassword()); // 기존패스워드 설정
        }

        userRepository.save(userVO);

        return resultMap;
    }
}