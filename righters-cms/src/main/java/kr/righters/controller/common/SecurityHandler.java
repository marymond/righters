package kr.righters.controller.common;

import kr.righters.config.ApplicationProperty;
import kr.righters.domain.common.ManagerToken;
import kr.righters.domain.entity.Manager;
import kr.righters.exception.SecurityPermissionException;
import kr.righters.manager.MemcachedManager;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by yj.nam on 19.10.29..
 */
@ControllerAdvice(assignableTypes = {SecurityController.class})
public class SecurityHandler {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    ApplicationProperty applicationProperty;

    @Autowired
    MemcachedManager memcachedManager;


    @ModelAttribute
    public Manager manager(HttpServletRequest httpServletRequest, @CookieValue(value = "MTK", required = false) String token) {

        if (StringUtils.isEmpty(token)) {
            throw new SecurityException();
        }
        ManagerToken managerToken = memcachedManager.getManagerToken(token);

        if (null == managerToken) {
            throw new SecurityException();
        }

        // TODO : Update Cookie expiration and Memcached Expiration
        // TODO : 권한처리 필요
        if (!Manager.Level.ROLE_ADMIN.equals(managerToken.getManager().getLevel())) {
            String ar = managerToken.getManager().getAccessRights();
            if (StringUtils.isEmpty(ar)) {
                throw new SecurityPermissionException();
            }
            String[] accessRights = ar.split("\\|");

            String path = httpServletRequest.getRequestURI();
            boolean authority = false;
            for (String accessRight : accessRights) {
                if (path.startsWith(accessRight)) {
                    authority = true;
                    break;
                }
            }
            if(!authority) {
                throw new SecurityPermissionException();
            }


        }
        return managerToken.getManager();
    }


    @ExceptionHandler(value = SecurityException.class)
    public String securityException(SecurityException e, WebRequest request) {

        return "redirect:" + applicationProperty.getApplicationHost();
    }


    @ExceptionHandler(value = SecurityPermissionException.class)
    public String securityPermissionException(SecurityPermissionException e, WebRequest request) {

        return "exception/securityPermissionException";
    }




}
