package kr.righters.controller.common;

import kr.righters.config.ApplicationProperty;
import kr.righters.domain.common.StatusEnum;
import kr.righters.exception.CustomException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by yj.nam on 19.10.29..
 */
@Slf4j
@ControllerAdvice(assignableTypes = {CommonController.class })
public class CommonHandler {

    @Autowired
    ApplicationProperty applicationProperty;

    @ModelAttribute
    public void addAttributes(Model model) {

        model.addAttribute("environment", applicationProperty.getEnvironment());
        model.addAttribute("applicationDomain", applicationProperty.getApplicationDomain());
        model.addAttribute("applicationHost", applicationProperty.getApplicationHost());
        model.addAttribute("staticHost", applicationProperty.getStaticHost());
        model.addAttribute("apiHost", applicationProperty.getApiHost());
        model.addAttribute("wwwHost", applicationProperty.getWwwHost());
        model.addAttribute("cmsHost", applicationProperty.getCmsHost());
    }

    @ModelAttribute
    public ModelMap resultMap(HttpServletRequest httpServletRequest) {

        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("result", StatusEnum.SUCCESS);
        modelMap.addAttribute("message", StatusEnum.SUCCESS.getDescription());
        return modelMap;
    }


    @ExceptionHandler(value = CustomException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public
    @ResponseBody
    ModelMap customException(CustomException e, WebRequest request) {

        log.info("{}:{}", e.getResult(), e.getMessage());

        if(log.isDebugEnabled()) {
            for (StackTraceElement list : e.getStackTrace()) {
                if (list.getClassName().startsWith("kr.righters")) {
                    log.debug("- {}:{}:{}",list.getClassName(), list.getMethodName(), list.getLineNumber());
                    break;
                }
            }
        }

        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("result", e.getResult());
        modelMap.addAttribute("message", e.getMessage());

        return modelMap;

    }

}
