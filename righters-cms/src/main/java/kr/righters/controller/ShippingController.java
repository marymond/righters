package kr.righters.controller;

import kr.righters.controller.common.CommonController;
import kr.righters.controller.common.SecurityController;
import kr.righters.domain.common.Flag;
import kr.righters.domain.common.StatusEnum;
import kr.righters.domain.entity.Donation;
import kr.righters.domain.entity.Shipping;
import kr.righters.domain.search.ShippingSearch;
import kr.righters.exception.CustomException;
import kr.righters.service.ShippingService;
import kr.righters.util.UniqueIdGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by yj.nam on 20.1.02..
 */

@RestController
@RequestMapping(value = "/shipping")
public class ShippingController implements CommonController, SecurityController {

    @Autowired
    UniqueIdGenerator uniqueIdGenerator;

    @Autowired
    ShippingService service;

    @GetMapping("")
    public ModelAndView getContents(ShippingSearch search, @PageableDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable) {
        search.setNotStatus(Shipping.Status.DELETED);
        Page<Shipping> contents = service.findBySearch(search, pageable);

        ModelAndView modelAndView = new ModelAndView("shipping/shippings");
        modelAndView.addObject("contents",contents);
        modelAndView.addObject("statuses",Donation.Status.values());
        modelAndView.addObject("shipStationFlags", Flag.values());
        modelAndView.addObject("search",search);

        return modelAndView;
    }

    @GetMapping("/id/{id}")
    public @ResponseBody ModelMap getContent(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if (StringUtils.isEmpty(id)) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        Shipping content = service.findOne(id);

        resultMap.addAttribute("content", content);

        return resultMap;
    }

    @PostMapping("/save")
    public @ResponseBody ModelMap setContent(@ModelAttribute ModelMap resultMap, @RequestBody Shipping contentVO) {

        if(null== contentVO) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        if(contentVO.getId() == null){
            String id = uniqueIdGenerator.getStringId();
            contentVO.setId(id);
        }

        service.save(contentVO);

        return resultMap;
    }

    @PostMapping("/changeStatus/id/{id}")
    public @ResponseBody ModelMap changeStatus(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if(null== id) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        Shipping content = service.findOne(id);

        switch (content.getStatus()){
            case HIDE:
                content.setStatus(Shipping.Status.SHOW);
                break;
            case SHOW:
                content.setStatus(Shipping.Status.HIDE);
                break;
            default:
                break;
        }
        service.save(content);

        return resultMap;
    }

    @PostMapping("/delete/id/{id}")
    public @ResponseBody ModelMap deleteContent(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if(null== id) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }
        Shipping content = service.findOne(id);
        content.setStatus(Shipping.Status.DELETED);

        service.save(content);

        return resultMap;
    }
}