package kr.righters.controller;

import kr.righters.controller.common.CommonController;
import kr.righters.controller.common.SecurityController;
import kr.righters.domain.common.StatusEnum;
import kr.righters.domain.entity.Product;
import kr.righters.domain.entity.ProductTab;
import kr.righters.domain.search.ProductTabSearch;
import kr.righters.exception.CustomException;
import kr.righters.service.ProductService;
import kr.righters.service.ProductTabService;
import kr.righters.util.UniqueIdGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by yj.nam on 20.1.02..
 */

@RestController
@RequestMapping(value = "/product/tab")
public class ProductTabController implements CommonController, SecurityController {

    @Autowired
    UniqueIdGenerator uniqueIdGenerator;

    @Autowired
    ProductTabService service;

    @Autowired
    ProductService productService;

    @GetMapping("")
    public ModelAndView getContents(ProductTabSearch search, @PageableDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable, @RequestParam String productId) {
        search.setNotStatus(ProductTab.Status.DELETED);
        search.setProductId(productId);
        Page<ProductTab> contents = service.findBySearch(search, pageable);

        ModelAndView modelAndView = new ModelAndView("product/tabs");
        modelAndView.addObject("contents",contents);
        modelAndView.addObject("product",productService.findOne(productId));
        modelAndView.addObject("statuses", ProductTab.Status.values());
        modelAndView.addObject("search",search);

        return modelAndView;
    }

    @GetMapping("/id/{id}")
    public @ResponseBody ModelMap getContent(@ModelAttribute ModelMap resultMap, @PathVariable String id, @RequestParam String productId) {

        if (StringUtils.isEmpty(id)) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        ProductTab content = service.findOne(id);
        Product    product = productService.findOne(productId);
        content.setProductId(product.getId());

        resultMap.addAttribute("content", content);

        return resultMap;
    }

    @PostMapping("/save")
    public @ResponseBody ModelMap setContent(@ModelAttribute ModelMap resultMap, @RequestBody ProductTab contentVO) {

        if(null== contentVO) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        service.save(contentVO);

        return resultMap;
    }

    @PostMapping("/changeStatus/id/{id}")
    public @ResponseBody ModelMap changeStatus(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if(null== id) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        ProductTab content = service.findOne(id);

        switch (content.getStatus()){
            case HIDE:
                content.setStatus(ProductTab.Status.SHOW);
                break;
            case SHOW:
                content.setStatus(ProductTab.Status.HIDE);
                break;
            default:
                break;
        }
        service.save(content);

        return resultMap;
    }

    @PostMapping("/delete/id/{id}")
    public @ResponseBody ModelMap deleteContent(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if(null== id) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }
        ProductTab content = service.findOne(id);
        content.setStatus(ProductTab.Status.DELETED);

        service.save(content);

        return resultMap;
    }
}