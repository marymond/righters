package kr.righters.controller;

import kr.righters.controller.common.CommonController;
import kr.righters.controller.common.SecurityController;
import kr.righters.domain.common.StatusEnum;
import kr.righters.domain.entity.Product;
import kr.righters.domain.entity.ProductRelation;
import kr.righters.domain.search.ProductRelationSearch;
import kr.righters.domain.search.ProductSearch;
import kr.righters.exception.CustomException;
import kr.righters.service.ProductRelationService;
import kr.righters.service.ProductService;
import kr.righters.util.UniqueIdGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by yj.nam on 20.1.02..
 */

@RestController
@RequestMapping(value = "/product/relation")
public class ProductRelationController implements CommonController, SecurityController {

    @Autowired
    UniqueIdGenerator uniqueIdGenerator;

    @Autowired
    ProductRelationService service;

    @Autowired
    ProductService productService;


    @GetMapping("")
    public ModelAndView getContents(ProductRelationSearch search, @PageableDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable, @RequestParam String productId) {
        search.setNotStatus(ProductRelation.Status.DELETED);
        search.setProductId(productId);
        Page<ProductRelation> contents = service.findBySearch(search, pageable);

        ProductSearch productSearch = new ProductSearch();
        productSearch.setStatus(Product.Status.SHOW);

        List<Product> products = productService.findBySearch(productSearch, new Sort(Sort.Direction.ASC, "id"));

        ModelAndView modelAndView = new ModelAndView("product/relations");
        modelAndView.addObject("contents",contents);
        modelAndView.addObject("product",productService.findOne(productId));
        modelAndView.addObject("products",products);
        modelAndView.addObject("statuses",ProductRelation.Status.values());
        modelAndView.addObject("search",search);

        return modelAndView;
    }

    @GetMapping("/id/{id}")
    public @ResponseBody ModelMap getContent(@ModelAttribute ModelMap resultMap, @PathVariable String id, @RequestParam String productId) {

        if (StringUtils.isEmpty(id)) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        ProductRelation content = service.findOne(id);
        content.setProductId(productId);

        resultMap.addAttribute("content", content);

        return resultMap;
    }

    @PostMapping("/save")
    public @ResponseBody ModelMap setContent(@ModelAttribute ModelMap resultMap, @RequestBody ProductRelation contentVO) {

        if(null== contentVO) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        if(contentVO.getId() == null){
            String id = uniqueIdGenerator.getStringId();
            contentVO.setId(id);
        }

        service.save(contentVO);

        return resultMap;
    }

    @PostMapping("/changeStatus/id/{id}")
    public @ResponseBody ModelMap changeStatus(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if(null== id) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        ProductRelation content = service.findOne(id);

        switch (content.getStatus()){
            case HIDE:
                content.setStatus(ProductRelation.Status.SHOW);
                break;
            case SHOW:
                content.setStatus(ProductRelation.Status.HIDE);
                break;
            default:
                break;
        }
        service.save(content);

        return resultMap;
    }

    @PostMapping("/delete/id/{id}")
    public @ResponseBody ModelMap deleteContent(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if(null== id) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }
        ProductRelation content = service.findOne(id);
        content.setStatus(ProductRelation.Status.DELETED);

        service.save(content);

        return resultMap;
    }
}