package kr.righters.controller;

import com.google.gson.GsonBuilder;
import kr.righters.controller.common.CommonController;
import kr.righters.controller.common.SecurityController;
import kr.righters.domain.common.StatusEnum;
import kr.righters.domain.entity.Inquiry;
import kr.righters.domain.entity.Notice;
import kr.righters.domain.search.InquirySearch;
import kr.righters.domain.search.NoticeSearch;
import kr.righters.exception.CustomException;
import kr.righters.repository.InquiryRepository;
import kr.righters.repository.NoticeRepository;
import kr.righters.util.GsonAdapterFactory;
import kr.righters.util.UniqueIdGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by yj.nam on 19.10.29..
 */

@RestController
@RequestMapping(value = "/inquiry")
public class InquiryController implements CommonController, SecurityController {

    @Autowired
    InquiryRepository inquiryRepository;

    @Autowired
    UniqueIdGenerator uniqueIdGenerator;


    @GetMapping("")
    public ModelAndView getContents(InquirySearch search, @PageableDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable) {

        Page<Inquiry> inquirys = inquiryRepository.findAll(search.precidate(), pageable);

        ModelAndView modelAndView = new ModelAndView("inquiry/inquirys");
        modelAndView.addObject("statuses", Inquiry.Status.values());
        modelAndView.addObject("contents",inquirys);
        modelAndView.addObject("search",search);

        return modelAndView;
    }

    @GetMapping("/id/{id}")
    public @ResponseBody ModelMap getContent(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if (StringUtils.isEmpty(id)) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        Inquiry inquiry = inquiryRepository.findOne(id);

        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(new GsonAdapterFactory());

        resultMap.addAttribute("content", inquiry);
        return resultMap;
    }

    @PostMapping("/save")
    public @ResponseBody ModelMap setContent(@ModelAttribute ModelMap resultMap, @RequestBody Inquiry inquiryVO) {

        if(null== inquiryVO || StringUtils.isEmpty(inquiryVO.getContent())) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        if(inquiryVO.getId() == null){
            String id = uniqueIdGenerator.getStringId();
            inquiryVO.setId(id);
        }else{
            Inquiry inquiry = inquiryRepository.findOne(inquiryVO.getId());
        }

        inquiryRepository.save(inquiryVO);

        return resultMap;
    }
}