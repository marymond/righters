package kr.righters.controller;

import kr.righters.controller.common.CommonController;
import kr.righters.controller.common.SecurityController;
import kr.righters.domain.common.StatusEnum;
import kr.righters.domain.entity.ProductCategory;
import kr.righters.domain.search.ProductCategorySearch;
import kr.righters.exception.CustomException;
import kr.righters.service.ProductCategoryService;
import kr.righters.util.UniqueIdGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by yj.nam on 20.1.02..
 */

@RestController
@RequestMapping(value = "/category")
public class ProductCategoryController implements CommonController, SecurityController {

    @Autowired
    UniqueIdGenerator uniqueIdGenerator;

    @Autowired
    ProductCategoryService service;

    @GetMapping("")
    public ModelAndView getContents(ProductCategorySearch search, @PageableDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable, @RequestParam(value = "parentId", required = false) String parentId) {
        search.setNeStatus(ProductCategory.Status.DELETED); //삭제된 목록 제외
        search.setParentId(parentId);
        Page<ProductCategory> contents = service.findBySearch(search, pageable);

        ModelAndView modelAndView = new ModelAndView("product/categories");
        modelAndView.addObject("contents",contents);
        modelAndView.addObject("parent",service.findOne(parentId));
        modelAndView.addObject("statuses",ProductCategory.Status.values());
        modelAndView.addObject("search",search);

        return modelAndView;
    }

    @GetMapping("/id/{id}")
    public @ResponseBody ModelMap getContent(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if (StringUtils.isEmpty(id)) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        ProductCategory content = service.findOne(id);

        resultMap.addAttribute("content", content);
        return resultMap;
    }

    @PostMapping("/save")
    public @ResponseBody ModelMap setContent(@ModelAttribute ModelMap resultMap, @RequestBody ProductCategory contentVO) {

        if(null== contentVO) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        if(contentVO.getId() == null){
            String id = uniqueIdGenerator.getStringId();
            contentVO.setId(id);
        }

        service.save(contentVO);

        return resultMap;
    }

    @PostMapping("/changeStatus/id/{id}")
    public @ResponseBody ModelMap changeStatus(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if(null== id) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        ProductCategory content = service.findOne(id);

        switch (content.getStatus()){
            case HIDE:
                content.setStatus(ProductCategory.Status.SHOW);
                break;
            case SHOW:
                content.setStatus(ProductCategory.Status.HIDE);
                break;
            default:
                break;
        }
        service.save(content);

        return resultMap;
    }

    @PostMapping("/delete/id/{id}")
    public @ResponseBody ModelMap deleteContent(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if(null== id) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }
        ProductCategory content = service.findOne(id);
        content.setStatus(ProductCategory.Status.DELETED);

        service.save(content);

        return resultMap;
    }

    @GetMapping("/parentId/{parentId}")
    public @ResponseBody ModelMap getSubCategories(@ModelAttribute ModelMap resultMap, @PathVariable String parentId) {

        if (StringUtils.isEmpty(parentId)) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }
        ProductCategorySearch productCategorySearch = new ProductCategorySearch();
        productCategorySearch.setStatus(ProductCategory.Status.SHOW);
        productCategorySearch.setParentId(parentId);
        List<ProductCategory> subOptionCategories = service.findBySearch(productCategorySearch, new Sort(Sort.Direction.ASC, "id"));

        resultMap.addAttribute("subOptionCategories", subOptionCategories);
        return resultMap;
    }
}