package kr.righters.controller;

import kr.righters.controller.common.CommonController;
import kr.righters.controller.common.SecurityController;
import kr.righters.domain.common.StatusEnum;
import kr.righters.domain.entity.Donation;
import kr.righters.domain.search.DonationSearch;
import kr.righters.exception.CustomException;
import kr.righters.service.DonationService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by yj.nam on 20.1.02..
 */

@RestController
@RequestMapping(value = "/donation")
public class DonationController implements CommonController, SecurityController {

    @Autowired
    DonationService service;

    @GetMapping("")
    public ModelAndView getContents(DonationSearch search, @PageableDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable) {
        search.setNotStatus(Donation.Status.DELETED);
        Page<Donation> contents = service.findBySearch(search, pageable);

        ModelAndView modelAndView = new ModelAndView("donation/donations");
        modelAndView.addObject("contents",contents);
        modelAndView.addObject("statuses",Donation.Status.values());
        modelAndView.addObject("search",search);

        return modelAndView;
    }

    @GetMapping("/id/{id}")
    public @ResponseBody ModelMap getContent(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if (StringUtils.isEmpty(id)) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        Donation content = service.findOne(id);

        resultMap.addAttribute("content", content);

        return resultMap;
    }

    @PostMapping("/save")
    public @ResponseBody ModelMap setContent(@ModelAttribute ModelMap resultMap, @RequestBody Donation contentVO) {

        if(null== contentVO) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        service.save(contentVO);

        return resultMap;
    }

    @PostMapping("/changeStatus/id/{id}")
    public @ResponseBody ModelMap changeStatus(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if(null== id) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        Donation content = service.findOne(id);

        switch (content.getStatus()){
            case HIDE:
                content.setStatus(Donation.Status.SHOW);
                break;
            case SHOW:
                content.setStatus(Donation.Status.HIDE);
                break;
            default:
                break;
        }
        service.save(content);

        return resultMap;
    }

    @PostMapping("/delete/id/{id}")
    public @ResponseBody ModelMap deleteContent(@ModelAttribute ModelMap resultMap, @PathVariable String id) {

        if(null== id) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }
        Donation content = service.findOne(id);
        content.setStatus(Donation.Status.DELETED);

        service.save(content);

        return resultMap;
    }
}