package kr.righters.controller;


import kr.righters.config.ApplicationProperty;
import kr.righters.controller.common.CommonController;
import kr.righters.domain.common.CookieEnum;
import kr.righters.domain.common.ManagerToken;
import kr.righters.domain.entity.Manager;
import kr.righters.domain.search.ManagerSearch;
import kr.righters.exception.CustomException;
import kr.righters.manager.MemcachedManager;
import kr.righters.repository.ManagerRepository;
import kr.righters.util.PasswordEncoder;
import kr.righters.util.UniqueIdGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Created by yjnam on 19.10.29..
 */
@Slf4j
@Controller
@RequestMapping
public class SignController implements CommonController {

    private static final String REGEX_FOR_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    @Autowired
    ApplicationProperty applicationProperty;

    @Autowired
    ManagerRepository managerRepository;

    @Autowired
    UniqueIdGenerator uniqueIdGenerator;

    @Autowired
    MemcachedManager memcachedManager;

    @Autowired
    PasswordEncoder passwordEncoder;


    @PostMapping("/signin")
    public @ResponseBody ModelMap signin(HttpServletResponse httpServletResponse, @ModelAttribute ModelMap modelMap, @RequestBody ManagerSearch managerSearch) {


        if (!managerSearch.getLoginId().matches(REGEX_FOR_EMAIL)) {
            throw new CustomException("정보를 찾을수 없거나 비밀번호가 틀립니다.");
        }

        //관리자정보 취득
        Manager manager = (Manager) managerRepository.findOne(managerSearch.precidate());

        if (null == manager) {
            throw new CustomException("정보를 찾을수 없거나 비밀번호가 틀립니다.");
        }

        if(!passwordEncoder.isMatch(managerSearch.getPassword(), manager.getPassword())){
            throw new CustomException("정보를 찾을수 없거나 비밀번호가 틀립니다.");
        }

        String       token        = uniqueIdGenerator.getTokenId();
        ManagerToken managerToken = new ManagerToken();
        managerToken.setToken(token);
        managerToken.setManager(manager);

        memcachedManager.setManagerToken(managerToken);

        Cookie managerTokenCookie = new Cookie(CookieEnum.MANAGER_TOKEN.getCookieName(), token);
        managerTokenCookie.setMaxAge(CookieEnum.MANAGER_TOKEN.getExpiration());

        httpServletResponse.addCookie(managerTokenCookie);

        return modelMap;

    }

    @GetMapping("/signout")
    public String signout(HttpServletRequest request, HttpServletResponse httpServletResponse, @CookieValue(value = "MTK", required = true) String token, ModelMap modelMap) {

        memcachedManager.deleteManagerToken(token);
        Cookie managerTokenCookie = new Cookie(CookieEnum.MANAGER_TOKEN.getCookieName(), null);
        managerTokenCookie.setMaxAge(0);
        httpServletResponse.addCookie(managerTokenCookie);

        modelMap.clear();

        return "redirect:" + applicationProperty.getApplicationHost();

    }
}
