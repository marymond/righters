package kr.righters.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by yj.nam on 19.10.29..
 */
@Component
@Aspect
public class PageableAop {
	@Autowired
	private ObjectMapper objectMapper;

	@Around("execution(* kr.righters.*.*Controller.*(.., org.springframework.data.domain.Pageable, ..))")
	public Object pageble(ProceedingJoinPoint joinPoint) throws Throwable {
		Pageable pageable = getPageable(joinPoint);

		Object result = joinPoint.proceed(joinPoint.getArgs());

		if (pageable != null && result instanceof ModelAndView) {
			String pageableJson = objectMapper.writeValueAsString(pageable);

			ModelAndView modelAndView = (ModelAndView) result;
			modelAndView.addObject("_pageable", pageableJson);
		}

		return result;
	}

	private Pageable getPageable(ProceedingJoinPoint joinPoint) {
		for (Object object : joinPoint.getArgs()) {
			if (object instanceof Pageable) {
				return (Pageable) object;
			}
		}

		return null;
	}
}
