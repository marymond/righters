package kr.righters;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import kr.righters.util.PasswordEncoder;
import kr.righters.util.UniqueIdGenerator;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

/**
 * Created by yj.nam on 19. 11. 15..
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("localhost")
public class ApplicationTest {

    @Autowired
    UniqueIdGenerator uniqueIdGenerator;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Test
    public void idGeneratorTest(){
        String id = uniqueIdGenerator.getStringId();
    }

    @Test
    public void passwordTest(){
        String password = passwordEncoder.encodePassword("marymond1234");
    }

    @Test
    public void test(){

        //String[] a = {"20190102-20190106", "20190202-20190206","20190302-20190306","20190402-20190406"};
        List<String> a= Arrays.asList("20190102-20190106", "20190202-20190206","20190302-20190306","20190402-20190406");
        Gson   gson        = new GsonBuilder().create();
        String messageJson = gson.toJson(a);

        System.out.println(messageJson);

        List<String> b = gson.fromJson(messageJson, List.class);
        //["20190102-20190106","20190202-20190206","20190302-20190306","20190402-20190406"]
        //["20190102-20190106","20190202-20190206","20190302-20190306","20190402-20190406"]

    }

}
