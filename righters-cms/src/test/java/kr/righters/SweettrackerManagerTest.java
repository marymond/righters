package kr.righters;

import kr.righters.domain.common.Flag;
import kr.righters.domain.entity.Shipping;
import kr.righters.manager.sweettracker.SweettrackerManager;
import kr.righters.manager.sweettracker.vo.CompanyListResponseVO;
import kr.righters.repository.ShippingRepository;
import kr.righters.util.PasswordEncoder;
import kr.righters.util.UniqueIdGenerator;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by yj.nam on 20.09.11
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("localhost")
public class SweettrackerManagerTest {

    @Autowired
    UniqueIdGenerator uniqueIdGenerator;

    @Autowired
    SweettrackerManager sweettrackerManager;

    @Autowired
    ShippingRepository shippingRepository;

    @Test
    public void getCompanyList(){
        CompanyListResponseVO companyListResponseVO = sweettrackerManager.getCompanyList();
        for(CompanyListResponseVO.DeliveryCompanyInfo deliveryCompanyInfo : companyListResponseVO.getCompany()){
            Shipping shipping = new Shipping();
            shipping.setId(uniqueIdGenerator.getStringId());
            shipping.setName(deliveryCompanyInfo.getName());
            shipping.setSweettrackerCode(deliveryCompanyInfo.getCode());
            shipping.setShippingFee(0L);
            shipping.setShipStationFlag(Flag.FALSE);
            shipping.setStatus(Shipping.Status.HIDE);
            shippingRepository.save(shipping);
        }
    }

}
