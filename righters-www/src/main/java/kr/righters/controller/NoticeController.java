package kr.righters.controller;

import kr.righters.controller.common.CommonController;
import kr.righters.domain.common.UserToken;
import kr.righters.domain.entity.Faq;
import kr.righters.domain.entity.Notice;
import kr.righters.domain.entity.Ordered;
import kr.righters.domain.search.FaqSearch;
import kr.righters.domain.search.NoticeSearch;
import kr.righters.domain.search.OrderedSearch;
import kr.righters.repository.NoticeRepository;
import kr.righters.repository.OrderedRepository;
import kr.righters.service.FaqService;
import kr.righters.service.NoticeService;
import kr.righters.service.OrderedService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by yj.nam on 19.10.29..
 */

@Slf4j
@RestController
@RequestMapping(value = "/notice")
public class NoticeController implements CommonController {

    @Autowired
    NoticeService noticeService;

    @Autowired
    FaqService faqService;

    @Autowired
    NoticeRepository noticeRepository;

    @GetMapping("/")
    public ModelAndView getContent() {

        ModelAndView modelAndView = new ModelAndView("notice/notice");

        return modelAndView;
    }

    @GetMapping("/notice/list")
    public @ResponseBody
    ModelMap getNoticeList(@ModelAttribute ModelMap resultMap, @ModelAttribute UserToken userToken, @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC, size = 10) Pageable pageable) {
        NoticeSearch noticeSearch = new NoticeSearch();
        noticeSearch.setStatus(Notice.Status.SHOW);

        Page<Notice> notices = noticeService.findBySearch(noticeSearch, pageable);

        resultMap.addAttribute("notices",notices.getContent());
        resultMap.addAttribute("totalPages", notices.getTotalPages());

        return resultMap;
    }

    @GetMapping("/faq/list")
    public @ResponseBody
    ModelMap getFaqList(@ModelAttribute ModelMap resultMap, @ModelAttribute UserToken userToken, @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC, size = 10) Pageable pageable) {
        FaqSearch faqSearch = new FaqSearch();
        faqSearch.setStatus(Faq.Status.SHOW);

        Page<Faq> faqs = faqService.findBySearch(faqSearch, pageable);

        resultMap.addAttribute("faqs",faqs.getContent());
        resultMap.addAttribute("totalPages", faqs.getTotalPages());

        return resultMap;
    }
}