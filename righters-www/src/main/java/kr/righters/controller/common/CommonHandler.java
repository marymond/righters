package kr.righters.controller.common;

import kr.righters.config.ApplicationProperty;
import kr.righters.domain.common.StatusEnum;
import kr.righters.domain.common.UserToken;
import kr.righters.exception.CustomException;
import kr.righters.exception.CustomJsonException;
import kr.righters.manager.MemcachedManager;
import kr.righters.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by yj.nam on 19. 12. 05..
 */
@Slf4j
@ControllerAdvice(assignableTypes = {CommonController.class})
public class CommonHandler {

    @Autowired
    ApplicationProperty applicationProperty;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    MemcachedManager memcachedManager;

    @ModelAttribute
    public void addAttributes(Model model, HttpServletRequest httpServletRequest) {
        String path = httpServletRequest.getServletPath();
        boolean isMobileMenu = false;

        path = path.equals("/") ? "/home" : path;
        String[] pathes = path.split("/");

        if(path.equals("/") || pathes.length > 0) {
            model.addAttribute("menuId", pathes[1]);
            if(pathes.length < 3 && ("home".equals(pathes[1])||"shop".equals(pathes[1])||"impact".equals(pathes[1])||"mypage".equals(pathes[1]))){
                isMobileMenu = true;
            }
        }
        model.addAttribute("isMobileMenu", isMobileMenu);

        model.addAttribute("environment", applicationProperty.getEnvironment());
        model.addAttribute("applicationDomain", applicationProperty.getApplicationDomain());
        model.addAttribute("applicationHost", applicationProperty.getApplicationHost());
        model.addAttribute("staticHost", applicationProperty.getStaticHost());
        model.addAttribute("wwwHost", applicationProperty.getWwwHost());
        model.addAttribute("cmsHost", applicationProperty.getCmsHost());
//        model.addAttribute("imageHost", applicationProperty.getImageHost());
    }

    @ModelAttribute
    public UserToken userToken(@CookieValue(value = "UTK", required = false) String token) {

        if (StringUtils.isNotEmpty(token)) {
            UserToken userToken = memcachedManager.getUserToken(token);
            if (null != userToken) {
                return userToken;
            }
        }

        return null;
    }

    @ModelAttribute
    public ModelMap resultMap(HttpServletRequest httpServletRequest) {
        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("result", StatusEnum.SUCCESS);
        modelMap.addAttribute("message", StatusEnum.SUCCESS.getDescription());

        return modelMap;
    }

    @ExceptionHandler(value = CustomException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ModelAndView  customException(CustomException e, WebRequest request) {

        ModelAndView modelAndView  = new ModelAndView("/error/5xx");

        modelAndView.addObject("result", e.getResult());
        modelAndView.addObject("message", e.getMessage());

        log.info("{}:{}", e.getResult(), e.getMessage());
        if (log.isDebugEnabled()) {
            modelAndView.addObject("e1", ExceptionUtils.getStackTrace(e));
            modelAndView.addObject("e2", ExceptionUtils.getStackFrames(e));
            modelAndView.addObject("e3", ExceptionUtils.getRootCauseStackTrace(e));
            for (StackTraceElement list : e.getStackTrace()) {
                if (list.getClassName().startsWith("kr.righters")) {
                    log.debug("- {}:{}:{}", list.getClassName(), list.getMethodName(), list.getLineNumber());
                    break;
                }
            }
        }

        return modelAndView;
    }

    @ExceptionHandler(value = CustomJsonException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ModelMap customJsonException(CustomJsonException e, WebRequest request) {

        ModelMap modelMap = new ModelMap();

        modelMap.addAttribute("result", e.getResult());
        modelMap.addAttribute("message", e.getMessage());

        log.info("{}:{}", e.getResult(), e.getMessage());
        if (log.isDebugEnabled()) {
            for (StackTraceElement list : e.getStackTrace()) {
                if (list.getClassName().startsWith("kr.righters")) {
                    log.debug("- {}:{}:{}", list.getClassName(), list.getMethodName(), list.getLineNumber());
                    break;
                }
            }
        }

        return modelMap;

    }

    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ModelAndView exception(Exception e, WebRequest request) {

        e.printStackTrace();
        CustomException customException = new CustomException(StatusEnum.ERROR);
        return customException(customException, request);

    }
}
