package kr.righters.controller.common;

import kr.righters.config.ApplicationProperty;
import kr.righters.domain.common.UserToken;
import kr.righters.manager.MemcachedManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.context.request.WebRequest;

/**
 * Created by yj.nam on 19. 12. 04..
 */
@ControllerAdvice(assignableTypes = {SecurityController.class})
public class SecurityHandler {

    @Autowired
    ApplicationProperty applicationProperty;

    @Autowired
    MemcachedManager memcachedManager;


    @ModelAttribute
    public UserToken userToken(@ModelAttribute UserToken userToken) {
        if(null== userToken) {
            throw new SecurityException();
        }
        return userToken;
    }


    @ExceptionHandler(value = SecurityException.class)
    public String securityException(SecurityException e, WebRequest request) {
        return "redirect:" + applicationProperty.getApplicationHost() + "/user/signin";
    }


}
