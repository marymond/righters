package kr.righters.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import kr.righters.controller.common.CommonController;
import kr.righters.domain.common.StatusEnum;
import kr.righters.domain.common.UserToken;
import kr.righters.domain.entity.Inquiry;
import kr.righters.domain.entity.Notice;
import kr.righters.domain.entity.Ordered;
import kr.righters.domain.entity.User;
import kr.righters.domain.search.NoticeSearch;
import kr.righters.domain.search.OrderedSearch;
import kr.righters.exception.CustomException;
import kr.righters.repository.InquiryRepository;
import kr.righters.service.NoticeService;
import kr.righters.service.OrderedService;
import kr.righters.service.UserService;
import kr.righters.util.UniqueIdGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by yj.nam on 19.10.29..
 */



@RestController
public class HomeController implements CommonController {

    @Autowired
    UniqueIdGenerator uniqueIdGenerator;

    @Autowired
    InquiryRepository inquiryRepository;

    @Autowired
    UserService    userService;
    @Autowired
    OrderedService orderedService;

    @Autowired
    NoticeService noticeService;

    @GetMapping("/")
    public ModelAndView getContent(@ModelAttribute UserToken userToken) {

        ModelAndView modelAndView = new ModelAndView("home/home");

        if(userToken != null){
            User        user = userService.findOne(userToken.getUser().getId());

            GsonBuilder builder = new GsonBuilder();
            Gson            gson    = builder.create();

            OrderedSearch orderedSearch = new OrderedSearch();
            orderedSearch.setShareUserId(user.getId());

            List<Ordered> linkedPurchases      = orderedService.findBySearch(orderedSearch,new Sort(Sort.Direction.ASC, "id"));
            long          linkedPurchaseCent = 0L;
            for(Ordered ordered : linkedPurchases){
                BigDecimal shareRewardMileage = new BigDecimal(ordered.getShareUserRewardMileage());
                linkedPurchaseCent += shareRewardMileage.multiply(new BigDecimal(100L)).longValue();
            }

            BigDecimal linkedPurchaseAmountBig = (new BigDecimal(linkedPurchaseCent)).divide(new BigDecimal(100),2,BigDecimal.ROUND_DOWN);

            NoticeSearch noticeSearch = new NoticeSearch();
            noticeSearch.setStatus(Notice.Status.SHOW);

            Page<Notice> notices = noticeService.findBySearch(noticeSearch,new PageRequest(0, 2, Sort.Direction.DESC, "id"));

            modelAndView.addObject("user", gson.toJson(user));
            modelAndView.addObject("linkedPurchaseAmount", linkedPurchaseAmountBig.toPlainString());
            modelAndView.addObject("notices", notices.getContent());
        }



        return modelAndView;
    }

    @PostMapping("/inquiry/save")
    public @ResponseBody
    ModelMap setContent(@ModelAttribute ModelMap resultMap, @RequestBody Inquiry inquiryVO) {

        if(null== inquiryVO || StringUtils.isEmpty(inquiryVO.getFullName())
                || StringUtils.isEmpty(inquiryVO.getContent()) || StringUtils.isEmpty(inquiryVO.getEmail())) {
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        if(inquiryVO.getId() == null){
            String id = uniqueIdGenerator.getStringId();
            inquiryVO.setId(id);
        }else{
            Inquiry inquiry = inquiryRepository.findOne(inquiryVO.getId());
        }

        inquiryRepository.save(inquiryVO);

        return resultMap;
    }
}

