package kr.righters.controller;

import kr.righters.config.ApplicationProperty;
import kr.righters.controller.common.CommonController;
import kr.righters.controller.common.SecurityController;
import kr.righters.domain.common.CookieEnum;
import kr.righters.domain.entity.UserSigninHistory;
import kr.righters.manager.MemcachedManager;
import kr.righters.repository.UserSigninHistoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by yj.nam on 19.12.17..
 */

@Slf4j
@RestController
@RequestMapping(value = "/user")
public class UserController implements CommonController, SecurityController {
    @Autowired
    ApplicationProperty applicationProperty;

    @Autowired
    MemcachedManager memcachedManager;

    @Autowired
    UserSigninHistoryRepository userSigninHistoryRepository;

    /**
     * 로그아웃 처리
     * @param request
     * @param httpServletResponse
     * @param token
     * @param session
     * @param modelMap
     * @return
     */
    @GetMapping("/signout")
    public ModelAndView getSignout(HttpServletRequest request, HttpServletResponse httpServletResponse, @CookieValue(value = "UTK", required = true) String token, HttpSession session, ModelMap modelMap) {

        memcachedManager.deleteUserToken(token);

        UserSigninHistory userToken = userSigninHistoryRepository.findByToken(token);
        if (null != userToken) {
            userToken.setTokenStatus(UserSigninHistory.TokenStatus.SIGNOUT);
            userSigninHistoryRepository.save(userToken);
        }

        Cookie userTokenCookie = new Cookie(CookieEnum.USER_TOKEN.getCookieName(), null);
        userTokenCookie.setPath("/");
        userTokenCookie.setMaxAge(0);

        httpServletResponse.addCookie(userTokenCookie);
        modelMap.clear();

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setView(new RedirectView(applicationProperty.getApplicationHost(),true));

        return modelAndView;
    }
}