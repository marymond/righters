package kr.righters.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import kr.righters.config.ApplicationProperty;
import kr.righters.controller.common.CommonController;
import kr.righters.controller.common.SecurityController;
import kr.righters.domain.common.StatusEnum;
import kr.righters.domain.common.UserToken;
import kr.righters.domain.entity.*;
import kr.righters.domain.search.*;
import kr.righters.exception.CustomException;
import kr.righters.exception.CustomJsonException;
import kr.righters.manager.MemcachedManager;
import kr.righters.manager.StripeManager;
import kr.righters.manager.iamport.IamportManager;
import kr.righters.manager.iamport.vo.PaymentsCancelResponseVO;
import kr.righters.manager.iamport.vo.common.IamportResponse;
import kr.righters.manager.paypal.PaypalManager;
import kr.righters.repository.OrderedRepository;
import kr.righters.service.*;
import kr.righters.util.FileUploader;
import kr.righters.util.UniqueIdGenerator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by yj.nam on 19.12.17..
 */

@Slf4j
@RestController
@RequestMapping(value = "/mypage")
public class MypageController implements CommonController, SecurityController {

    private static final String REGEX_FOR_DOLLAR = "^\\d+(?:[.]?[\\d]?[\\d])?$";

    @Autowired
    ApplicationProperty applicationProperty;

    @Autowired
    MemcachedManager memcachedManager;

    @Autowired
    UserService userService;

    @Autowired
    UserAddressService userAddressService;

    @Autowired
    OrderedService orderedService;

    @Autowired
    OrderedRepository orderedRepository;

    @Autowired
    UserWishlistService userWishlistService;

    @Autowired
    private FileUploader fileUploader;

    @Autowired
    UniqueIdGenerator uniqueIdGenerator;

    @Autowired
    DonationService donationService;

    @Autowired
    UserDonationHistoryService userDonationHistoryService;

    @Autowired
    StripeManager stripeManager;

    @Autowired
    PaypalManager paypalManager;

    @Autowired
    IamportManager iamportManager;

    @GetMapping("/")
    public ModelAndView getContent(@ModelAttribute UserToken userToken) {

        User content = userService.findOne(userToken.getUser().getId());

        if(content != null){
            content.setPassword(null); //화면에서 패스워드숨김
            content.setRegistrationDate(null); //가입일 숨김
        }

        GsonBuilder  builder      = new GsonBuilder();
        Gson         gson         = builder.create();

        OrderedSearch orderedSearch = new OrderedSearch();
        orderedSearch.setShareUserId(content.getId());

        List<Ordered> linkedPurchases      = orderedService.findBySearch(orderedSearch,new Sort(Sort.Direction.ASC, "id"));
        long          linkedPurchaseCent = 0L;
        for(Ordered ordered : linkedPurchases){
            BigDecimal shareRewardMileage = new BigDecimal(ordered.getShareUserRewardMileage());
            linkedPurchaseCent += shareRewardMileage.multiply(new BigDecimal(100L)).longValue();
        }

        BigDecimal linkedPurchaseAmountBig = (new BigDecimal(linkedPurchaseCent)).divide(new BigDecimal(100),2,BigDecimal.ROUND_DOWN);

        ModelAndView modelAndView = new ModelAndView("mypage/mypage");
        modelAndView.addObject("content", gson.toJson(content));
        modelAndView.addObject("linkedPurchaseAmount", "$"+linkedPurchaseAmountBig.toPlainString());

        return modelAndView;
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public @ResponseBody ModelMap upload(@ModelAttribute ModelMap resultMap, @ModelAttribute UserToken userToken, @RequestBody MultipartFile file) {
        User user = userService.findOne(userToken.getUser().getId());

        String url = fileUploader.upload(file, "thumbnail", user.getId());

        user.setProfileImageUrl(url);

        userService.save(user);

        resultMap.addAttribute("profileImageUrl", url);
        return resultMap;
    }

    @PostMapping("/save")
    public @ResponseBody ModelMap setContent(@ModelAttribute ModelMap resultMap, @RequestBody User userVO) {

        User user = userService.findOne(userVO.getId());

        //화면에서 받은 유저정보는 이메일과 풀네임만 변경가능함
        user.setFullName(userVO.getFullName());
        user.setEmail(userVO.getEmail());

        userService.save(user);

        return resultMap;
    }


    @GetMapping("/orders")
    public ModelAndView getOrders(@ModelAttribute UserToken userToken) {
         ModelAndView modelAndView = new ModelAndView("mypage/orders");

        return modelAndView;
    }
    @GetMapping("/orders/success")
    public @ResponseBody ModelMap getOrdersList(@ModelAttribute ModelMap resultMap, @ModelAttribute UserToken userToken
            , @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC, size = 5) Pageable pageable) {
        OrderedSearch orderedSearch = new OrderedSearch();
        orderedSearch.setUserId(userToken.getUser().getId());
        orderedSearch.setStatuses(Arrays.asList(Ordered.Status.SUCCESS_PAYMENT, Ordered.Status.COMPLETE));

        Page<Ordered> contents = orderedService.findBySearch(orderedSearch, pageable);

        resultMap.addAttribute("contents",contents.getContent());
        resultMap.addAttribute("totalPages", contents.getTotalPages());

        return resultMap;
    }

    @GetMapping("/orders/cancel")
    public @ResponseBody ModelMap getOrdersCancelList(@ModelAttribute ModelMap resultMap, @ModelAttribute UserToken userToken
            , @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC, size = 5) Pageable pageable) {
        OrderedSearch orderedSearch = new OrderedSearch();
        orderedSearch.setUserId(userToken.getUser().getId());
        orderedSearch.setStatuses(Arrays.asList(Ordered.Status.REFUNDED));

        Page<Ordered> contents = orderedService.findBySearch(orderedSearch, pageable);

        resultMap.addAttribute("contents",contents.getContent());
        resultMap.addAttribute("totalPages", contents.getTotalPages());

        return resultMap;
    }


    @GetMapping("/order/detail/{orderId}")
    public ModelAndView getOrderDetail(@ModelAttribute UserToken userToken,@PathVariable String orderId) {

        Ordered ordered = orderedService.findOne(orderId);

        for(OrderedProduct orderedProduct : ordered.getOrderedProducts()){
            //주소변경 및 환불가능여부 판단(배송전단계인 Reviewing 일때만 가능)
            if(OrderedProduct.Status.REFUNDED.equals(orderedProduct.getStatus()) || !OrderedProduct.ShippingStatus.REVIEWING.equals(orderedProduct.getShippingStatus())){
                ordered.setUpdatableFlag(false);
            }
        }

        ModelAndView modelAndView = new ModelAndView("mypage/orderDetail");
        modelAndView.addObject("ordered", ordered);
        modelAndView.addObject("shippingStatuses", OrderedProduct.ShippingStatus.values());
        modelAndView.addObject("itemCnt", ordered.getOrderedProducts().size());

        return modelAndView;
    }


    @PostMapping("/order/detail/address/{orderId}")
    public @ResponseBody ModelMap setUpdateAddress(@ModelAttribute ModelMap resultMap, @RequestBody UserAddress userAddressVO, @PathVariable String orderId) {
        Ordered ordered = orderedService.findOne(orderId);
        ordered.setBuyerFullName(userAddressVO.getFullName());
        ordered.setBuyerAddress1(userAddressVO.getAddress1());
        ordered.setBuyerAddress2(userAddressVO.getAddress2());
        ordered.setBuyerCity(userAddressVO.getCity());
        ordered.setBuyerState(userAddressVO.getState());
        ordered.setBuyerZipCode(userAddressVO.getZipCode());
        ordered.setBuyerPhoneNumber(userAddressVO.getPhoneNumber());
        ordered.setBuyerDeliveryInstructions(userAddressVO.getDeliveryInstructions());

        orderedService.save(ordered);

        return resultMap;
    }

    @PostMapping("/order/detail/refund/{orderId}")
    public @ResponseBody ModelMap setRefund(@ModelAttribute ModelMap resultMap, @PathVariable String orderId) {
        Ordered ordered = orderedService.findOne(orderId);
        switch (ordered.getPaymentType()){
            case STRIPE:
                stripeManager.refund(ordered.getPgReceiptId(), String.valueOf(ordered.getTotalAmount()));
                break;
            case PAYPAL:
                paypalManager.refund(ordered.getPgReceiptId(), String.valueOf(ordered.getTotalAmount()));
                break;
            case IAMPORT:

                IamportResponse<PaymentsCancelResponseVO> paymentsCancelResponseVO = null;
                try {
                    paymentsCancelResponseVO = iamportManager.postPaymentsCancel(ordered.getPgReceiptId(), "변심으로 인한 결제취소");
                    log.info("{}", paymentsCancelResponseVO);
                }catch (Exception e){
                    log.error(String.format("Iamport Capture Order Error"));
                    e.printStackTrace();
                    break;
                }

                if(paymentsCancelResponseVO != null && paymentsCancelResponseVO.getCode() == 0){
                    //결제취소처리가 잘되었을경우
                    orderedService.refund(ordered.getPgOrderId());
                }

                break;
        }
        return resultMap;
    }



    @GetMapping("/linked")
    public ModelAndView getLinked() {

        ModelAndView modelAndView = new ModelAndView("mypage/linked");

        return modelAndView;
    }
    @GetMapping("/linked/list")
    public @ResponseBody ModelMap getLinkedList(@ModelAttribute ModelMap resultMap, @ModelAttribute UserToken userToken, @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC, size = 10) Pageable pageable) {
        OrderedSearch orderedSearch = new OrderedSearch();
        orderedSearch.setShareUserId(userToken.getUser().getId());
        orderedSearch.setStatuses(Arrays.asList(Ordered.Status.SUCCESS_PAYMENT,Ordered.Status.COMPLETE));

        Page<Ordered> linkedPurchases = orderedService.findBySearch(orderedSearch, pageable);

        resultMap.addAttribute("linkedPurchases",linkedPurchases.getContent());
        resultMap.addAttribute("totalPages", linkedPurchases.getTotalPages());

        return resultMap;
    }

    @GetMapping("/wishlist")
    public ModelAndView getWishlist() {

        ModelAndView modelAndView = new ModelAndView("mypage/wishlist");

        return modelAndView;
    }
    @GetMapping("/wishlist/list")
    public @ResponseBody ModelMap getwhishList(@ModelAttribute ModelMap resultMap, @ModelAttribute UserToken userToken, @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC, size = 8) Pageable pageable) {

        UserWishlistSearch userWishlistSearch = new UserWishlistSearch();
        userWishlistSearch.setUserId(userToken.getUser().getId());

        Page<UserWishlist> wishList = userWishlistService.findBySearch(userWishlistSearch, pageable);
        List<UserWishlist> returnWishList = new ArrayList<>();
        for(UserWishlist userWishlist : wishList.getContent()){
           if(Product.Status.SHOW.equals(userWishlist.getProduct().getStatus())){
               returnWishList.add(userWishlist);
           }
        }
        resultMap.addAttribute("wishList",returnWishList);
        resultMap.addAttribute("totalPages", wishList.getTotalPages());

        return resultMap;
    }

    @PostMapping("/wishlist/delete/{productId}")
    @ResponseBody
    public ModelMap deleteWishlist(@ModelAttribute ModelMap modelMap, @ModelAttribute UserToken userToken, @PathVariable String productId) {

        if(productId !=null) {
            UserWishlistSearch search = new UserWishlistSearch();
            search.setProductId(productId);
            search.setUserId(userToken.getUser().getId());
            List<UserWishlist> wishlist = userWishlistService.findBySearch(search, new Sort(Sort.Direction.ASC, "id"));
            if(wishlist.size()==0){
                throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
            }
            else{
                for(int i=0;i<wishlist.size();i++) {
                    userWishlistService.delete(wishlist.get(i));
                }
            }
        }else{
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        return modelMap;
    }

    @GetMapping("/address")
    public ModelAndView getAddress(@ModelAttribute UserToken userToken, @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC, size = 8) Pageable pageable) {

        ModelAndView modelAndView = new ModelAndView("mypage/address");
        return modelAndView;
    }

    @GetMapping("/address/list")
    public @ResponseBody ModelMap getAddressList(@ModelAttribute ModelMap resultMap, @ModelAttribute UserToken userToken, @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC, size = 6) Pageable pageable) {
        UserAddressSearch userAddressSearch = new UserAddressSearch();
        userAddressSearch.setUserId(userToken.getUser().getId());

        Page<UserAddress> userAddressList = userAddressService.findBySearch(userAddressSearch, pageable);
        resultMap.addAttribute("address", userAddressList.getContent());
        resultMap.addAttribute("totalPages", userAddressList.getTotalPages());

        return resultMap;
    }


    @PostMapping("/address/save")
    public @ResponseBody ModelMap setContent(@ModelAttribute ModelMap resultMap, @ModelAttribute UserToken userToken, @RequestBody UserAddress userAddressVO) {

        //유효성 체크
        userAddressService.check(userAddressVO);

        if(userAddressVO.getId() == null){
            String id = uniqueIdGenerator.getStringId();
            userAddressVO.setId(id);
            userAddressVO.setUserId(userToken.getUser().getId());

        }else{
            UserAddress userAddress = userAddressService.findOne(userAddressVO.getId());
        }

        userAddressService.save(userAddressVO);

        return resultMap;
    }

    @PostMapping("/address/delete/{id}")
    @ResponseBody
    public ModelMap changeContent(@ModelAttribute ModelMap resultMap, @ModelAttribute UserToken userToken, @PathVariable String id) {
        UserAddressSearch userAddressSearch = new UserAddressSearch();
        userAddressSearch.setId(id);
        UserAddress userAddress = userAddressService.findOne(id);

        if(userAddress!=null){
            userAddressService.delete(userAddress);
        }
        else{
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        return resultMap;
    }

    @PostMapping("/address/default/{id}")
    public @ResponseBody ModelMap setDefaultAddress(@ModelAttribute ModelMap resultMap, @ModelAttribute UserToken userToken, @PathVariable String id) {

        UserAddressSearch userAddressSearch = new UserAddressSearch();
        userAddressSearch.setDefaultFlag(UserAddress.Flag.TRUE);
        userAddressSearch.setUserId(userToken.getUser().getId());
        List<UserAddress> userAddresses = userAddressService.findBySearch(userAddressSearch, new Sort(Sort.Direction.ASC, "id"));

        for(int i=0;i<userAddresses.size();i++){
            userAddresses.get(i).setDefaultFlag(UserAddress.Flag.FALSE);
            userAddressService.save(userAddresses.get(i));
        }


        UserAddress saveAddress = userAddressService.findOne(id);
        saveAddress.setDefaultFlag(UserAddress.Flag.TRUE);

        if(saveAddress != null) {
            userAddressService.save(saveAddress);
        }
        else{
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        return resultMap;
    }



    @GetMapping("/donation")
    public ModelAndView getDonation(@ModelAttribute UserToken userToken) {

        DonationSearch donationSearch = new DonationSearch();
        donationSearch.setStatus(Donation.Status.SHOW);

        List<Donation> donations = donationService.findBySearch(donationSearch, new Sort(Sort.Direction.ASC, "id"));
        User user = userService.findOne(userToken.getUser().getId());

        ModelAndView modelAndView = new ModelAndView("mypage/donation");
        modelAndView.addObject("donations",donations);
        modelAndView.addObject("balanceMileage", user.getBalanceMileage());


        return modelAndView;
    }

    @GetMapping("/donation/list")
    public @ResponseBody ModelMap getDonationHistoyList(@ModelAttribute ModelMap resultMap, @ModelAttribute UserToken userToken, @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC, size = 8) Pageable pageable) {
        UserDonationHistorySearch userDonationHistorySearch = new UserDonationHistorySearch();
        userDonationHistorySearch.setUserId(userToken.getUser().getId());

        Page<UserDonationHistory> donationHistories = userDonationHistoryService.findBySearch(userDonationHistorySearch, pageable);

        resultMap.addAttribute("donationHistories", donationHistories.getContent());
        resultMap.addAttribute("totalPages", donationHistories.getTotalPages());

        return resultMap;
    }

    @PostMapping("/donation/submit")
    public @ResponseBody ModelMap donation(@ModelAttribute ModelMap resultMap, @ModelAttribute UserToken userToken, @RequestBody UserDonationHistory donation) {
        //donationFee 체크
        //null,공백문자체크(스크립트 장난으로 들어오는 사람방지)
        if(StringUtils.isBlank(donation.getDonationId())){
            throw new CustomJsonException("Please choice the donation place.");
        }
        if(donation.getAmount() == null){
            throw new CustomJsonException("Please input the donation fee.");
        }
        //기부금 숫자확인(소수점두자리까지 가능)
//        if (!donation.getAmount().matches(REGEX_FOR_DOLLAR)) {
//            throw new CustomJsonException("Donation fee can be up to cents.(2 decimal places)");
//        }

        User user = userService.findOne(userToken.getUser().getId());
        Donation donationMaster = donationService.findOne(donation.getDonationId());

        //잔액보다 적은지 체크(db락걸린상태에서 체크)
        BigDecimal userBalance = new BigDecimal(user.getBalanceMileage());
        BigDecimal userUseMileage = new BigDecimal(donation.getAmount());
        if(userBalance.compareTo(userUseMileage) == -1){
            throw new CustomJsonException("You don't have enough reserve balance.");
        }

        donationService.saveDonation(user, donationMaster, donation);

        return resultMap;
    }
}