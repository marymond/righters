package kr.righters.controller;

import kr.righters.controller.common.CommonController;
import kr.righters.domain.common.CookieEnum;
import kr.righters.domain.common.StatusEnum;
import kr.righters.domain.common.UserToken;
import kr.righters.domain.entity.Product;
import kr.righters.domain.entity.ProductCategory;
import kr.righters.domain.entity.UserWishlist;
import kr.righters.domain.search.ProductCategorySearch;
import kr.righters.domain.search.ProductSearch;
import kr.righters.domain.search.UserWishlistSearch;
import kr.righters.exception.CustomException;
import kr.righters.manager.MemcachedManager;
import kr.righters.service.ProductCategoryService;
import kr.righters.service.ProductService;
import kr.righters.service.UserWishlistService;
import kr.righters.util.UniqueIdGenerator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;


/**
 * Created by yj.nam on 19.10.29..
 */

@Slf4j
@RestController
@RequestMapping(value = "/shop")
public class ShopController implements CommonController {

    @Autowired
    ProductService productService;

    @Autowired
    ProductCategoryService productCategoryService;

    @Autowired
    UserWishlistService userWishlistService;

    @Autowired
    UniqueIdGenerator uniqueIdGenerator;

    @Autowired
    MemcachedManager memcachedManager;

    @GetMapping("/")
    public ModelAndView getContents(String categoryId, String subCategoryId, ProductSearch productSearch) {

        ProductCategorySearch productCategorySearch = new ProductCategorySearch();
        productCategorySearch.setStatus(ProductCategory.Status.SHOW);

        List<ProductCategory> categories = productCategoryService.findBySearch(productCategorySearch,new Sort(Sort.Direction.ASC, "id"));
        List<ProductCategory> subCategories = new ArrayList<>();
        if(StringUtils.isNotBlank(categoryId)){
            productCategorySearch.setParentId(categoryId);
            subCategories = productCategoryService.findBySearch(productCategorySearch,new Sort(Sort.Direction.ASC, "id"));
        }

        ModelAndView          modelAndView     = new ModelAndView("shop/shop");
        modelAndView.addObject("categoryId",categoryId);
        modelAndView.addObject("subCategoryId",subCategoryId);
        modelAndView.addObject("search",productSearch);
        modelAndView.addObject("categories",categories);
        modelAndView.addObject("subCategories",subCategories);
        modelAndView.addObject("dealTypeFilters",Product.DealTypeFilter.values());
        modelAndView.addObject("brandFilters",Product.BrandFilter.values());
        modelAndView.addObject("priceFilters",Product.PriceFilter.values());

        return modelAndView;
    }



    @GetMapping("/list")
    public @ResponseBody
    ModelMap getLinkedList(@ModelAttribute ModelMap resultMap, @ModelAttribute UserToken userToken
            , @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC, size = 8) Pageable pageable
            , String categoryId, String subCategoryId, ProductSearch productSearch) {

        productSearch.setStatus(Product.Status.SHOW);
        if(StringUtils.isNotBlank(categoryId)) productSearch.setCategory(productCategoryService.findOne(categoryId));
        if(StringUtils.isNotBlank(subCategoryId)) productSearch.setSubCategory(productCategoryService.findOne(subCategoryId));

        Page<Product> products = productService.findBySearch(productSearch, pageable);

        if(userToken != null) {
            //로그인 했을 경우 위시리스트 정보 연동
            UserWishlistSearch search = new UserWishlistSearch();
            search.setUserId(userToken.getUser().getId());
            List<UserWishlist> wishlist = userWishlistService.findBySearch(search, new Sort(Sort.Direction.ASC, "id"));

            /* 해당 유저의 모든 wishlist 불러옴 -> 해당 productId의 wishlist 정보를 가지고 있는지 검사해서 있으면 true, 없으면 false */

            for(int i=0; i<products.getContent().size(); i++){
                List<UserWishlist> sublist = wishlist.stream().filter(this.isInWishlist(products.getContent().get(i).getId())).collect(Collectors.toList());
                if(sublist.size()==0){
                    products.getContent().get(i).setWishListFlag(false);
                }
                else{
                    products.getContent().get(i).setWishListFlag(true);
                }
            }
        }
        else{
            // 로그인 안했을 경우 위시리스트 정보 모두 false 대입
            for(int i=0; i<products.getContent().size(); i++){
                products.getContent().get(i).setWishListFlag(false);
            }
        }

        resultMap.addAttribute("products", products.getContent());
        resultMap.addAttribute("totalPages", products.getTotalPages());


        return resultMap;
    }

    private Predicate<UserWishlist> isInWishlist(String productId) {
        return p -> p.getProductId().equals(productId);
    }



    @GetMapping("/category")
    ModelMap getCategoryList(@ModelAttribute ModelMap resultMap, @ModelAttribute UserToken userToken) {

        ProductCategorySearch productCategorySearch = new ProductCategorySearch();
        productCategorySearch.setStatus(ProductCategory.Status.SHOW);

        List<ProductCategory> categories = productCategoryService.findBySearch(productCategorySearch,new Sort(Sort.Direction.ASC, "id"));

        resultMap.addAttribute("categories", categories);

        return resultMap;
    }

    @GetMapping("/id/{id}")
    public ModelAndView getContent(HttpServletResponse response, @PathVariable String id, @RequestParam(required = false) String uid) {
        Product product = productService.findOne(id);
        //공유한 유저ID 쿠키설정
        if(uid != null) {
            Cookie shareCookie = new Cookie(product.getId(), uid);
            shareCookie.setMaxAge(CookieEnum.SHARE_PRODUT.getExpiration()); //쿠키 유효시간
            shareCookie.setPath("/");
            response.addCookie(shareCookie);
        }

        ModelAndView modelAndView = new ModelAndView("shop/shopContent");
        modelAndView.addObject("content", product);

        return modelAndView;
    }

    @PostMapping("/wishlist/change/{productId}")
    @ResponseBody
    public ModelMap changeContent(@ModelAttribute ModelMap modelMap, @ModelAttribute UserToken userToken, @PathVariable String productId) {

        if(productId !=null) {
            UserWishlistSearch search = new UserWishlistSearch();
            search.setProductId(productId);
            search.setUserId(userToken.getUser().getId());
            List<UserWishlist> wishlist = userWishlistService.findBySearch(search, new Sort(Sort.Direction.ASC, "id"));
            if(wishlist.size()==0){
                String id = uniqueIdGenerator.getStringId();
                UserWishlist wishlistVO = new UserWishlist();
                wishlistVO.setId(id);
                wishlistVO.setProductId(productId);
                wishlistVO.setUserId(userToken.getUser().getId());
                userWishlistService.save(wishlistVO);
            }
            else{
                for(int i=0;i<wishlist.size();i++) {
                    userWishlistService.delete(wishlist.get(i));
                }
            }
        }else{
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        return modelMap;
    }
}