package kr.righters.controller;

import kr.righters.controller.common.CommonController;
import kr.righters.controller.vo.CheckOutRequestVO;
import kr.righters.controller.vo.IamportCheckoutResponseVO;
import kr.righters.domain.common.UserToken;
import kr.righters.domain.entity.Cart;
import kr.righters.domain.entity.Product;
import kr.righters.domain.entity.ProductOption;
import kr.righters.domain.search.CartSearch;
import kr.righters.manager.StripeManager;
import kr.righters.service.CartService;
import kr.righters.service.OrderedService;
import kr.righters.service.ProductService;
import kr.righters.service.UserAddressService;
import kr.righters.util.UniqueIdGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yj.nam on 20.09.15..
 */
@Slf4j
@RestController
@RequestMapping(value="/iamport")
public class IamportController implements CommonController {
    @Autowired
    CartService cartService;

    @Autowired
    ProductService productService;

    @Autowired
    OrderedService orderedService;

    @Autowired
    UserAddressService userAddressService;

    @Autowired
    UniqueIdGenerator uniqueIdGenerator;

    //callback, webhook의 경우
    @PostMapping("/result")
    public ModelMap result(@ModelAttribute ModelMap modelMap, @RequestBody IamportCheckoutResponseVO iamportCheckoutResponseVO){

        boolean result = this.resultProcess(iamportCheckoutResponseVO.getImp_uid(), iamportCheckoutResponseVO.getMerchant_uid());
        modelMap.addAttribute("processResult", result);

        return modelMap;
    }

    //m_edirect_url로 접근한 경우
    @GetMapping("/result")
    public ModelAndView result(@RequestParam("imp_uid") String impUid, @RequestParam("merchant_uid") String merchantUid){

        boolean result = this.resultProcess(impUid, merchantUid);
        String page = "cart/success";
        if(!result){
            page = "cart/canceled";
        }
        ModelAndView          modelAndView     = new ModelAndView(page);

        return modelAndView;
    }

    /**
     * 결제 취소한 경우
     * @param modelMap
     * @param iamportCheckoutResponseVO
     * @return
     */
    @PostMapping("/checkOutFail")
    public ModelMap checkOutFail(@ModelAttribute ModelMap modelMap, @RequestBody IamportCheckoutResponseVO iamportCheckoutResponseVO){

        orderedService.checkOutFail(iamportCheckoutResponseVO.getMerchant_uid());

        return modelMap;
    }

    /**
     * 결제 완료후 화면이동
     * @param result
     * @return
     */
    @GetMapping("/result/view")
    public ModelAndView resultView(@RequestParam Boolean result){

        String page = "cart/success";
        if(!result){
            page = "cart/canceled";
        }
        ModelAndView          modelAndView     = new ModelAndView(page);

        return modelAndView;
    }

    /**
     * 결제 처리 공통
     * @param impUid
     * @param merchantUid
     * @return
     */
    private boolean resultProcess(String impUid, String merchantUid){
        //결제내역 검증
        boolean resultflag = orderedService.verifyCheckOut(merchantUid, impUid);

        if(resultflag){
            //결제성공처리
            orderedService.checkOutComplete(merchantUid, impUid);
        }

        return resultflag;
    }

    @ResponseBody
    @PostMapping("/create")
    public ModelMap createSession(HttpServletRequest httpServletRequest, @ModelAttribute ModelMap modelMap, @ModelAttribute UserToken userToken, @RequestBody CheckOutRequestVO checkOutRequestVO) {
        //유효성 체크
        //shipping address
        userAddressService.check(checkOutRequestVO.getShippingAddress());

        Cart cartVO = checkOutRequestVO.getCart();

        List<Cart> carts = null;
        if(cartVO.getProductId() == null) {
            //유저 카트정보 취득
            CartSearch cartSearch = new CartSearch();
            cartSearch.setStatus(Cart.Status.BEFORE_PAYMENT);
            cartSearch.setUserId(userToken.getUser().getId());
            carts = cartService.findBySearch(cartSearch, new Sort(Sort.Direction.DESC, "id"));
        }else{
            carts = new ArrayList<>();
            carts.add(cartVO);
        }

        for(Cart cart : carts) {
            Product       product       = productService.findOne(cart.getProductId());
            for (ProductOption option : product.getProductOptions()) {
                if (cart.getProductOptionId().equals(option.getId())) {
                    cart.setProductOption(option);
                }
            }
            cart.setProduct(product);

            //다른유저에 의해 공유된것인지 쿠키확인
            Cookie shareUserCookie = WebUtils.getCookie(httpServletRequest, product.getId());
            if(shareUserCookie != null){
                product.setShareUid(shareUserCookie.getValue());
            }
        }

        String merchantUid = orderedService.createMerchanUid(carts, checkOutRequestVO.getShippingAddress(), userToken.getUser(), cartVO.getProductId() == null ? false: true);
        modelMap.addAttribute("merchantUid", merchantUid);
        modelMap.addAttribute("buyerEmail", userToken.getUser().getEmail());
        modelMap.addAttribute("buyerName", userToken.getUser().getFullName());

        return modelMap;
    }

}

