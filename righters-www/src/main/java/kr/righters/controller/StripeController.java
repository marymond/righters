package kr.righters.controller;

import kr.righters.controller.common.CommonController;
import kr.righters.controller.vo.CheckOutRequestVO;
import kr.righters.domain.common.UserToken;
import kr.righters.domain.entity.Cart;
import kr.righters.domain.entity.Product;
import kr.righters.domain.entity.ProductOption;
import kr.righters.domain.search.CartSearch;
import kr.righters.manager.StripeManager;
import kr.righters.service.CartService;
import kr.righters.service.OrderedService;
import kr.righters.service.ProductService;
import kr.righters.service.UserAddressService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yj.nam on 20.01.31..
 */
@Slf4j
@RestController
public class StripeController implements CommonController {
    @Autowired
    StripeManager stripeManager;

    @Autowired
    CartService cartService;

    @Autowired
    ProductService productService;

    @Autowired
    OrderedService orderedService;

    @Autowired
    UserAddressService userAddressService;

    //결제 성공
    @GetMapping("/stripe/success")
    public ModelAndView success(@RequestParam("session_id") String sessionId){
        //결제성공처리
        String paymentIntent = stripeManager.getPaymentIntent(sessionId);
        orderedService.checkOutComplete(sessionId, paymentIntent);

        ModelAndView          modelAndView     = new ModelAndView("cart/success");
        return modelAndView;
    }

    //결제 취소(스트라이프 내부에서 취소처리했을경우)
    @GetMapping("/canceled")
    public ModelAndView canceled(@RequestParam("session_id") String sessionId){

        //주문장 결제 실패처리
        orderedService.checkOutFail(sessionId);

        ModelAndView          modelAndView     = new ModelAndView("cart/canceled");
        return modelAndView;
    }

    @ResponseBody
    @PostMapping("/create-session")
    public ModelMap createSession(HttpServletRequest httpServletRequest, @ModelAttribute ModelMap modelMap, @ModelAttribute UserToken userToken, @RequestBody CheckOutRequestVO checkOutRequestVO) {
        //유효성 체크
        //shipping address
        userAddressService.check(checkOutRequestVO.getShippingAddress());

        Cart cartVO = checkOutRequestVO.getCart();

        List<Cart> carts = null;
        if(cartVO.getProductId() == null) {
            //유저 카트정보 취득
            CartSearch cartSearch = new CartSearch();
            cartSearch.setStatus(Cart.Status.BEFORE_PAYMENT);
            cartSearch.setUserId(userToken.getUser().getId());
            carts = cartService.findBySearch(cartSearch, new Sort(Sort.Direction.DESC, "id"));
        }else{
            carts = new ArrayList<>();
            carts.add(cartVO);
        }

        for(Cart cart : carts) {
            Product       product       = productService.findOne(cart.getProductId());
            for (ProductOption option : product.getProductOptions()) {
                if (cart.getProductOptionId().equals(option.getId())) {
                    cart.setProductOption(option);
                }
            }
            cart.setProduct(product);

            //다른유저에 의해 공유된것인지 쿠키확인
            Cookie shareUserCookie = WebUtils.getCookie(httpServletRequest, product.getId());
            if(shareUserCookie != null){
                product.setShareUid(shareUserCookie.getValue());
            }
        }

        String sessionId = stripeManager.createSession(carts, checkOutRequestVO.getShippingAddress(), userToken.getUser(), cartVO.getProductId() == null ? false: true);
        modelMap.addAttribute("sessionId", sessionId);

        return modelMap;
    }

}

