package kr.righters.controller;

import kr.righters.controller.common.CommonController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by yj.nam on 19.10.29..
 */

@Slf4j
@RestController
@RequestMapping(value = "/impact")
public class ImpactController implements CommonController {

    @GetMapping("/")
    public ModelAndView getContent() {

        ModelAndView modelAndView = new ModelAndView("impact/impact");

        return modelAndView;
    }
}