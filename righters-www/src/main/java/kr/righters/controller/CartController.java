package kr.righters.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import kr.righters.controller.common.CommonController;
import kr.righters.controller.common.SecurityController;
import kr.righters.controller.vo.ShippingProductResponseVO;
import kr.righters.controller.vo.ShippingResponseVO;
import kr.righters.domain.common.StatusEnum;
import kr.righters.domain.common.UserToken;
import kr.righters.domain.entity.*;
import kr.righters.domain.search.CartSearch;
import kr.righters.domain.search.UserAddressSearch;
import kr.righters.domain.search.UserWishlistSearch;
import kr.righters.exception.CustomException;
import kr.righters.manager.StripeManager;
import kr.righters.manager.paypal.PaypalClient;
import kr.righters.service.*;
import kr.righters.util.UniqueIdGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by yj.nam on 19.12.17..
 */

@Slf4j
@RestController
@RequestMapping(value = "/cart")
public class CartController implements CommonController, SecurityController {

    @Autowired
    UserService userService;

    @Autowired
    CartService cartService;

    @Autowired
    ProductService productService;

    @Autowired
    ProductOptionService productOptionService;

    @Autowired
    UniqueIdGenerator uniqueIdGenerator;

    @Autowired
    UserWishlistService userWishlistService;

    @Autowired
    UserAddressService userAddressService;

    @Autowired
    StripeManager stripeManager;

    @Autowired
    PaypalClient paypalClient;

    @GetMapping("/")
    public ModelAndView getContent() {


        ModelAndView modelAndView = new ModelAndView("cart/cart");

        return modelAndView;
    }

    @GetMapping("/list")
    public @ResponseBody
    ModelMap getCartList(@ModelAttribute ModelMap modelMap, @ModelAttribute UserToken userToken) {
        CartSearch cartSearch = new CartSearch();
        cartSearch.setStatus(Cart.Status.BEFORE_PAYMENT);
        cartSearch.setUserId(userToken.getUser().getId());

        List<Cart> carts = cartService.findBySearch(cartSearch, new Sort(Sort.Direction.DESC, "id"));
        for(Cart cart : carts){
            Product product = productService.findOne(cart.getProductId());
            ProductOption productOption = null;
            for(ProductOption option : product.getProductOptions()){
                if(cart.getProductOptionId().equals(option.getId())){
                    productOption = option;
                }
            }

            cart.setProduct(product);
            cart.setProductOption(productOption);
        }

        modelMap.addAttribute("carts",carts);
        modelMap.addAttribute("itemCount",carts.size());

        return modelMap;


    }

    @PostMapping("/add")
    @ResponseBody
    public ModelMap addContent(@ModelAttribute ModelMap modelMap, @ModelAttribute UserToken userToken, @RequestBody Cart cart) {
        CartSearch cartSearch = new CartSearch();
        cartSearch.setStatus(Cart.Status.BEFORE_PAYMENT);
        cartSearch.setUserId(userToken.getUser().getId());
        cartSearch.setProductId(cart.getProductId());
        cartSearch.setProductOptionId(cart.getProductOptionId());

        List<Cart> carts = cartService.findBySearch(cartSearch,new Sort(Sort.Direction.DESC, "id"));
        if(carts.size() > 0){
            int quantity = cart.getQuantity();

            cart = carts.get(0);
            cart.setQuantity( quantity + cart.getQuantity());
        }else{
            cart.setId(uniqueIdGenerator.getStringId());
            cart.setUserId(userToken.getUser().getId());
        }

        cartService.save(cart);

        return modelMap;
    }

    @GetMapping("/address/list")
    public @ResponseBody ModelMap getAddressList(@ModelAttribute ModelMap resultMap, @ModelAttribute UserToken userToken, @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC, size = 6) Pageable pageable) {
        UserAddressSearch userAddressSearch = new UserAddressSearch();
        userAddressSearch.setUserId(userToken.getUser().getId());

        Page<UserAddress> userAddressList = userAddressService.findBySearch(userAddressSearch, pageable);
        resultMap.addAttribute("address", userAddressList.getContent());

        return resultMap;
    }





    @PostMapping("/wishlist/{productId}")
    @ResponseBody
    public ModelMap addWishlist(@ModelAttribute ModelMap modelMap, @ModelAttribute UserToken userToken, @PathVariable String productId) {
        if(productId !=null) {
            UserWishlistSearch search = new UserWishlistSearch();
            search.setProductId(productId);
            search.setUserId(userToken.getUser().getId());
            List<UserWishlist> wishlist = userWishlistService.findBySearch(search, new Sort(Sort.Direction.ASC, "id"));
            if(wishlist.size() == 0){
                UserWishlist wishlistVO = new UserWishlist();
                wishlistVO.setId(uniqueIdGenerator.getStringId());
                wishlistVO.setProductId(productId);
                wishlistVO.setUserId(userToken.getUser().getId());

                userWishlistService.save(wishlistVO);
            }
        }else{
            throw new CustomException(StatusEnum.REQUEST_REQUIRE_PARAMS);
        }

        return modelMap;
    }

    @PostMapping("/delete/{id}")
    @ResponseBody
    public ModelMap deleteContent(@ModelAttribute ModelMap modelMap, @ModelAttribute UserToken userToken, @PathVariable String id) {
        cartService.delete(id);
        return modelMap;
    }

    @PostMapping("/checkout")
    @ResponseBody
    public ModelMap deleteContent(@ModelAttribute ModelMap modelMap, @ModelAttribute UserToken userToken, @RequestBody List<Cart> carts) {

        cartService.save(carts);

        return modelMap;
    }

    @GetMapping("/checkout")
    public ModelAndView getCheckOut(@ModelAttribute UserToken userToken
            ,@RequestParam(name="productId" ,required = false) String productId
            ,@RequestParam(name="productOptionId" ,required = false) String productOptionId
            ,@RequestParam(name="quantity" ,required = false) Integer quantity){
        List<Cart> carts = null;
        Cart detail = new Cart();
        detail.setStatus(null);

        if(productId == null) {
            //유저 카트정보 취득
            CartSearch cartSearch = new CartSearch();
            cartSearch.setStatus(Cart.Status.BEFORE_PAYMENT);
            cartSearch.setUserId(userToken.getUser().getId());
            carts = cartService.findBySearch(cartSearch, new Sort(Sort.Direction.DESC, "id"));
        }else{
            carts = new ArrayList<>();
            detail.setUserId(userToken.getUser().getId());
            detail.setProductId(productId);
            detail.setProductOptionId(productOptionId);
            detail.setQuantity(quantity);
            carts.add(detail);
        }

        BigDecimal                      totalSalePrice = new BigDecimal("0.00");
        BigDecimal                      totalShippingFee    = new BigDecimal("0.00");
        Map<String, ShippingResponseVO> shippingMap    = new HashMap<>();
        for(Cart cart : carts){
            Product product = productService.findOne(cart.getProductId());
            ProductOption productOption = null;
            for(ProductOption option : product.getProductOptions()){
                if(cart.getProductOptionId().equals(option.getId())){
                    productOption = option;
                }
            }

            ShippingResponseVO shippingInfo = shippingMap.get(product.getShipping().getId());
            if(shippingInfo == null){
                shippingInfo = new ShippingResponseVO();

                shippingInfo.setName(product.getShipping().getName());
                shippingInfo.setShippingFee(product.getShipping().getShippingFee());
                List<ShippingProductResponseVO> productInfos = new ArrayList<>();
                ShippingProductResponseVO productInfo = new ShippingProductResponseVO();
                productInfo.setPrductName(product.getName());
                productInfo.setProductOptionName(productOption.getName());
                productInfos.add(productInfo);
                shippingInfo.setProductInfo(productInfos);

                totalShippingFee = totalShippingFee.add(new BigDecimal(product.getShipping().getShippingFee()));
            }else{
                List<ShippingProductResponseVO> productInfos = shippingInfo.getProductInfo();
                ShippingProductResponseVO productInfo = new ShippingProductResponseVO();
                productInfo.setPrductName(product.getName());
                productInfo.setProductOptionName(productOption.getName());
                productInfos.add(productInfo);
            }

            shippingMap.put(product.getShipping().getId(), shippingInfo);

            //총합계산
            BigDecimal salePrice = new BigDecimal(product.getSalePrice());
            BigDecimal quantityBig = new BigDecimal(cart.getQuantity());
            totalSalePrice = totalSalePrice.add(salePrice.multiply(quantityBig));
        }

        ModelAndView modelAndView = new ModelAndView("cart/checkOut");

        //기본주소취득
        UserAddressSearch userAddressSearch = new UserAddressSearch();
        userAddressSearch.setUserId(userToken.getUser().getId());
        userAddressSearch.setDefaultFlag(UserAddress.Flag.TRUE);

        GsonBuilder builder = new GsonBuilder();
        Gson        gson    = builder.create();

        List<UserAddress> userAddresses = userAddressService.findBySearch(userAddressSearch,new Sort(Sort.Direction.DESC, "id"));
        UserAddress userAddress = new UserAddress();
        if(userAddresses.size() > 0){
            userAddress = userAddresses.get(0);
            userAddress.setId(null);
            userAddress.setUserId(null);
            userAddress.setRegistrationDate(null);

        }
        modelAndView.addObject("defaultAddress", gson.toJson(userAddress)); //기본주소 셋팅
        modelAndView.addObject("cart",gson.toJson(detail)); //직접구매로 구입하는 상품

        modelAndView.addObject("shippingInfos", new ArrayList(shippingMap.values()));
        modelAndView.addObject("itemCount",carts.size());
        modelAndView.addObject("totalAmount",totalSalePrice.toPlainString());
        modelAndView.addObject("totalShippingFee",totalShippingFee.toPlainString());
        modelAndView.addObject("orderTotal",totalSalePrice.add(totalShippingFee).toPlainString());
        modelAndView.addObject("stripePublickKey", stripeManager.getPublicKey());
        modelAndView.addObject("clientId", paypalClient.getClientId());

        return modelAndView;
    }
}