package kr.righters.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import kr.righters.controller.common.CommonController;
import kr.righters.controller.vo.AboutCategoryVO;
import kr.righters.domain.entity.AboutCategory;
import kr.righters.domain.entity.AboutDetail;
import kr.righters.domain.search.AboutCategorySearch;
import kr.righters.domain.search.AboutDetailSearch;
import kr.righters.service.AboutCategoryService;
import kr.righters.service.AboutDetailService;
import kr.righters.util.ObjectUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yj.nam on 19.10.29..
 */

@Slf4j
@RestController
@RequestMapping(value = "/about")
public class AboutController implements CommonController {
    @Autowired
    AboutCategoryService aboutCategoryService;

    @Autowired
    AboutDetailService aboutDetailService;

    @GetMapping("")
    public ModelAndView getContent() {
        //최초표시될 카테고리 정보 취득
        AboutCategorySearch aboutCategorySearch = new AboutCategorySearch();
        aboutCategorySearch.setRootFlag(true); //최상위 카테고리 취득
        aboutCategorySearch.setStatus(AboutCategory.Status.SHOW); //표시처리된 것만 취득

        List<AboutCategory> aboutCategoryList = aboutCategoryService.findBySearch(aboutCategorySearch, new Sort(Sort.Direction.ASC, "sortNumber"));

        ModelAndView modelAndView = new ModelAndView("about/about");
        if(aboutCategoryList.size() > 0){
            modelAndView.addObject("firstRootCategoryId", aboutCategoryList.get(0).getId());
            if(aboutCategoryList.get(0).getChildrenCategories().size() > 0){
                modelAndView.addObject("firstSubCategoryId", aboutCategoryList.get(0).getChildrenCategories().get(0).getId());
            }else{
                modelAndView.addObject("firstSubCategoryId", "nothing");
            }

            modelAndView.addObject("firstType", aboutCategoryList.get(0).getType().name());
        }

        return modelAndView;
    }

    @GetMapping("/rootCategoryId/{rootCategoryId}/subCategoryId/{subCategoryId}")
    @ResponseBody
    public ModelMap getDetailContent(@ModelAttribute ModelMap resultMap, @PathVariable String rootCategoryId, @PathVariable String subCategoryId, @RequestParam(required = false) Boolean firstFlag){

        if(firstFlag) {
            AboutCategorySearch aboutCategorySearch = new AboutCategorySearch();
            aboutCategorySearch.setRootFlag(true); //최상위 카테고리 취득
            aboutCategorySearch.setStatus(AboutCategory.Status.SHOW); //표시처리된 것만 취득

            List<AboutCategory> aboutCategoryList = aboutCategoryService.findBySearch(aboutCategorySearch, new Sort(Sort.Direction.ASC, "sortNumber"));

            List<AboutCategoryVO> aboutCategoryVOList = new ArrayList<>();
            for (AboutCategory aboutCategory : aboutCategoryList) {
                AboutCategoryVO aboutCategoryVO = new AboutCategoryVO();
                ObjectUtils.copyProperties(aboutCategory, aboutCategoryVO);

                aboutCategoryVOList.add(aboutCategoryVO);
            }

            resultMap.addAttribute("categories", aboutCategoryVOList);
        }

        AboutCategory selectRootAboutCategory = aboutCategoryService.findOne(rootCategoryId);
        AboutCategory selectChildAboutCategory = null;
        if(selectRootAboutCategory.getChildrenCategories().size() > 0){
            //자식 카테고리 취득
            List<AboutCategoryVO> subAboutCategoryVOList = new ArrayList<>();
            for (AboutCategory subAboutCategory : selectRootAboutCategory.getChildrenCategories()) {
                AboutCategoryVO aboutCategoryVO = new AboutCategoryVO();
                ObjectUtils.copyProperties(subAboutCategory, aboutCategoryVO);

                subAboutCategoryVOList.add(aboutCategoryVO);
            }
            //TODO 서브카테고리 소팅처리
            resultMap.addAttribute("childCategories", subAboutCategoryVOList);

            if("nothing".equals(subCategoryId)){
                //자식 카테고리가 있고 선택된 자식 카테고리가 없을때 제일 처음거로 선택
                selectChildAboutCategory = selectRootAboutCategory.getChildrenCategories().get(0);
                selectChildAboutCategory.setType(selectRootAboutCategory.getType()); //부모 타입으로 설정
                subCategoryId = selectChildAboutCategory.getId();
            }else{
                selectChildAboutCategory = aboutCategoryService.findOne(subCategoryId);
                selectChildAboutCategory.setType(selectRootAboutCategory.getType());//부모 타입으로 설정
            }
        }

        if(selectChildAboutCategory != null){
            //리턴할 데이터 정리
            selectRootAboutCategory = selectChildAboutCategory;
        }

        AboutDetailSearch aboutDetailSearch = new AboutDetailSearch();
        aboutDetailSearch.setAboutCategoryId("nothing".equals(subCategoryId) ? rootCategoryId : subCategoryId);
        aboutDetailSearch.setStatus(AboutDetail.Status.SHOW);
        List<AboutDetail> aboutDetails = aboutDetailService.findBySearch(aboutDetailSearch, new Sort(Sort.Direction.DESC, "id"));

        AboutCategoryVO content = new AboutCategoryVO();
        ObjectUtils.copyProperties(selectRootAboutCategory, content);

        content.setAboutDetails(aboutDetails);

        resultMap.addAttribute("content", content);

        return resultMap;
    }
}