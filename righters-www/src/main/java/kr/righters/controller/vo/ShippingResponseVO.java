package kr.righters.controller.vo;

import lombok.Data;

import java.util.List;

@Data
public class ShippingResponseVO {

    private String name;
    private Long shippingFee;
    private List<ShippingProductResponseVO> productInfo;
}
