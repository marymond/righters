package kr.righters.controller.vo;

import lombok.Data;

@Data
public class IamportCheckoutResponseVO {
//    private Boolean success; //결제처리가 성공적이었는지 여부
//    private String error_code; //결제처리에 실패한 경우 단축메세지(현재 코드체계는 없음)
//    private String error_msg; //결제처리에 실패한 경우 상세메세지
    private String imp_uid; //아임포트 거래 고유 번호
    private String merchant_uid; //가맹점에서 생성/관리하는 고유 주문번호
//    private String pay_method; //결제수단
//    private Long paid_amount; //결제금액
//    private String status; //결제상태, ready(미결제), paid(결제완료), cancelled(결제취소, 부분취소포함), failed(결제실패)
//    private String name; //주문명
//    private String pg_provider; //결제승인/시도된 PG사, html5_inicis(웹표준방식의 KG이니시스), inicis(일반 KG이니시스), kakaopay(카카오페이), uplus(LGU+), nice(나이스정보통신), jtnet(JTNet), danal(다날)
//    private String pg_tid; //PG사 거래고유번호
//    private String buyer_name; //주문자 이름
//    private String buyer_email; //주문자 Email
//    private String buyer_tel; //주문자 연락처
//    private String buyer_addr; //주문자 주소
//    private String buyer_postcode; //주문자 우편번호
////    private LocalDateTime paid_at; //결제승인시각
//    private String receipt_url;// PG사에서 발행되는 거래 매출전표 URL
//
//    //추가정보
//    private String apply_num; //카드사 승인번호
//    private String vbank_num; //가상계좌 입금계좌번호
//    private String vbank_name; //가상계좌 은행명
//    private String vbank_holder; //가상계좌 예금주
////    private LocalDateTime vbank_date; //가상계좌 입금기한
}
