package kr.righters.controller.vo;

import kr.righters.domain.entity.Cart;
import kr.righters.domain.entity.UserAddress;
import lombok.Data;

@Data
public class CheckOutRequestVO {

    private Cart        cart;
    private UserAddress shippingAddress;
}
