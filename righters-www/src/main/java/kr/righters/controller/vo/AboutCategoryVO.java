package kr.righters.controller.vo;

import kr.righters.domain.entity.AboutCategory;
import kr.righters.domain.entity.AboutDetail;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * Created by yj.nam on 20.10.14..
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AboutCategoryVO implements Serializable{

    private static final long serialVersionUID = -5622165957880355666L;

    private String id;
    private String name;

    private String parentId;

    private AboutCategory.ChildrenFlag childrenFlag = AboutCategory.ChildrenFlag.FALSE;

    private Integer sortNumber;

    private AboutCategory.Type type;

    private AboutCategory.Status status = AboutCategory.Status.HIDE;

    private List<AboutCategoryVO> subCategories;
    private List<AboutDetail> aboutDetails;
}
