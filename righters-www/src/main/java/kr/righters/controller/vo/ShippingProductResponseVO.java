package kr.righters.controller.vo;

import lombok.Data;

import java.util.List;

@Data
public class ShippingProductResponseVO {

    private String prductName;
    private String productOptionName;
}
