package kr.righters.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import kr.righters.controller.common.CommonController;
import kr.righters.manager.StripeManager;
import kr.righters.manager.paypal.PaypalManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

/**
 * Created by yj.nam on 20.01.31..
 */
@Slf4j
@RestController
public class WebhookController implements CommonController {
    @Autowired
    StripeManager stripeManager;

    @Autowired
    PaypalManager paypalManager;

    /**
     * 스트라이프 웹훅
     * @param request
     * @param response
     */
    @PostMapping("/stripe/webhook")
    @ResponseBody
    public void stripeWebhook(HttpServletRequest request, HttpServletResponse response) {
        GsonBuilder builder = new GsonBuilder();
        Gson        gson    = builder.create();
        log.info(String.format("Stripe Webhook Excute Request:{}", gson.toJson(request)));

        String payload        = this.getBody(request);
        String sigHeader      = request.getHeader("Stripe-Signature");

        stripeManager.webhook(payload, sigHeader);
    }

    /**
     * webhook으로 받은 리퀘스트를 문자열로 변환
     * @param request
     * @return
     */
    private String getBody(HttpServletRequest request){
        String         body           = null;
        StringBuilder  stringBuilder  = new StringBuilder();
        BufferedReader bufferedReader = null;

        try {
            InputStream inputStream = request.getInputStream();
            if (inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(request.getInputStream()));
                char[] charBuffer = new char[128];
                int bytesRead = -1;
                while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                    stringBuilder.append(charBuffer, 0, bytesRead);
                }
            }
        } catch (IOException ex) {
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ex) {
                }
            }
        }

        body = stringBuilder.toString();
        return body;
    }

    @PostMapping("/paypal/webhook")
    @ResponseBody
    public void getEvent(@RequestBody Map<String, Object> request) {

        GsonBuilder builder = new GsonBuilder();
        Gson        gson    = builder.create();
        log.info(String.format("Stripe Webhook Excute Request:{}", gson.toJson(request)));


        paypalManager.webhook(request);
    }

}

