package kr.righters.controller;

import kr.righters.controller.common.CommonController;
import kr.righters.controller.common.SecurityController;
import kr.righters.controller.vo.CheckOutRequestVO;
import kr.righters.domain.common.UserToken;
import kr.righters.domain.entity.Cart;
import kr.righters.domain.entity.Product;
import kr.righters.domain.entity.ProductOption;
import kr.righters.domain.search.CartSearch;
import kr.righters.manager.paypal.PaypalManager;
import kr.righters.service.CartService;
import kr.righters.service.ProductService;
import kr.righters.service.UserAddressService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yj.nam on 20.01.31..
 */
@Slf4j
@Controller
public class PaypalController implements CommonController, SecurityController {
    @Autowired
    PaypalManager paypalManager;

    @Autowired
    CartService cartService;

    @Autowired
    ProductService productService;

    @Autowired
    UserAddressService userAddressService;

    //결제 성공
    @GetMapping("/paypal/success")
    public ModelAndView success(){
        ModelAndView          modelAndView     = new ModelAndView("cart/success");

        return modelAndView;
    }

    //결제 실패
    @GetMapping("/paypal/fail")
    public ModelAndView fail(){
        ModelAndView          modelAndView     = new ModelAndView("cart/canceled");

        return modelAndView;
    }

    /**
     * 주문장 생성
     * @param modelMap
     * @param userToken
     * @param
     * @return
     */
    @ResponseBody
    @PostMapping("/create-paypal-transaction")
    public ModelMap createOrder(HttpServletRequest httpServletRequest, @ModelAttribute ModelMap modelMap, @ModelAttribute UserToken userToken, @RequestBody CheckOutRequestVO checkOutRequestVO) {
        //유효성 체크
        //shipping address
        userAddressService.check(checkOutRequestVO.getShippingAddress());

        Cart cartVO = checkOutRequestVO.getCart();

        List<Cart> carts = null;
        if(cartVO.getProductId() == null) {
            //유저 카트정보 취득
            CartSearch cartSearch = new CartSearch();
            cartSearch.setStatus(Cart.Status.BEFORE_PAYMENT);
            cartSearch.setUserId(userToken.getUser().getId());
            carts = cartService.findBySearch(cartSearch, new Sort(Sort.Direction.DESC, "id"));
        }else{
            carts = new ArrayList<>();
            carts.add(cartVO);
        }

        for(Cart cart : carts) {
            Product product = productService.findOne(cart.getProductId());
            for (ProductOption option : product.getProductOptions()) {
                if (cart.getProductOptionId().equals(option.getId())) {
                    cart.setProductOption(option);
                }
            }
            cart.setProduct(product);

            //다른유저에 의해 공유된것인지 세션확인
            Cookie shareUserCookie = WebUtils.getCookie(httpServletRequest, product.getId());
            if(shareUserCookie != null){
                product.setShareUid(shareUserCookie.getValue());
            }
        }

        String      orderID = paypalManager.createOrder(carts, checkOutRequestVO.getShippingAddress(), userToken.getUser(), cartVO.getProductId() == null ? false: true);
        modelMap.addAttribute("orderID", orderID);

        return modelMap;
    }

    /**
     * 결제실행
     * @param modelMap
     * @param userToken
     * @param orderId
     * @return
     */
    @ResponseBody
    @PostMapping("/get-paypal-transaction")
    public ModelMap getTransaction(@ModelAttribute ModelMap modelMap, @ModelAttribute UserToken userToken, @RequestBody String orderId) {

        boolean result = paypalManager.captureOrder(orderId);
        modelMap.addAttribute("result", result);

        return modelMap;
    }

//    /**
//     * 환불처리
//     * @param modelMap
//     * @param userToken
//     * @param captureId
//     * @return
//     */
//    @ResponseBody
//    @PostMapping("/refund/{captureId}/{productId}")
//    public ModelMap refund(@ModelAttribute ModelMap modelMap, @ModelAttribute UserToken userToken, @PathVariable String captureId, @PathVariable String productId) {
//        //환불금액 계산
//
//
//        paypalManager.refundOrder(captureId,0L);
//
//        return modelMap;
//    }

}

