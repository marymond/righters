package kr.righters.controller;

import kr.righters.config.ApplicationProperty;
import kr.righters.controller.common.CommonController;
import kr.righters.domain.common.CookieEnum;
import kr.righters.domain.common.UserToken;
import kr.righters.domain.entity.User;
import kr.righters.domain.entity.UserSigninHistory;
import kr.righters.domain.search.UserSearch;
import kr.righters.exception.CustomJsonException;
import kr.righters.manager.MemcachedManager;
import kr.righters.repository.UserSigninHistoryRepository;
import kr.righters.service.UserService;
import kr.righters.util.PasswordEncoder;
import kr.righters.util.RandomStringGenerator;
import kr.righters.util.UniqueIdGenerator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.List;


/**
 * Created by yj.nam on 19. 12. 05..
 */
@Slf4j
@Controller
@RequestMapping(value = "/user")
public class UserSignController implements CommonController {

    private static final String REGEX_FOR_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    @Autowired
    ApplicationProperty applicationProperty;

    @Autowired
    UserService userService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UniqueIdGenerator uniqueIdGenerator;

    @Autowired
    MemcachedManager memcachedManager;

    @Autowired
    RandomStringGenerator randomStringGenerator;

    @Autowired
    UserSigninHistoryRepository userSigninHistoryRepository;

    /**
     * 로그인 화면
     *
     * @param request
     * @param response
     * @param session
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/signin")
    public String getSignin(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap modelMap, @RequestParam(required = false) String returnPage) {

        if (StringUtils.isNotEmpty(returnPage)) {
            modelMap.addAttribute("returnPage", returnPage);
        }

        return "user/signin";

    }

    /**
     * 로그인 처리
     *
     * @param modelMap
     * @param signIn
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "/signin")
    @ResponseBody
    public ModelMap postSignin(HttpServletRequest request, HttpServletResponse httpServletResponse, @ModelAttribute ModelMap modelMap, @RequestBody User signIn) {

        if (!signIn.getLoginId().matches(REGEX_FOR_EMAIL)) {
            throw new CustomJsonException("ID does not match email format");
        }

        UserSearch userSearch = new UserSearch();
        userSearch.setLoginId(signIn.getLoginId());

        List<User> users = userService.findBySearch(userSearch, new Sort(Sort.Direction.ASC, "id"));

//        // 로그인 임시 차단
//        if (users.size() > 0) {
//            throw new CustomJsonException("현재 서비스 점검중입니다. 보다 더 나은 서비스를 위해 기다려 주시면 감사드리겠습니다.");
//        }

        if (users.size() == 0) {
            throw new CustomJsonException("존재하지않는 유저입니다. ID를 확인해주세요.");
        }

        //로그인유저 정보취득
        User user = users.get(0);

        if (StringUtils.isEmpty(user.getPassword())) {
            throw new CustomJsonException("패스워드를 입력해 주세요.");
        } else {
            if (!passwordEncoder.isMatch(signIn.getPassword(),user.getPassword())) {
                throw new CustomJsonException("패스워드가 틀립니다.");
            }
        }

        if (user.getStatus().equals(User.Status.UNUSED)) {
            throw new CustomJsonException("탈퇴한 사용자입니다. 관리자에게 문의하세요.");
        }

        this.signin(httpServletResponse, user);

        return modelMap;

    }

    /**
     * 로그인 처리
     * @param httpServletResponse
     * @param user
     */
    private void signin(HttpServletResponse httpServletResponse, User user){
        // 토큰값
        String token = uniqueIdGenerator.getTokenId();
        // 토큰 생성
        UserToken userToken = new UserToken(token, user, new Timestamp(System.currentTimeMillis()));

        memcachedManager.setUserToken(userToken);

        // 토큰쿠키 생성
        Cookie userTokenCookie = new Cookie(CookieEnum.USER_TOKEN.getCookieName(), token);
//        userTokenCookie.setMaxAge(CookieEnum.USER_TOKEN.getExpiration());
        userTokenCookie.setMaxAge(-1);
        userTokenCookie.setPath("/");

        httpServletResponse.addCookie(userTokenCookie);


        //사인인 이력 추가.
        HttpServletRequest req      = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        String             signinIp = req.getHeader("X-FORWARDED-FOR");
        if (StringUtils.isEmpty(signinIp)) {
            signinIp = req.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (StringUtils.isEmpty(signinIp)) {
            signinIp = req.getRemoteAddr();
        }
        String signinEnvironment = req.getHeader("User-Agent");

        UserSigninHistory userSigninHistory = new UserSigninHistory(uniqueIdGenerator.getStringId(), user.getId(), token, UserSigninHistory.TokenStatus.SIGNIN, signinIp, signinEnvironment, null);

        userSigninHistoryRepository.save(userSigninHistory);

    }


    /**
     * 회원가입화면
     *
     * @param request
     * @param response
     * @param session
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/signup")
    public String getSignup(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap modelMap) {

        return "user/signup";

    }


    /**
     * 회원가입처리
     *
     * @param resultMap
     * @param signup
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "/signup")
    @ResponseBody
    ModelMap postSignup(HttpServletResponse httpServletResponse, @ModelAttribute ModelMap resultMap, @Valid @RequestBody User signup) {

        // Validate Check(DB 필수사항 체크)
        if (StringUtils.isEmpty(signup.getFullName())) {
            throw new CustomJsonException("Input your Full Name, please.");
        }
        if (StringUtils.isEmpty(signup.getLoginId())) {
            throw new CustomJsonException("Input your ID(Email), please.");
        }
        if (StringUtils.isEmpty(signup.getPassword())) {
            throw new CustomJsonException("Input your Password, please.");
        }
        if (StringUtils.isEmpty(signup.getPasswordConfirm())) {
            throw new CustomJsonException("Input your Password Confirm, please.");
        }
        if (StringUtils.isEmpty(signup.getPhoneNumber())) {
            throw new CustomJsonException("Input your Phone Number, please.");
        }

        if (!signup.getLoginId().matches(REGEX_FOR_EMAIL)) {
            throw new CustomJsonException("ID(Email) does not match email format");
        }

        if(signup.getFullName().length() > 100){
            throw new CustomJsonException("You cannot enter more than 100 digits for the Full Name.");
        }

        if(signup.getLoginId().length() > 30){
            throw new CustomJsonException("You cannot enter more than 100 digits for the ID(Email)");
        }

        //패스워드 체크
        if(signup.getPassword().length() < 6){
            throw new CustomJsonException("Please enter at least 6 digits for the password.");
        }

        if (!StringUtils.equals(signup.getPassword(), signup.getPasswordConfirm())) {
            throw new CustomJsonException("Password and password confirm do not match.");
        }

        //아이디중복체크
        UserSearch userSearch = new UserSearch();
        userSearch.setLoginId(signup.getLoginId());

        List<User> users = userService.findBySearch(userSearch, new Sort(Sort.Direction.ASC, "id"));

        if (users.size() > 0) {
            throw new CustomJsonException("This ID is already taken.");
        }

        //전화번호 형식 체크(외국전화번호 체크용)
        String phoneNumber      = signup.getPhoneNumber().replaceAll("-", "");
        String mobileNumberRegex = "^[0-9]*$";//숫자만 있는지 확인
        if (!phoneNumber.matches(mobileNumberRegex)) {
            throw new CustomJsonException("The phone number is not in the correct format.");
        }

        //전화번호 중복체크
        userSearch = new UserSearch();
        userSearch.setPhoneNumber(phoneNumber);
        userSearch.setNotStatus(User.Status.UNUSED);

        users = userService.findBySearch(userSearch, new Sort(Sort.Direction.ASC, "id"));
        if (users.size() > 0) {
            throw new CustomJsonException("This phone number is already in use.");
        }

        if (StringUtils.isEmpty(signup.getId())) {
            signup.setId(uniqueIdGenerator.getStringId());
        }

        signup.setPassword(passwordEncoder.encodePassword(signup.getPassword())); //패스워드 암호화

        // 가입시 이메일은 로그인아이디로 초기등록
        signup.setEmail(signup.getLoginId());

        // 추천코드 등록
        boolean validRefferer = false;
        do {

            String myUid    = randomStringGenerator.generateUserRefferer();

            userSearch = new UserSearch();
            userSearch.setUid(myUid);
            users = userService.findBySearch(userSearch, new Sort(Sort.Direction.ASC, "id"));

            if (users.size() == 0) {
                signup.setUid(myUid);
                validRefferer = true;
            }

        } while (!validRefferer);

        //썸네일 기본셋팅
        signup.setProfileImageUrl("https://via.placeholder.com/100x100");

        User user = userService.save(signup);

        //로그인처리
        this.signin(httpServletResponse, user); //토큰생성 및 쿠키 셋팅

        resultMap.addAttribute("signup", signup);
        return resultMap;

    }

    @RequestMapping(method = RequestMethod.POST, value = "/signup/check/loginId")
    @ResponseBody
    ModelMap postSignupCheckEmail(@ModelAttribute ModelMap resultMap, @Valid @RequestBody User signup) {

        if (null == signup || StringUtils.isEmpty(signup.getLoginId())) {
            throw new CustomJsonException("필수 입력사항을 입력해주세요.");
        }

        if (!signup.getLoginId().matches(REGEX_FOR_EMAIL)) {
            throw new CustomJsonException("ID가 이메일 형식에 맞지 않습니다.");
        }

        //아이디중복체크
        UserSearch userSearch = new UserSearch();
        userSearch.setLoginId(signup.getLoginId());

        List<User> users = userService.findBySearch(userSearch,new Sort(Sort.Direction.ASC, "id"));

        if (users.size() > 0) {
            throw new CustomJsonException("사용중인 아이디입니다.");
        }

        return resultMap;

    }
    @RequestMapping(method = RequestMethod.POST, value = "/signup/check/mobileNumber")
    @ResponseBody
    ModelMap postSignupCheckMobileNumber(@ModelAttribute ModelMap resultMap, @Valid @RequestBody User signup) {

        if (null == signup || StringUtils.isEmpty(signup.getPhoneNumber())) {
            throw new CustomJsonException("필수 입력사항을 입력해주세요.");
        }

        String phoneNumber      = signup.getPhoneNumber().replaceAll("-", "");

        //TODO 전화번호 형식체크
//        String mobileNumberRegex = "^\\d{10,11}$";
//        if (!mobileNumber.matches(mobileNumberRegex)) {
//            throw new CustomJsonException("휴대폰번호 형식이 잘못되었습니다.");
//        }

        UserSearch userSearch = new UserSearch();
        userSearch.setPhoneNumber(phoneNumber);
        userSearch.setNotStatus(User.Status.UNUSED);

        long existCount = userService.findBySearch(userSearch, new Sort(Sort.Direction.ASC, "id")).size();

        if (existCount > 0) {
            throw new CustomJsonException("사용중인 휴대폰번호 입니다.");
        }

        return resultMap;

    }

    @GetMapping("/findPassword")
    public ModelAndView getfindPassword() {
        ModelAndView modelAndView = new ModelAndView("user/findPassword");
        return modelAndView;
    }

    @GetMapping("/editPassword")
    public ModelAndView geteditPassword() {
        ModelAndView modelAndView = new ModelAndView("user/editPassword");
        return modelAndView;
    }
}
