package kr.righters.controller;

import kr.righters.controller.common.CommonController;
import kr.righters.controller.common.SecurityController;
import kr.righters.domain.entity.Notice;
import kr.righters.domain.entity.UserWishlist;
import kr.righters.domain.search.UserWishlistSearch;
import kr.righters.repository.NoticeRepository;
import kr.righters.repository.UserWishlistRepository;
import kr.righters.util.UniqueIdGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by yj.nam on 19.12.17..
 */

@Slf4j
@RestController
@RequestMapping(value = "/wishlist")
public class WishlistController implements CommonController, SecurityController {

}