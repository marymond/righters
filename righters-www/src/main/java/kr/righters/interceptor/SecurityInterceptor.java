package kr.righters.interceptor;

import kr.righters.config.ApplicationProperty;
import kr.righters.controller.common.SecurityController;
import kr.righters.domain.common.CookieEnum;
import kr.righters.domain.common.UserToken;
import kr.righters.domain.entity.User;
import kr.righters.domain.entity.UserSigninHistory;
import kr.righters.manager.MemcachedManager;
import kr.righters.repository.UserRepository;
import kr.righters.repository.UserSigninHistoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.sql.Timestamp;

/**
 * Created by yj.nam on 2019. 12. 13..
 */
@Slf4j
public class SecurityInterceptor implements HandlerInterceptor {


    @Autowired
    ApplicationProperty applicationProperty;

    @Autowired
    MemcachedManager memcachedManager;

    @Autowired
    UserSigninHistoryRepository userSigninHistoryRepository;

    @Autowired
    UserRepository userRepository;
    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object handler) throws Exception {


        Cookie tokenCookie = WebUtils.getCookie(httpServletRequest, CookieEnum.USER_TOKEN.getCookieName());
        if (((HandlerMethod) handler).getBean() instanceof SecurityController) {
            // 로그인 사용자 정보를 쿠키에서 가져옴

            StringBuilder sb = new StringBuilder();
            String path = httpServletRequest.getServletPath();
            String qs = httpServletRequest.getQueryString();
            if(StringUtils.isNotEmpty(qs)) {
                path = path + "?" + qs;
            }

            sb.append(URLEncoder.encode(applicationProperty.getApplicationHost() + path, "UTF-8"));

            if (null == tokenCookie) {
                httpServletResponse.sendRedirect(applicationProperty.getApplicationHost() + "/user/signin?returnPage="+sb.toString());
                return false;
            }

            UserToken userToken = memcachedManager.getUserToken(tokenCookie.getValue());

            if (null == userToken) {
                UserSigninHistory userTokenDb = userSigninHistoryRepository.findByToken(tokenCookie.getValue());

                if (userTokenDb == null || userTokenDb.getTokenStatus() != UserSigninHistory.TokenStatus.SIGNIN) {
                    httpServletResponse.sendRedirect(applicationProperty.getApplicationHost() + "/user/signin?returnPage="+sb.toString());
                    return false;
                }

                User user = userRepository.findOne(userTokenDb.getId());
                if (null == user || user.getStatus().equals(User.Status.USED)) {
                    httpServletResponse.sendRedirect(applicationProperty.getApplicationHost() + "/user/signin?returnPage="+sb.toString());
                    return false;
                }

                userToken = new UserToken(userTokenDb.getToken(), user, new Timestamp(System.currentTimeMillis()));
                memcachedManager.setUserToken(userToken);
            }

            // Memcached Token to more life
            if(userToken.getCreatedAt()==null || (new Timestamp(System.currentTimeMillis()).getTime() - userToken.getCreatedAt().getTime()) > 1000*(MemcachedManager.Key.USER_TOKEN.getExpiration()-(10*60))) {
                userToken.setCreatedAt(new Timestamp(System.currentTimeMillis()));
                memcachedManager.setUserToken(userToken);
            }
        }

        if(null!=tokenCookie) {
//            tokenCookie.setMaxAge(CookieEnum.USER_TOKEN.getExpiration());
            tokenCookie.setMaxAge(-1);
            tokenCookie.setPath("/");
            httpServletResponse.addCookie(tokenCookie);
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object handler, Exception e) throws Exception {

    }
}
