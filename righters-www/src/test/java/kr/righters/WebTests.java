package kr.righters;

import kr.righters.util.UniqueIdGenerator;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by yj.nam on 19. 10. 4..
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("localhost")
public class WebTests {

    @Autowired
    UniqueIdGenerator uniqueIdGenerator;

    @Test
    public void postUsersToken(){
        String token = uniqueIdGenerator.getTokenId();
        System.out.println(token);
    }

}
