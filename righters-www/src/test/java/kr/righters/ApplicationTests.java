package kr.righters;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by yjnam on 20. 02. 11..
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class ApplicationTests {


	@Test
	public void contextLoads() {
		long day = ChronoUnit.DAYS.between(LocalDate.of(2017,10,5),LocalDate.of(2017,11,03));

		System.out.println(day +"********");
	}

	@Test
	public void test(){
		String pattern = "^\\d+(?:[.]?[\\d]?[\\d])?$";

		String  value ="22.222";

		Pattern p     = Pattern.compile(pattern);
		Matcher m     = p.matcher(value);
		boolean ok    = m.matches();

		System.out.println(pattern+", "+value+", "+(ok ? "O" : "X"));

	}

	@Test
	public void randomTest(){
		long time = System.currentTimeMillis();
		System.out.println(time);
	}

}
