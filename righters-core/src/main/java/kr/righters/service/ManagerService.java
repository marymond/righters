package kr.righters.service;

import kr.righters.domain.entity.Manager;
import kr.righters.domain.search.ManagerSearch;
import kr.righters.repository.ManagerRepository;
import kr.righters.util.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ManagerService {
	@Autowired
	private ManagerRepository repository;

	public Manager save(Manager content) {
		return repository.save(content);
	}

	@Transactional
	public void save(List<Manager> contents) {
		repository.save(contents);
	}

	public Manager findOne(String id) {
		if (StringUtils.isEmpty(id)) {
			return null;
		}
		if ("new".equals(id)) {
			return new Manager();
		}

		return repository.findOne(id);
	}

	public Manager save(String id, Manager parameter) {
		Manager source = findOne(id);

		ObjectUtils.copyProperties(parameter, source);

		return save(source);
	}

	public List<Manager> findAll() {
		return repository.findAll();
	}

	public Page<Manager> findBySearch(ManagerSearch search, Pageable pageable) {
		return repository.findAll(search.precidate(), pageable);
	}

	public List<Manager> findBySearch(ManagerSearch search, Sort sort) {
		return (List<Manager>) repository.findAll(search.precidate(), sort);
	}
}