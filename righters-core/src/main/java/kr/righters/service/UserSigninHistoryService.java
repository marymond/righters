package kr.righters.service;

import kr.righters.domain.entity.UserSigninHistory;
import kr.righters.domain.search.UserSigninHistorySearch;
import kr.righters.repository.UserSigninHistoryRepository;
import kr.righters.util.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class UserSigninHistoryService {
	@Autowired
	private UserSigninHistoryRepository repository;

	public UserSigninHistory save(UserSigninHistory content) {
		return repository.save(content);
	}

	@Transactional
	public void save(List<UserSigninHistory> contents) {
		repository.save(contents);
	}

	public UserSigninHistory findOne(String id) {
		if (StringUtils.isEmpty(id)) {
			return null;
		}
		if ("new".equals(id)) {
			return new UserSigninHistory();
		}

		return repository.findOne(id);
	}

	public UserSigninHistory save(String id, UserSigninHistory parameter) {
		UserSigninHistory source = findOne(id);

		ObjectUtils.copyProperties(parameter, source);

		return save(source);
	}

	public List<UserSigninHistory> findAll() {
		return repository.findAll();
	}

	public Page<UserSigninHistory> findBySearch(UserSigninHistorySearch search, Pageable pageable) {
		return repository.findAll(search.precidate(), pageable);
	}

	public List<UserSigninHistory> findBySearch(UserSigninHistorySearch search, Sort sort) {
		return (List<UserSigninHistory>) repository.findAll(search.precidate(), sort);
	}
}