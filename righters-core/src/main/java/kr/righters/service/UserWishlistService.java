package kr.righters.service;

import kr.righters.domain.entity.UserWishlist;
import kr.righters.domain.search.UserWishlistSearch;
import kr.righters.repository.UserWishlistRepository;
import kr.righters.util.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class UserWishlistService {
	@Autowired
	private UserWishlistRepository repository;

	public UserWishlist save(UserWishlist content) {
		return repository.save(content);
	}

	@Transactional
	public void save(List<UserWishlist> contents) {
		repository.save(contents);
	}

	public UserWishlist findOne(String id) {
		if (StringUtils.isEmpty(id)) {
			return null;
		}
		if ("new".equals(id)) {
			return new UserWishlist();
		}

		return repository.findOne(id);
	}

	public UserWishlist save(String id, UserWishlist parameter) {
		UserWishlist source = findOne(id);

		ObjectUtils.copyProperties(parameter, source);

		return save(source);
	}

	public List<UserWishlist> findAll() {
		return repository.findAll();
	}

	public Page<UserWishlist> findBySearch(UserWishlistSearch search, Pageable pageable) {
		return repository.findAll(search.precidate(), pageable);
	}

	public List<UserWishlist> findBySearch(UserWishlistSearch search, Sort sort) {
		return (List<UserWishlist>) repository.findAll(search.precidate(), sort);
	}

	public void delete(UserWishlist wishlist){
		repository.delete(wishlist);
	}
}