package kr.righters.service;

import kr.righters.domain.entity.ProductOption;
import kr.righters.domain.search.ProductOptionSearch;
import kr.righters.repository.ProductOptionRepository;
import kr.righters.util.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ProductOptionService {
	@Autowired
	private ProductOptionRepository repository;

	public ProductOption save(ProductOption content) {
		return repository.save(content);
	}

	@Transactional
	public void save(List<ProductOption> contents) {
		repository.save(contents);
	}

	public ProductOption findOne(String id) {
		if (StringUtils.isEmpty(id)) {
			return null;
		}
		if ("new".equals(id)) {
			return new ProductOption();
		}

		return repository.findOne(id);
	}

	public ProductOption save(String id, ProductOption parameter) {
		ProductOption source = findOne(id);

		ObjectUtils.copyProperties(parameter, source);

		return save(source);
	}

	public List<ProductOption> findAll() {
		return repository.findAll();
	}

	public Page<ProductOption> findBySearch(ProductOptionSearch search, Pageable pageable) {
		return repository.findAll(search.precidate(), pageable);
	}

	public List<ProductOption> findBySearch(ProductOptionSearch search, Sort sort) {
		return (List<ProductOption>) repository.findAll(search.precidate(), sort);
	}
}