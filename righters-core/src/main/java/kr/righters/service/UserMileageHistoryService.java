package kr.righters.service;

import kr.righters.domain.entity.UserMileageHistory;
import kr.righters.domain.search.UserMileageHistorySearch;
import kr.righters.repository.UserMileageHistoryRepository;
import kr.righters.util.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class UserMileageHistoryService {
	@Autowired
	private UserMileageHistoryRepository repository;

	public UserMileageHistory save(UserMileageHistory content) {
		return repository.save(content);
	}

	@Transactional
	public void save(List<UserMileageHistory> contents) {
		repository.save(contents);
	}

	public UserMileageHistory findOne(String id) {
		if (StringUtils.isEmpty(id)) {
			return null;
		}
		if ("new".equals(id)) {
			return new UserMileageHistory();
		}

		return repository.findOne(id);
	}

	public UserMileageHistory save(String id, UserMileageHistory parameter) {
		UserMileageHistory source = findOne(id);

		ObjectUtils.copyProperties(parameter, source);

		return save(source);
	}

	public List<UserMileageHistory> findAll() {
		return repository.findAll();
	}

	public Page<UserMileageHistory> findBySearch(UserMileageHistorySearch search, Pageable pageable) {
		return repository.findAll(search.precidate(), pageable);
	}

	public List<UserMileageHistory> findBySearch(UserMileageHistorySearch search, Sort sort) {
		return (List<UserMileageHistory>) repository.findAll(search.precidate(), sort);
	}
}