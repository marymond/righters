package kr.righters.service;

import kr.righters.domain.entity.AboutCategory;
import kr.righters.domain.search.AboutCategorySearch;
import kr.righters.repository.AboutCategoryRepository;
import kr.righters.util.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class AboutCategoryService {
	@Autowired
	private AboutCategoryRepository repository;

	public AboutCategory save(AboutCategory content) {
		return repository.save(content);
	}

	@Transactional
	public void save(List<AboutCategory> contents) {
		repository.save(contents);
	}

	public AboutCategory findOne(String id) {
		if (StringUtils.isEmpty(id)) {
			return null;
		}
		if ("new".equals(id)) {
			AboutCategory aboutCategory = new AboutCategory();
			aboutCategory.setStatus(AboutCategory.Status.HIDE);
			return aboutCategory;
		}

		return repository.findOne(id);
	}

	public AboutCategory save(String id, AboutCategory parameter) {
		AboutCategory source = findOne(id);

		ObjectUtils.copyProperties(parameter, source);

		return save(source);
	}

	public List<AboutCategory> findAll() {
		return repository.findAll();
	}

	public Page<AboutCategory> findBySearch(AboutCategorySearch search, Pageable pageable) {
		return repository.findAll(search.precidate(), pageable);
	}

	public List<AboutCategory> findBySearch(AboutCategorySearch search, Sort sort) {
		return (List<AboutCategory>) repository.findAll(search.precidate(), sort);
	}
}