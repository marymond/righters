package kr.righters.service;

import kr.righters.domain.entity.Cart;
import kr.righters.domain.search.CartSearch;
import kr.righters.repository.CartRepository;
import kr.righters.util.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class CartService {
	@Autowired
	private CartRepository repository;

	public Cart save(Cart content) {
		return repository.save(content);
	}

	@Transactional
	public void save(List<Cart> contents) {
		repository.save(contents);
	}

	public Cart findOne(String id) {
		if (StringUtils.isEmpty(id)) {
			return null;
		}
		if ("new".equals(id)) {
			return new Cart();
		}

		return repository.findOne(id);
	}

	public Cart save(String id, Cart parameter) {
		Cart source = findOne(id);

		ObjectUtils.copyProperties(parameter, source);

		return save(source);
	}

	public void delete(String id){
		repository.delete(id);
	}

	public void deleteList(List<Cart> carts){
		repository.delete(carts);
	}

	public List<Cart> findAll() {
		return repository.findAll();
	}

	public Page<Cart> findBySearch(CartSearch search, Pageable pageable) {
		return repository.findAll(search.precidate(), pageable);
	}

	public List<Cart> findBySearch(CartSearch search, Sort sort) {
		return (List<Cart>) repository.findAll(search.precidate(), sort);
	}
}