package kr.righters.service;

import kr.righters.domain.entity.Faq;
import kr.righters.domain.search.FaqSearch;
import kr.righters.repository.FaqRepository;
import kr.righters.util.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class FaqService {
	@Autowired
	private FaqRepository repository;

	public Faq save(Faq content) {
		return repository.save(content);
	}

	@Transactional
	public void save(List<Faq> contents) {
		repository.save(contents);
	}

	public Faq findOne(String id) {
		if (StringUtils.isEmpty(id)) {
			return null;
		}
		if ("new".equals(id)) {
			return new Faq();
		}

		return repository.findOne(id);
	}

	public Faq save(String id, Faq parameter) {
		Faq source = findOne(id);

		ObjectUtils.copyProperties(parameter, source);

		return save(source);
	}

	public List<Faq> findAll() {
		return repository.findAll();
	}

	public Page<Faq> findBySearch(FaqSearch search, Pageable pageable) {
		return repository.findAll(search.precidate(), pageable);
	}

	public List<Faq> findBySearch(FaqSearch search, Sort sort) {
		return (List<Faq>) repository.findAll(search.precidate(), sort);
	}
}