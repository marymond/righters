package kr.righters.service;

import kr.righters.domain.entity.Banner;
import kr.righters.domain.search.BannerSearch;
import kr.righters.repository.BannerRepository;
import kr.righters.util.ObjectUtils;
import kr.righters.util.UniqueIdGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class BannerService {
	@Autowired
	private UniqueIdGenerator uniqueIdGenerator;

	@Autowired
	private BannerRepository repository;

	public Banner save(Banner content) {
		return repository.save(content);
	}

	@Transactional
	public void save(List<Banner> contents) {
		repository.save(contents);
	}

	public Banner findOne(String id) {
		if (StringUtils.isEmpty(id)) {
			return null;
		}
		if ("new".equals(id)) {
			Banner banner = new Banner();
			banner.setId(uniqueIdGenerator.getStringId());

			return banner;
		}

		return repository.findOne(id);
	}

	public Banner save(String id, Banner parameter) {
		Banner source = findOne(id);

		ObjectUtils.copyProperties(parameter, source);

		return save(source);
	}

	public List<Banner> findAll() {
		return repository.findAll();
	}

	public Page<Banner> findBySearch(BannerSearch search, Pageable pageable) {
		return repository.findAll(search.precidate(), pageable);
	}

	public List<Banner> findBySearch(BannerSearch search) {
		return (List<Banner>) repository.findAll(search.precidate(),new Sort(Sort.Direction.ASC, "id"));
	}
}