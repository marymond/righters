package kr.righters.service;

import kr.righters.domain.entity.ProductTab;
import kr.righters.domain.search.ProductTabSearch;
import kr.righters.repository.ProductTabContentRepository;
import kr.righters.util.ObjectUtils;
import kr.righters.util.UniqueIdGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ProductTabService {

	@Autowired
	private UniqueIdGenerator uniqueIdGenerator;

	@Autowired
	private ProductTabContentRepository repository;

	public ProductTab save(ProductTab content) {
		return repository.save(content);
	}

	@Transactional
	public void save(List<ProductTab> contents) {
		repository.save(contents);
	}

	public ProductTab findOne(String id) {
		if (StringUtils.isEmpty(id)) {
			return null;
		}
		if ("new".equals(id)) {
			ProductTab productTab = new ProductTab();
			productTab.setId(uniqueIdGenerator.getStringId());
			return productTab;
		}

		return repository.findOne(id);
	}

	public ProductTab save(String id, ProductTab parameter) {
		ProductTab source = findOne(id);

		ObjectUtils.copyProperties(parameter, source);

		return save(source);
	}

	public List<ProductTab> findAll() {
		return repository.findAll();
	}

	public Page<ProductTab> findBySearch(ProductTabSearch search, Pageable pageable) {
		return repository.findAll(search.precidate(), pageable);
	}

	public List<ProductTab> findBySearch(ProductTabSearch search, Sort sort) {
		return (List<ProductTab>) repository.findAll(search.precidate(), sort);
	}
}