package kr.righters.service;

import kr.righters.domain.entity.Donation;
import kr.righters.domain.entity.User;
import kr.righters.domain.entity.UserDonationHistory;
import kr.righters.domain.entity.UserMileageHistory;
import kr.righters.domain.search.DonationSearch;
import kr.righters.repository.DonationRepository;
import kr.righters.util.ObjectUtils;
import kr.righters.util.UniqueIdGenerator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

@Slf4j
@Service
public class DonationService {
	@Autowired
	private UniqueIdGenerator uniqueIdGenerator;

	@Autowired
	private DonationRepository repository;

	@Autowired
	private RowLockService rowLockService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserDonationHistoryService donationHistoryService;

	@Autowired
	private UserMileageHistoryService userMileageHistoryService;

	public Donation save(Donation content) {
		return repository.save(content);
	}

	@Transactional
	public void save(List<Donation> contents) {
		repository.save(contents);
	}

	public Donation findOne(String id) {
		if (StringUtils.isEmpty(id)) {
			return null;
		}
		if ("new".equals(id)) {
			Donation donation = new Donation();
			donation.setId(uniqueIdGenerator.getStringId());
			return donation;
		}

		return repository.findOne(id);
	}

	public Donation save(String id, Donation parameter) {
		Donation source = findOne(id);

		ObjectUtils.copyProperties(parameter, source);

		return save(source);
	}

	public List<Donation> findAll() {
		return repository.findAll();
	}

	public Page<Donation> findBySearch(DonationSearch search, Pageable pageable) {
		return repository.findAll(search.precidate(), pageable);
	}

	public List<Donation> findBySearch(DonationSearch search, Sort sort) {
		return (List<Donation>) repository.findAll(search.precidate(), sort);
	}

	public Donation saveBalance(Donation donation, Long balance) {
		try {
			rowLockService.lock();

			// 한번에 하나만 진행 가능한 로직 진행
			BigDecimal beforeBalance = new BigDecimal(donation.getBalanceMileage());
			BigDecimal beforeTotalMileage = new BigDecimal(donation.getTotalMileage());
			BigDecimal addBalance    = new BigDecimal(balance);

			donation.setBalanceMileage(beforeBalance.add(addBalance).toPlainString());
			donation.setTotalMileage(beforeTotalMileage.add(addBalance).toPlainString());

			this.save(donation);

			return donation;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
			return null;
		}
	}

	@Transactional
	public void saveDonation(User user, Donation donation, UserDonationHistory donationHistory){
		//유저 마일리지사용 처리
		Long beforeUserMileage = user.getBalanceMileage();
		Long afterUserMileage = userService.saveBalance(user, donationHistory.getAmount(), "withdraw", true);
		UserMileageHistory userMileageHistory = new UserMileageHistory();
		userMileageHistory.setId(uniqueIdGenerator.getStringId());
		userMileageHistory.setUserId(user.getId());
		userMileageHistory.setBeforeMileage(beforeUserMileage);
		userMileageHistory.setAfterMileage(afterUserMileage);
		userMileageHistory.setAmount(-donationHistory.getAmount());
		userMileageHistory.setReason("Donation used");

		userMileageHistoryService.save(userMileageHistory);

		//기부관련 처리
		this.saveBalance(donation, donationHistory.getAmount());
		donationHistory.setId(uniqueIdGenerator.getStringId());
		donationHistory.setUserId(user.getId());

		donationHistoryService.save(donationHistory);
	}
}