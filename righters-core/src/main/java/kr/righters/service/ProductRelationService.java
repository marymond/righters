package kr.righters.service;

import kr.righters.domain.entity.ProductRelation;
import kr.righters.domain.search.ProductRelationSearch;
import kr.righters.repository.ProductRelationRepository;
import kr.righters.util.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ProductRelationService {
	@Autowired
	private ProductRelationRepository repository;

	public ProductRelation save(ProductRelation content) {
		return repository.save(content);
	}

	@Transactional
	public void save(List<ProductRelation> contents) {
		repository.save(contents);
	}

	public ProductRelation findOne(String id) {
		if (StringUtils.isEmpty(id)) {
			return null;
		}
		if ("new".equals(id)) {
			return new ProductRelation();
		}

		return repository.findOne(id);
	}

	public ProductRelation save(String id, ProductRelation parameter) {
		ProductRelation source = findOne(id);

		ObjectUtils.copyProperties(parameter, source);

		return save(source);
	}

	public List<ProductRelation> findAll() {
		return repository.findAll();
	}

	public Page<ProductRelation> findBySearch(ProductRelationSearch search, Pageable pageable) {
		return repository.findAll(search.precidate(), pageable);
	}

	public List<ProductRelation> findBySearch(ProductRelationSearch search, Sort sort) {
		return (List<ProductRelation>) repository.findAll(search.precidate(), sort);
	}
}