package kr.righters.service;

import kr.righters.domain.entity.Shipping;
import kr.righters.domain.search.ShippingSearch;
import kr.righters.repository.ShippingRepository;
import kr.righters.util.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ShippingService {
	@Autowired
	private ShippingRepository repository;

	public Shipping save(Shipping content) {
		return repository.save(content);
	}

	@Transactional
	public void save(List<Shipping> contents) {
		repository.save(contents);
	}

	public Shipping findOne(String id) {
		if (StringUtils.isEmpty(id)) {
			return null;
		}
		if ("new".equals(id)) {
			return new Shipping();
		}

		return repository.findOne(id);
	}

	public Shipping save(String id, Shipping parameter) {
		Shipping source = findOne(id);

		ObjectUtils.copyProperties(parameter, source);

		return save(source);
	}

	public List<Shipping> findAll() {
		return repository.findAll();
	}

	public Page<Shipping> findBySearch(ShippingSearch search, Pageable pageable) {
		return repository.findAll(search.precidate(), pageable);
	}

	public List<Shipping> findBySearch(ShippingSearch search, Sort sort) {
		return (List<Shipping>) repository.findAll(search.precidate(), sort);
	}
}