package kr.righters.service;

import kr.righters.domain.entity.UserDonationHistory;
import kr.righters.domain.search.UserDonationHistorySearch;
import kr.righters.repository.UserDonationHistoryRepository;
import kr.righters.util.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class UserDonationHistoryService {
	@Autowired
	private UserDonationHistoryRepository repository;

	public UserDonationHistory save(UserDonationHistory content) {
		return repository.save(content);
	}

	@Transactional
	public void save(List<UserDonationHistory> contents) {
		repository.save(contents);
	}

	public UserDonationHistory findOne(String id) {
		if (StringUtils.isEmpty(id)) {
			return null;
		}
		if ("new".equals(id)) {
			return new UserDonationHistory();
		}

		return repository.findOne(id);
	}

	public UserDonationHistory save(String id, UserDonationHistory parameter) {
		UserDonationHistory source = findOne(id);

		ObjectUtils.copyProperties(parameter, source);

		return save(source);
	}

	public List<UserDonationHistory> findAll() {
		return repository.findAll();
	}

	public Page<UserDonationHistory> findBySearch(UserDonationHistorySearch search, Pageable pageable) {
		return repository.findAll(search.precidate(), pageable);
	}

	public List<UserDonationHistory> findBySearch(UserDonationHistorySearch search, Sort sort) {
		return (List<UserDonationHistory>) repository.findAll(search.precidate(), sort);
	}
}