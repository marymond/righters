package kr.righters.service;

import kr.righters.domain.entity.OrderedProduct;
import kr.righters.domain.search.OrderedProductSearch;
import kr.righters.repository.OrderedProductRepository;
import kr.righters.util.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class OrderedProductService {
	@Autowired
	private OrderedProductRepository repository;

	public OrderedProduct save(OrderedProduct content) {
		return repository.save(content);
	}

	@Transactional
	public void save(List<OrderedProduct> contents) {
		repository.save(contents);
	}

	public OrderedProduct findOne(String id) {
		if (StringUtils.isEmpty(id)) {
			return null;
		}
		if ("new".equals(id)) {
			return new OrderedProduct();
		}

		return repository.findOne(id);
	}

	public OrderedProduct save(String id, OrderedProduct parameter) {
		OrderedProduct source = findOne(id);

		ObjectUtils.copyProperties(parameter, source);

		return save(source);
	}

	public List<OrderedProduct> findAll() {
		return repository.findAll();
	}

	public Page<OrderedProduct> findBySearch(OrderedProductSearch search, Pageable pageable) {
		return repository.findAll(search.precidate(), pageable);
	}

	public List<OrderedProduct> findBySearch(OrderedProductSearch search, Sort sort) {
		return (List<OrderedProduct>) repository.findAll(search.precidate(), sort);
	}
}