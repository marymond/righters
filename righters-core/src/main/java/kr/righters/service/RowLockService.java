package kr.righters.service;

import kr.righters.repository.RowLockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class RowLockService {
	@Autowired
	private RowLockRepository repository;

	public void lock() {
		repository.findOne("00015813251650900a9a41b625ae0001");
	}

}