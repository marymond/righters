package kr.righters.service;

import kr.righters.domain.entity.Inquiry;
import kr.righters.domain.search.InquirySearch;
import kr.righters.repository.InquiryRepository;
import kr.righters.util.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class InquiryService {
	@Autowired
	private InquiryRepository repository;

	public Inquiry save(Inquiry content) {
		return repository.save(content);
	}

	@Transactional
	public void save(List<Inquiry> contents) {
		repository.save(contents);
	}

	public Inquiry findOne(String id) {
		if (StringUtils.isEmpty(id)) {
			return null;
		}
		if ("new".equals(id)) {
			return new Inquiry();
		}

		return repository.findOne(id);
	}

	public Inquiry save(String id, Inquiry parameter) {
		Inquiry source = findOne(id);

		ObjectUtils.copyProperties(parameter, source);

		return save(source);
	}

	public List<Inquiry> findAll() {
		return repository.findAll();
	}

	public Page<Inquiry> findBySearch(InquirySearch search, Pageable pageable) {
		return repository.findAll(search.precidate(), pageable);
	}

	public List<Inquiry> findBySearch(InquirySearch search, Sort sort) {
		return (List<Inquiry>) repository.findAll(search.precidate(), sort);
	}
}