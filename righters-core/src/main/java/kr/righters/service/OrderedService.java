package kr.righters.service;

import com.google.common.collect.Lists;
import kr.righters.domain.entity.*;
import kr.righters.domain.search.CartSearch;
import kr.righters.domain.search.OrderedSearch;
import kr.righters.domain.search.UserSearch;
import kr.righters.exception.CustomException;
import kr.righters.exception.CustomJsonException;
import kr.righters.manager.iamport.IamportManager;
import kr.righters.manager.iamport.vo.PaymentResponseVO;
import kr.righters.manager.iamport.vo.common.IamportResponse;
import kr.righters.repository.OrderedProductRepository;
import kr.righters.repository.OrderedRepository;
import kr.righters.util.ObjectUtils;
import kr.righters.util.RandomStringGenerator;
import kr.righters.util.UniqueIdGenerator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class OrderedService {
	@Autowired
	private OrderedRepository repository;

	@Autowired
	private OrderedProductRepository orderedProductRepository;

	@Autowired
	private UniqueIdGenerator uniqueIdGenerator;

	@Autowired
	private RandomStringGenerator randomStringGenerator;

	@Autowired
	private CartService cartService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserMileageHistoryService userMileageHistoryService;

	@Autowired
	private IamportManager iamportManager;

	public Ordered save(Ordered content) {
		return repository.save(content);
	}

	@Transactional
	public void save(List<Ordered> contents) {
		repository.save(contents);
	}

	public Ordered findOne(String id) {
		if (StringUtils.isEmpty(id)) {
			return null;
		}
		if ("new".equals(id)) {
			return new Ordered();
		}

		return repository.findOne(id);
	}

	public Ordered save(String id, Ordered parameter) {
		Ordered source = findOne(id);

		ObjectUtils.copyProperties(parameter, source);

		return save(source);
	}

	public List<Ordered> findAll() {
		return repository.findAll();
	}

	public Page<Ordered> findBySearch(OrderedSearch search, Pageable pageable) {
		return repository.findAll(search.precidate(), pageable);
	}

	public List<Ordered> findBySearch(OrderedSearch search, Sort sort) {
		return (List<Ordered>) repository.findAll(search.precidate(), sort);
	}

	public void setUserInfo(List<Ordered> ordereds) {
		List<String> userIds = new ArrayList<>();
		for(Ordered ordered : ordereds){
			userIds.add(ordered.getUserId());
			if(StringUtils.isNotEmpty(ordered.getShareUserId())){
				userIds.add(ordered.getShareUserId());
			}
		}

		UserSearch userSearch = new UserSearch();
		userSearch.setUserIds(userIds);

		List<User>        users   = userService.findBySearch(userSearch, new Sort(Sort.Direction.DESC, "id"));
		Map<String, User> userMap = new HashMap<>();
		for(User user : users){
			userMap.put(user.getId(), user);
		}

		for(Ordered ordered : ordereds){
			ordered.setUser(userMap.get(ordered.getUserId()));
			if(StringUtils.isNotEmpty(ordered.getShareUserId())){
				ordered.setShareUser(userMap.get(ordered.getShareUserId()));
			}
		}

	}

	/**
	 *
	 * @param carts
	 * @param shippingAddress
	 * @param user
	 * @param buynowFlag
	 * @return
	 */
	public String createMerchanUid(List<Cart> carts, UserAddress shippingAddress, User user, boolean buynowFlag){
		BigDecimal                         totalSalePrice   = new BigDecimal("0");
		BigDecimal                         totalShippingFee = new BigDecimal("0");
		for(Cart cart : carts){
			BigDecimal salePrice = new BigDecimal(cart.getProduct().getSalePrice());

			//총합계산
			BigDecimal quantityBig = new BigDecimal(cart.getQuantity());
			totalSalePrice = totalSalePrice.add(salePrice.multiply(quantityBig));
			totalShippingFee = totalShippingFee.add(new BigDecimal(cart.getProduct().getShipping().getShippingFee()));

		}

		//Order 저장처리
		return this.createOrder(Ordered.PaymentType.IAMPORT, carts, shippingAddress, totalSalePrice.longValue(),totalShippingFee.longValue(), totalSalePrice.add(totalShippingFee).longValue(),null, user, buynowFlag);
	}

	/**
	 * 주문장생성
	 * @param paymentType
	 * @param carts
	 * @param shippingAddress
	 * @param totalProductAmount
	 * @param totalShippingFee
	 * @param totalAmount
	 * @param pgOrderId
	 * @param user
	 * @param buynowFlag
	 */
	@Transactional
	public String createOrder(Ordered.PaymentType paymentType, List<Cart> carts, UserAddress shippingAddress, Long totalProductAmount, Long totalShippingFee, Long totalAmount, String pgOrderId, User user, boolean buynowFlag){
		//주문서 등록
		Ordered ordered = new Ordered();
		String orderedId = uniqueIdGenerator.getStringId();
		ordered.setId(orderedId);
		ordered.setUserId(user.getId());
		ordered.setOrderNumber(randomStringGenerator.generateOrderNumber());
		ordered.setStatus(Ordered.Status.BEFORE_PAYMENT);
		ordered.setPgOrderId(pgOrderId == null ? orderedId : pgOrderId); //PG에서 만든 트랜잭션ID 성격의 ID(아이엠포트의 경우 MerchantUid로 설정)
		ordered.setTransationId(orderedId); //가맹점에서 만든 트랜잭션ID(아이엠포트의 MerchantUid)
		ordered.setTotalAmount(totalAmount);
		ordered.setTotalProductAmount(totalProductAmount);
		ordered.setTotalShippingFee(totalShippingFee);
		ordered.setBuyerFullName(shippingAddress.getFullName());
		ordered.setBuyerAddress1(shippingAddress.getAddress1());
		ordered.setBuyerAddress2(shippingAddress.getAddress2());
		ordered.setBuyerCity(shippingAddress.getCity());
		ordered.setBuyerState(shippingAddress.getState());
		ordered.setBuyerZipCode(shippingAddress.getZipCode());
		ordered.setBuyerCountry(shippingAddress.getCountry());
		ordered.setBuyerPhoneNumber(shippingAddress.getPhoneNumber());
		ordered.setBuyerDeliveryInstructions(shippingAddress.getDeliveryInstructions());
		ordered.setPaymentType(paymentType);
		ordered.setBuynowFlag(buynowFlag ? Ordered.BuynowFlag.TRUE : Ordered.BuynowFlag.FALSE);

		List<OrderedProduct> orderedProducts = Lists.newArrayList();
		for (Cart cart : carts) {
 			if(cart.getProduct().getShareUid() != null){
				//공유유저가 있을경우
				UserSearch userSearch = new UserSearch();
				userSearch.setUid(cart.getProduct().getShareUid());
				userSearch.setStatus(User.Status.USED); //가입유저만

				List<User> users = userService.findBySearch(userSearch, new Sort(Sort.Direction.ASC, "id"));
				if(users != null && users.size() > 0){
					ordered.setShareUserFullName(user.getFullName());
					ordered.setShareUserId(user.getId());
					BigDecimal productSalePriceBig = new BigDecimal(cart.getProduct().getSalePrice());
					BigDecimal productShareRewardRate = new BigDecimal(cart.getProduct().getShareRewardRate());
					BigDecimal quantityBig = new BigDecimal(cart.getQuantity());

					BigDecimal shareRewardAmountBig = productSalePriceBig.multiply(quantityBig).multiply(productShareRewardRate).divide(new BigDecimal("100"),2, BigDecimal.ROUND_DOWN);

					ordered.setShareUserRewardMileage(shareRewardAmountBig.longValue());
				}
			}
			OrderedProduct orderedProduct = new OrderedProduct();
			orderedProduct.setId(uniqueIdGenerator.getStringId());
			orderedProduct.setOrderId(orderedId);
			orderedProduct.setProductId(cart.getProductId());
			orderedProduct.setProductOptionId(cart.getProductOptionId());
			orderedProduct.setProductName(cart.getProduct().getName());
			orderedProduct.setProductMainImageUrl(cart.getProduct().getMainImageUrl());
			orderedProduct.setProductOptionName(cart.getProductOption().getName());
			orderedProduct.setProductPrice(cart.getProduct().getPrice());
			orderedProduct.setProductSalePrice(cart.getProduct().getSalePrice());
			orderedProduct.setProductSaleRate(cart.getProduct().getSaleRate());
			orderedProduct.setProductShareRewardRate(cart.getProduct().getShareRewardRate());
			orderedProduct.setQuantity(cart.getQuantity());
			orderedProduct.setShippingId(cart.getProduct().getShipping().getId());
			orderedProduct.setShippingName(cart.getProduct().getShipping().getName());
			orderedProduct.setShippingFee(cart.getProduct().getShipping().getShippingFee());

			orderedProducts.add(orderedProduct);
		}

		repository.save(ordered);
		orderedProductRepository.save(orderedProducts);

		return ordered.getId();
	}

	/**
	 * 아이엠포트 검증
	 * @param merchantUid
	 */
	public boolean verifyCheckOut(String merchantUid, String impUid){
		OrderedSearch orderedSearch = new OrderedSearch();
		orderedSearch.setId(merchantUid); //merchantUid가 ordered 테이블 ID
		orderedSearch.setStatus(Ordered.Status.BEFORE_PAYMENT);

		List<Ordered> ordereds = this.findBySearch(orderedSearch, new Sort(Sort.Direction.DESC,"id"));

		boolean verifyFlag = true;

		if(ordereds.size() > 0) {
			//결제내역이 존재하는 경우
			IamportResponse<PaymentResponseVO> paymentResponse = null;
			try {
				paymentResponse = iamportManager.getPaymentImpUid(impUid);
			} catch (Exception e) {
				throw new CustomJsonException("결제내역 검증중 문제가 발생하였습니다. 잠시후 구매내역을 확인해주세요.");
			}

			log.info("{}", paymentResponse);
			if(paymentResponse.getMessage() != null){
				throw new CustomJsonException(paymentResponse.getMessage());
			}

			if(!"paid".equals(paymentResponse.getResponse().getStatus())){
				//결제완료 이외의 상태의 경우 결제 실패로 처리
				this.checkOutFail(merchantUid); //결제 실패 처리
				return false;
			}

			if(!ordereds.get(0).getTotalAmount().equals(paymentResponse.getResponse().getAmount())){
				//결제된 금액과 실재 결제되어야할 금액이 다른경우
				return false;
			}
		}else{
			return false;
		}

		return verifyFlag;
	}

	/**
	 * 결제성공처리
	 * @param pgOrderId
	 * @param pgReceiptId
	 */
	@Transactional
	public void checkOutComplete(String pgOrderId, String pgReceiptId){
		//order정보 업데이트
		OrderedSearch orderedSearch = new OrderedSearch();
		orderedSearch.setId(pgOrderId); //merchantUid가 ordered 테이블 ID
//		orderedSearch.setPgOrderId(pgOrderId);
		orderedSearch.setStatus(Ordered.Status.BEFORE_PAYMENT);

		List<Ordered> ordereds = this.findBySearch(orderedSearch, new Sort(Sort.Direction.DESC,"id"));

		if(ordereds.size() > 0) {
			Ordered ordered = ordereds.get(0); //가장최근 주문취득
			ordered.setStatus(Ordered.Status.SUCCESS_PAYMENT);
			ordered.setPgReceiptId(pgReceiptId);
			ordered.setOrderDate(LocalDateTime.now());

			List<OrderedProduct> orderedProducts = ordered.getOrderedProducts();
			for (OrderedProduct orderedProduct : orderedProducts) {
				orderedProduct.setStatus(OrderedProduct.Status.SUCCESS_PAYMENT); //결제완료 상태
				orderedProduct.setShippingStatus(OrderedProduct.ShippingStatus.REVIEWING); //배송상태는 검토중
			}

			//cart 삭제
			if(Ordered.BuynowFlag.FALSE.equals(ordered.getBuynowFlag())){

				//카트구매의 경우 카트 삭제
				CartSearch cartSearch = new CartSearch();
				cartSearch.setStatus(Cart.Status.BEFORE_PAYMENT);
				cartSearch.setUserId(ordered.getUserId());

				List<Cart> carts = cartService.findBySearch(cartSearch, new Sort(Sort.Direction.DESC, "id"));

				cartService.deleteList(carts);
			}
			//TODO (어드민에서 배송준비 상태로 변경시 처리) 공유유저 리워드
			boolean sharalbeFlag = true;
			User user = userService.findOne(ordered.getUserId());
			if(ordered.getUserId().equals(ordered.getShareUserId()) && UserAddress.Flag.FALSE.equals(user.getSelfRewardFlag())){
				//공유유저와 구입유저가 같고 자기자신 리워드 설정이 false경우 리워드 안함
				sharalbeFlag = false;
			}

			if(ordered.getShareUserId() != null && sharalbeFlag){
				User shareUser = userService.findOne(ordered.getShareUserId());
				Long beforeUserMileage = shareUser.getBalanceMileage();

				Long afterShareUserMileage = userService.saveBalance(shareUser,ordered.getShareUserRewardMileage(),"deposit", false);
				UserMileageHistory userMileageHistory = new UserMileageHistory();
				userMileageHistory.setId(uniqueIdGenerator.getStringId());
				userMileageHistory.setUserId(shareUser.getId());
				userMileageHistory.setBeforeMileage(beforeUserMileage);
				userMileageHistory.setAfterMileage(afterShareUserMileage);
				userMileageHistory.setAmount(ordered.getShareUserRewardMileage());
				userMileageHistory.setReason("Share Product Saving");

				userMileageHistoryService.save(userMileageHistory);
			}

			//db처리
			repository.save(ordered);
			orderedProductRepository.save(orderedProducts);

		}
	}

	/**
	 * 결제실패 처리
	 * @param pgOrderId
	 */
	@Transactional
	public void checkOutFail(String pgOrderId){
		//order정보 업데이트
		OrderedSearch orderedSearch = new OrderedSearch();
		orderedSearch.setPgOrderId(pgOrderId);
		orderedSearch.setStatus(Ordered.Status.BEFORE_PAYMENT);

		List<Ordered> ordereds = this.findBySearch(orderedSearch, new Sort(Sort.Direction.DESC,"id"));

		if(ordereds.size() > 0) {
			Ordered ordered = ordereds.get(0); //가장최근 주문취득
			ordered.setStatus(Ordered.Status.FAIL_PAYMENT);

			List<OrderedProduct> orderedProducts = ordered.getOrderedProducts();
			for (OrderedProduct orderedProduct : orderedProducts) {
				orderedProduct.setStatus(OrderedProduct.Status.FAIL_PAYMENT); //결제실패 상태
			}

			repository.save(ordered);
			orderedProductRepository.save(orderedProducts);
		}

	}

	/**
	 * 환불처리
	 * @param pgRecieiptId
	 */
	@Transactional
	public void refund(String pgRecieiptId){
		//order정보 업데이트
		OrderedSearch orderedSearch = new OrderedSearch();
		orderedSearch.setPgReceiptId(pgRecieiptId);
		orderedSearch.setStatus(Ordered.Status.SUCCESS_PAYMENT);

		List<Ordered> ordereds = this.findBySearch(orderedSearch, new Sort(Sort.Direction.DESC,"id"));

		if(ordereds.size() > 0) {
			Ordered ordered = ordereds.get(0); //가장최근 주문취득
			ordered.setStatus(Ordered.Status.REFUNDED);

			List<OrderedProduct> orderedProducts = ordered.getOrderedProducts();
			for (OrderedProduct orderedProduct : orderedProducts) {
				orderedProduct.setStatus(OrderedProduct.Status.REFUNDED); //결제실패 상태
				orderedProduct.setCancleDate(LocalDateTime.now());
			}

			repository.save(ordered);
			orderedProductRepository.save(orderedProducts);
		}

	}
}