package kr.righters.service;

import kr.righters.domain.entity.UserAddress;
import kr.righters.domain.search.UserAddressSearch;
import kr.righters.exception.CustomJsonException;
import kr.righters.repository.UserAddressRepository;
import kr.righters.util.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class UserAddressService {
	@Autowired
	private UserAddressRepository repository;

	public UserAddress save(UserAddress content) {
		return repository.save(content);
	}

	@Transactional
	public void save(List<UserAddress> contents) {
		repository.save(contents);
	}

	public UserAddress findOne(String id) {
		if (StringUtils.isEmpty(id)) {
			return null;
		}
		if ("new".equals(id)) {
			return new UserAddress();
		}

		return repository.findOne(id);
	}

	public UserAddress save(String id, UserAddress parameter) {
		UserAddress source = findOne(id);

		ObjectUtils.copyProperties(parameter, source);

		return save(source);
	}

	public List<UserAddress> findAll() {
		return repository.findAll();
	}

	public Page<UserAddress> findBySearch(UserAddressSearch search, Pageable pageable) {
		return repository.findAll(search.precidate(), pageable);
	}

	public List<UserAddress> findBySearch(UserAddressSearch search, Sort sort) {
		return (List<UserAddress>) repository.findAll(search.precidate(), sort);
	}

	public void delete(UserAddress userAddress){
		repository.delete(userAddress);
	}


	/**
	 * 배송 입력사항 체크
	 * @param shippingAddress
	 */
	public void check(UserAddress shippingAddress){
		if (StringUtils.isEmpty(shippingAddress.getFullName())) {
			throw new CustomJsonException("이름을 입력해 주세요.");
		}
		if (StringUtils.isEmpty(shippingAddress.getAddress1())) {
			throw new CustomJsonException("주소를 입력해 주세요.");
		}

		if (StringUtils.isEmpty(shippingAddress.getAddress2())) {
			throw new CustomJsonException("상세주소를 입력해 주세요.");
		}
//		if (StringUtils.isEmpty(shippingAddress.getCity())) {
//			throw new CustomJsonException("Input your City, please.");
//		}
//		if (StringUtils.isEmpty(shippingAddress.getState())) {
//			throw new CustomJsonException("Input your State, please.");
//		}
		if (StringUtils.isEmpty(shippingAddress.getZipCode())) {
			throw new CustomJsonException("우편번호를 입력해 주세요.");
		}
//		if (StringUtils.isEmpty(shippingAddress.getCountry())) {
//			throw new CustomJsonException("Input your Country or Region, please.");
//		}
		if (StringUtils.isEmpty(shippingAddress.getPhoneNumber())) {
			throw new CustomJsonException("전화번호를 입력해 주세요.");
		}

		//전화번호 형식 체크(외국전화번호 체크용)
		String phoneNumber      = shippingAddress.getPhoneNumber().replaceAll("-", "");
		String zipCode = shippingAddress.getZipCode().replaceAll("-", "");
		String mobileNumberRegex = "^[0-9]*$";//숫자만 있는지 확인

		if (!phoneNumber.matches(mobileNumberRegex)) {
			throw new CustomJsonException("전화번호는 숫자만 입력할 수 있습니다.");
		}

		if (!zipCode.matches(mobileNumberRegex)) {
			throw new CustomJsonException("우변번호는 숫자만 입력할 수 있습니다.");
		}

		if(shippingAddress.getFullName().length() > 100){
			throw new CustomJsonException("100자 이상의 이름은 입력할 수 없습니다.");
		}
		if(shippingAddress.getAddress1().length() > 100){
			throw new CustomJsonException("100자 이상의 주소는 입력할 수 없습니다.");
		}
		if(shippingAddress.getAddress2().length() > 100){
			throw new CustomJsonException("100자 이상의 상세주소는 입력할 수 없습니다.");
		}

//		if(shippingAddress.getCity().length() > 30){
//			throw new CustomJsonException("You cannot enter more than 30 digits for the City.");
//		}
//		if(shippingAddress.getState().length() > 30){
//			throw new CustomJsonException("You cannot enter more than 30 digits for the State.");
//		}
//		if(shippingAddress.getCountry().length() > 30){
//			throw new CustomJsonException("You cannot enter more than 30 digits for the Country/Region.");
//		}
		if(phoneNumber.length() > 20){
			throw new CustomJsonException("20자이상의 전화번호는 입력할 수 없습니다.");
		}
		if(zipCode.length() > 20){
			throw new CustomJsonException("20자이상의 우편번호는 입력할 수 없습니다.");
		}

	}
}