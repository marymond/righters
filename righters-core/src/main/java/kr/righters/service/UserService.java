package kr.righters.service;

import kr.righters.domain.entity.User;
import kr.righters.domain.search.UserSearch;
import kr.righters.repository.UserRepository;
import kr.righters.util.ObjectUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

@Slf4j
@Service
public class UserService {
	@Autowired
	private UserRepository repository;

	@Autowired
	private RowLockService rowLockService;

	public User save(User content) {
		return repository.save(content);
	}

	@Transactional
	public void save(List<User> contents) {
		repository.save(contents);
	}

	public User findOne(String id) {
		if (StringUtils.isEmpty(id)) {
			return null;
		}
		if ("new".equals(id)) {
			return new User();
		}

		return repository.findOne(id);
	}

	public User save(String id, User parameter) {
		User source = findOne(id);

		ObjectUtils.copyProperties(parameter, source);

		return save(source);
	}

	public List<User> findAll() {
		return repository.findAll();
	}

	public Page<User> findBySearch(UserSearch search, Pageable pageable) {
		return repository.findAll(search.precidate(), pageable);
	}

	public List<User> findBySearch(UserSearch search, Sort sort) {
		return (List<User>) repository.findAll(search.precidate(),sort);
	}

	public Long saveBalance(User user, Long amount, String type, boolean donationFlg) {
		try {
			rowLockService.lock();

			// 한번에 하나만 진행 가능한 로직 진행
			BigDecimal beforeBalance = new BigDecimal(user.getBalanceMileage());
			BigDecimal beforeDonationMileage = new BigDecimal(user.getDonationMileage());
			BigDecimal mileage = new BigDecimal(amount);

			Long afterBalance = null;
			if("deposit".equals(type)){
				afterBalance = beforeBalance.add(mileage).longValue();
			}else{
				afterBalance = beforeBalance.subtract(mileage).longValue();
			}

			user.setBalanceMileage(afterBalance);
			if (donationFlg){
				user.setDonationMileage(beforeDonationMileage.add(mileage).longValue());
			}

			this.save(user);

			return afterBalance;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
			return null;
		}
	}
}