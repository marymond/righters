package kr.righters.service;

import kr.righters.domain.entity.Product;
import kr.righters.domain.search.ProductSearch;
import kr.righters.repository.ProductRepository;
import kr.righters.util.ObjectUtils;
import kr.righters.util.UniqueIdGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

@Service
public class ProductService {
	@Autowired
	private ProductRepository repository;
	@Autowired
	private UniqueIdGenerator uniqueIdGenerator;

	public Product save(Product content) {
		//세일가격 계산
		this.calculateData(content);

		return repository.save(content);
	}

	@Transactional
	public void save(List<Product> contents) {
		repository.save(contents);
	}

	public Product findOne(String id) {
		if (StringUtils.isEmpty(id)) {
			return null;
		}
		if ("new".equals(id)) {
			Product product = new Product();
			product.setId(uniqueIdGenerator.getStringId());

			return product;
		}

		return repository.findOne(id);
	}

	public Product save(String id, Product parameter) {
		Product source = findOne(id);

		ObjectUtils.copyProperties(parameter, source);

		return save(source);
	}

	public List<Product> findAll() {
		return repository.findAll();
	}

	public Page<Product> findBySearch(ProductSearch search, Pageable pageable) {
		return repository.findAll(search.precidate(), pageable);
	}

	public List<Product> findBySearch(ProductSearch search, Sort sort) {
		return (List<Product>) repository.findAll(search.precidate(), sort);
	}

	private void calculateData(Product product){
		if(product.getPrice() == null || product.getSaleRate() == null){
			return;
		}
		BigDecimal priceBig = new BigDecimal(product.getPrice());
		BigDecimal saleRateBig = new BigDecimal(product.getSaleRate());

		BigDecimal salePriceBig = priceBig.multiply(saleRateBig).divide(new BigDecimal(100));


		product.setSalePrice(priceBig.subtract(salePriceBig).longValue());
	}
}