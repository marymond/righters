package kr.righters.service;

import kr.righters.domain.entity.ProductCategory;
import kr.righters.domain.search.ProductCategorySearch;
import kr.righters.repository.ProductCategoryRepository;
import kr.righters.util.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ProductCategoryService {
	@Autowired
	private ProductCategoryRepository repository;

	public ProductCategory save(ProductCategory content) {
		return repository.save(content);
	}

	@Transactional
	public void save(List<ProductCategory> contents) {
		repository.save(contents);
	}

	public ProductCategory findOne(String id) {
		if (StringUtils.isEmpty(id)) {
			return null;
		}
		if ("new".equals(id)) {
			ProductCategory productCategory = new ProductCategory();
			productCategory.setStatus(ProductCategory.Status.HIDE);
			return productCategory;
		}

		return repository.findOne(id);
	}

	public ProductCategory save(String id, ProductCategory parameter) {
		ProductCategory source = findOne(id);

		ObjectUtils.copyProperties(parameter, source);

		return save(source);
	}

	public List<ProductCategory> findAll() {
		return repository.findAll();
	}

	public Page<ProductCategory> findBySearch(ProductCategorySearch search, Pageable pageable) {
		return repository.findAll(search.precidate(), pageable);
	}

	public List<ProductCategory> findBySearch(ProductCategorySearch search, Sort sort) {
		return (List<ProductCategory>) repository.findAll(search.precidate(), sort);
	}
}