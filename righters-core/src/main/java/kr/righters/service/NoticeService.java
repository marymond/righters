package kr.righters.service;

import kr.righters.domain.entity.Notice;
import kr.righters.domain.search.NoticeSearch;
import kr.righters.repository.NoticeRepository;
import kr.righters.util.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class NoticeService {
	@Autowired
	private NoticeRepository repository;

	public Notice save(Notice content) {
		return repository.save(content);
	}

	@Transactional
	public void save(List<Notice> contents) {
		repository.save(contents);
	}

	public Notice findOne(String id) {
		if (StringUtils.isEmpty(id)) {
			return null;
		}
		if ("new".equals(id)) {
			return new Notice();
		}

		return repository.findOne(id);
	}

	public Notice save(String id, Notice parameter) {
		Notice source = findOne(id);

		ObjectUtils.copyProperties(parameter, source);

		return save(source);
	}

	public List<Notice> findAll() {
		return repository.findAll();
	}

	public Page<Notice> findBySearch(NoticeSearch search, Pageable pageable) {
		return repository.findAll(search.precidate(), pageable);
	}

	public List<Notice> findBySearch(NoticeSearch search, Sort sort) {
		return (List<Notice>) repository.findAll(search.precidate(), sort);
	}
}