package kr.righters.service;

import kr.righters.domain.entity.AboutDetail;
import kr.righters.domain.search.AboutDetailSearch;
import kr.righters.repository.AboutDetailRepository;
import kr.righters.util.ObjectUtils;
import kr.righters.util.UniqueIdGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class AboutDetailService {

	@Autowired
	private UniqueIdGenerator uniqueIdGenerator;

	@Autowired
	private AboutDetailRepository repository;

	public AboutDetail save(AboutDetail content) {
		return repository.save(content);
	}

	@Transactional
	public void save(List<AboutDetail> contents) {
		repository.save(contents);
	}

	public AboutDetail findOne(String id) {
		if (StringUtils.isEmpty(id)) {
			return null;
		}
		if ("new".equals(id)) {
			AboutDetail aboutDetail = new AboutDetail();
			aboutDetail.setId(uniqueIdGenerator.getStringId());
			return aboutDetail;
		}

		return repository.findOne(id);
	}

	public AboutDetail save(String id, AboutDetail parameter) {
		AboutDetail source = findOne(id);

		ObjectUtils.copyProperties(parameter, source);

		return save(source);
	}

	public List<AboutDetail> findAll() {
		return repository.findAll();
	}

	public Page<AboutDetail> findBySearch(AboutDetailSearch search, Pageable pageable) {
		return repository.findAll(search.precidate(), pageable);
	}

	public List<AboutDetail> findBySearch(AboutDetailSearch search, Sort sort) {
		return (List<AboutDetail>) repository.findAll(search.precidate(), sort);
	}
}