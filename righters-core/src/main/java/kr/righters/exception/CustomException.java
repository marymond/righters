package kr.righters.exception;

import kr.righters.domain.common.StatusEnum;

/**
 * Created by yj.nam on 19.10.29..
 */
public class CustomException extends RuntimeException {


    private static final long serialVersionUID = -717614801597398740L;

    private StatusEnum result = StatusEnum.ERROR;
    private String     message;

    public CustomException() {
        super();
        this.result = StatusEnum.ERROR;
        this.message = StatusEnum.ERROR.getDescription();
    }
    
    public CustomException(StatusEnum statusEnum) {
        super();
        this.result = statusEnum;
        this.message = statusEnum.getDescription();
    }

    public CustomException(String message) {
        super();
        this.result = StatusEnum.FAILED;
        this.message = message;
    }

    public CustomException(StatusEnum statusEnum, String message) {
        super();
        this.result = statusEnum;
        this.message = message;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(StatusEnum result) {
        this.result = result;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
