package kr.righters.exception;

import kr.righters.domain.common.StatusEnum;

/**
 * Created by yj.nam on 19.12.05..
 */
public class CustomJsonException extends RuntimeException {


    private static final long serialVersionUID = -3326772672557537114L;

    private StatusEnum result = StatusEnum.ERROR;
    private String message;

    public CustomJsonException() {
        super();
        this.result = StatusEnum.ERROR;
        this.message = StatusEnum.ERROR.getDescription();
    }

    public CustomJsonException(StatusEnum statusEnum) {
        super();
        this.result = statusEnum;
        this.message = statusEnum.getDescription();
    }

    public CustomJsonException(String message) {
        super();
        this.result = StatusEnum.FAILED;
        this.message = message;
    }

    public CustomJsonException(StatusEnum statusEnum, String message) {
        super();
        this.result = statusEnum;
        this.message = message;
    }

    public StatusEnum getResult() {
        return result;
    }

    public void setResult(StatusEnum result) {
        this.result = result;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
