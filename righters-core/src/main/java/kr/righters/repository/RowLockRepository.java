package kr.righters.repository;


import kr.righters.domain.entity.RowLock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import javax.persistence.LockModeType;

/**
 * Created by yj.nam on 20.02.11..
 */
public interface RowLockRepository extends JpaRepository<RowLock, String>, QueryDslPredicateExecutor {
    @Override
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    RowLock findOne(String id);

}
