package kr.righters.repository;


import kr.righters.domain.entity.Banner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * Created by yj.nam on 19.11.28..
 */
public interface BannerRepository extends JpaRepository<Banner, String>, QueryDslPredicateExecutor {


}
