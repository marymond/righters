package kr.righters.repository;


import kr.righters.domain.entity.AboutDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * Created by yj.nam on 20.10.14..
 */
public interface AboutDetailRepository extends JpaRepository<AboutDetail, String>, QueryDslPredicateExecutor {


}
