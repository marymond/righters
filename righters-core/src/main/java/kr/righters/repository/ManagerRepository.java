package kr.righters.repository;


import kr.righters.domain.entity.Manager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * Created by yj.nam on 19.10.29..
 */
public interface ManagerRepository extends JpaRepository<Manager, String>, QueryDslPredicateExecutor {


}
