package kr.righters.repository;


import kr.righters.domain.entity.ProductTab;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * Created by yj.nam on 19.11.29..
 */
public interface ProductTabContentRepository extends JpaRepository<ProductTab, String>, QueryDslPredicateExecutor {


}
