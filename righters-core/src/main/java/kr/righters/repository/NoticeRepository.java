package kr.righters.repository;


import kr.righters.domain.entity.Notice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * Created by yj.nam on 19.11.28..
 */
public interface NoticeRepository extends JpaRepository<Notice, String>, QueryDslPredicateExecutor {


}
