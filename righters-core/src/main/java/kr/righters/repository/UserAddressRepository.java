package kr.righters.repository;


import kr.righters.domain.entity.UserAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * Created by yj.nam on 19.11.29..
 */
public interface UserAddressRepository extends JpaRepository<UserAddress, String>, QueryDslPredicateExecutor {


}
