package kr.righters.repository;


import kr.righters.domain.entity.UserSigninHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * Created by yj.nam on 19.12.18..
 */
public interface UserSigninHistoryRepository extends JpaRepository<UserSigninHistory, String>, QueryDslPredicateExecutor {

    UserSigninHistory findByToken(String token);
}
