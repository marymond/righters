package kr.righters.repository;


import kr.righters.domain.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * Created by yj.nam on 19.11.29..
 */
public interface ProductRepository extends JpaRepository<Product, String>, QueryDslPredicateExecutor {


}
