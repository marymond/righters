package kr.righters.repository;


import kr.righters.domain.entity.UserDonationHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * Created by yj.nam on 19.11.29..
 */
public interface UserDonationHistoryRepository extends JpaRepository<UserDonationHistory, String>, QueryDslPredicateExecutor {


}
