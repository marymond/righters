package kr.righters.repository;


import kr.righters.domain.entity.AboutCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * Created by yj.nam on 20.10.14..
 */
public interface AboutCategoryRepository extends JpaRepository<AboutCategory, String>, QueryDslPredicateExecutor {


}
