package kr.righters.util;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Random;

/**
 * Created by yj.nam on 2019. 12. 05..
 */
@Component
public class RandomStringGenerator {

    private static final String SYMBOLS_FOR_USER_REFERRER = "0123456789ABCDEFGHJKLMNPQRSTUVWXUZ";
    private static final String SYMBOLS_FOR_RANDOM_STRING = "01234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String SYMBOLS_FOR_OTP           = "0123456789";
    private static final String SYMBOLS_FOR_PASSWORD = "0123456789abcdefghjkmnpqrstuvwxyz";



    public String generatorRandomString(int length) {

        char[] buf = new char[length];

        Random random = new Random();
        for (int i = 0; i < length; i++) {

            // First Character must be 1
            buf[i] = SYMBOLS_FOR_RANDOM_STRING.charAt(random.nextInt(SYMBOLS_FOR_RANDOM_STRING.length()));
        }

        return new String(buf);

    }

    public String generateOTP() {

        int otpLength = 6;
        char[] buf = new char[otpLength];

        Random random = new Random();
        for (int i = 0; i < otpLength; i++) {
            buf[i] = SYMBOLS_FOR_OTP.charAt(random.nextInt(SYMBOLS_FOR_OTP.length()));
        }
        return new String(buf);

    }

    public String generateRandomPassword() {

        int passwordLength = 10;
        char[] buf = new char[passwordLength];

        Random random = new Random();
        for (int i = 0; i < passwordLength; i++) {
            buf[i] = SYMBOLS_FOR_PASSWORD.charAt(random.nextInt(SYMBOLS_FOR_PASSWORD.length()));
        }
        return new String(buf);

    }

    public String generateUserRefferer() {

        int otpLength = 8;
        char[] buf = new char[otpLength];

        Random random = new Random();
        for (int i = 0; i < otpLength; i++) {
            buf[i] = SYMBOLS_FOR_USER_REFERRER.charAt(random.nextInt(SYMBOLS_FOR_USER_REFERRER.length()));
        }
        return new String(buf);

    }

    /**
     * 주문번호 생성
     * @return
     */
    public String generateOrderNumber(){
        LocalDateTime ldt = LocalDateTime.now();

        long boughtTimeSecond = ( ldt.getHour() * 60 * 60  )
                + ( ldt.getMinute() * 60 )
                + (ldt.getSecond() );

        String uniqueNumber = String.valueOf( (ldt.getNano() /1000000)).substring(0,3);
        uniqueNumber = (3 >= uniqueNumber.length())?uniqueNumber:uniqueNumber.substring(0, 3);

        StringBuilder sb = new StringBuilder();
        sb.append(ldt.getYear())
                .append( String.format("%2d", ldt.getMonthValue()).replaceAll(" ", "0"))
                .append( String.format("%2d", ldt.getDayOfMonth()).replaceAll(" ", "0"))
                .append("-")
                .append( String.format("%3s", uniqueNumber).replaceAll(" ", "0") )
                .append("-")
                .append( String.format("%5d", boughtTimeSecond).replaceAll(" ", "0"));
        return sb.toString();
    }


}
