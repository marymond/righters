package kr.righters.util;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
@Component
public class FileUploader {
	@Value("${aws.access_key_id}")
	private String awsAccessKeyId;
	@Value("${aws.secret_key}")
	private String awsSecretAccessKey;
	@Value("${aws.s3_bucket_name}")
	private String bucketName;
	@Value("${aws.s3_cloudfrontHost}")
	private String cloudfrontHost;

	private AmazonS3 s3client;

	@Autowired
	UniqueIdGenerator uniqueIdGenerator;

	@PostConstruct
	public void init() {
		AWSCredentials credentials = new BasicAWSCredentials(awsAccessKeyId, awsSecretAccessKey);
		AmazonS3ClientBuilder builder = AmazonS3ClientBuilder.standard();
		builder.withRegion(Regions.AP_NORTHEAST_2);
		builder.withCredentials(new AWSStaticCredentialsProvider(credentials));
		s3client = builder.build();
	}

	public String upload(File file, String directory, String id) {
		AccessControlList acl = getAllUserReadAcl();

		try {
			String filename = generateFileName(directory, id, file);
			s3client.putObject(new PutObjectRequest(bucketName, filename, file).withAccessControlList(acl));
			return cloudfrontHost.concat("/").concat(filename);
		} catch (IllegalStateException e) {
			log.error(e.getMessage(), e);
		}

		return null;
	}

	public String upload(MultipartFile multipartFile, String directory, String id) {
		File file = new File(multipartFile.getOriginalFilename());

		try {
			FileUtils.writeByteArrayToFile(file, multipartFile.getBytes());
			return upload(file, directory, id);
		} catch (IllegalStateException | IOException e) {
			log.error(e.getMessage(), e);
		} finally {
			FileUtils.deleteQuietly(file);
		}

		return null;
	}

	public String copy(String originUrl, String targetUrl) {
		String originKey = originUrl.replace(this.cloudfrontHost + "/", "");
		String targetKey = targetUrl.replace(this.cloudfrontHost + "/", "");

		if( targetKey.startsWith("/")) targetKey = targetKey.substring(1);

		try {
			if( originKey.hashCode() != targetKey.hashCode()) {
				CopyObjectRequest copyObjectRequest = new CopyObjectRequest(this.bucketName, originKey, this.bucketName, targetKey);
				copyObjectRequest.setCannedAccessControlList(CannedAccessControlList.PublicRead);
				CopyObjectResult result = this.s3client.copyObject(copyObjectRequest);
				this.s3client.deleteObject(this.bucketName, originKey);
			}
			return cloudfrontHost.concat("/").concat(targetKey);
		} catch (IllegalStateException e) {
			log.error(e.getMessage(), e);
		}
		return null;
	}

	private AccessControlList getAllUserReadAcl() {
		AccessControlList acl = new AccessControlList();
		acl.grantPermission(GroupGrantee.AllUsers, Permission.Read);
		return acl;
	}

	private String generateFileName(String directory, String id, File file) {
		String ext = ""; 
		String filename = file.getName();
		String unique_id = uniqueIdGenerator.getStringId();
		if (filename.lastIndexOf(".") != -1 && filename.lastIndexOf(".") != 0) { 
			ext = "."+filename.substring(filename.lastIndexOf(".")+1); 
		}
		return directory + "/" + id + "-" +LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"))+unique_id+ext;
	}	
}