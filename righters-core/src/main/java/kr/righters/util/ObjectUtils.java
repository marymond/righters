package kr.righters.util;

import java.beans.PropertyDescriptor;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import com.google.common.base.Throwables;

import lombok.experimental.UtilityClass;

/**
 * Created by yj.nam on 19.10.29..
 */
@UtilityClass
public class ObjectUtils {
	public static <T> void copyProperties(T source, T target) {
		BeanUtils.copyProperties(source, target, getNullPropertyNames(source));
	}

	public static String[] getNullPropertyNames(Object source) {
		final BeanWrapper src = new BeanWrapperImpl(source);
		PropertyDescriptor[] pds = src.getPropertyDescriptors();

		Set<String> emptyNames = new HashSet<String>();
		for (PropertyDescriptor pd : pds) {
			if (src.isWritableProperty(pd.getName()) && src.getPropertyValue(pd.getName()) == null) {
				emptyNames.add(pd.getName());
			}
		}

		String[] result = new String[emptyNames.size()];
		return emptyNames.toArray(result);
	}
	
	@SuppressWarnings("unchecked")
	public static <V> V clone(V source) {
		try {
			return (V) org.apache.commons.beanutils.BeanUtils.cloneBean(source);
		} catch (ReflectiveOperationException e) {
			Throwables.throwIfUnchecked(e);
		}

		return null;
	}
}
