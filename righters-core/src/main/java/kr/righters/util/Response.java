package kr.righters.util;

import lombok.Data;

@Data
public class Response {
	private String code;
	private String message;
	private Object data;

	public boolean isSuccess() {
		return "SUCCESS".equals(code);
	}

	public static Response success() {
		return success(null);
	}

	public static Response success(Object data) {
		Response response = new Response();
		response.setCode("SUCCESS");
		response.setMessage("성공했습니다.");
		response.setData(data);
		return response;
	}

	public static Response fail(String message) {
		Response response = new Response();
		response.setCode("FAIL");
		response.setMessage(message);
		response.setData(null);
		return response;
	}
}