package kr.righters.util;

import java.text.DecimalFormat;

/**
 * Created by yj.nam on 19.10.29..
 */
public class FormatterUtils {

	public static String getCellphoneNumber(String value) {		
		if (value == null) {
			return "";
		}
		
		if (value.length() == 8) {
			return value.replaceFirst("^([0-9]{4})([0-9]{4})$", "$1-$2");
		} else if (value.length() == 12) {
			return value.replaceFirst("(^[0-9]{4})([0-9]{4})([0-9]{4})$", "$1-$2-$3");
		}

		return value.replaceFirst("(^02|[0-9]{3})([0-9]{3,4})([0-9]{4})$", "$1-$2-$3");
	}

	public static String toCurrency(Long value) { 
		return (value == null) ? "" : new DecimalFormat("#,##0").format(value);		
	}
	
	public static String toNl2Br(String value) { 
		if (value == null) { 
			return "";
		}
		return value.replaceAll("(\r\n|\n)", "<br />");
	}
}
