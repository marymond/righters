package kr.righters.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

/**
 * Created by yj.nam on 19.10.29..
 */
@Component
public class LocalDateUtils {

	public static LocalDate getLocalDate(String dateAt) {
		if (StringUtils.isEmpty(dateAt)) { 
			return null; 
		}		
		DateTimeFormatter formatter = getDateTimeFormat(null);		
		return LocalDate.parse(dateAt, formatter); 
	}
	
	public static LocalDate getLocalDate(String dateAt,String pattern) {
		if (StringUtils.isEmpty(dateAt)) { 
			return null; 
		}		
		DateTimeFormatter formatter = getDateTimeFormat(pattern);		
		return LocalDate.parse(dateAt, formatter); 
	}
	
	public static String getLocalDateToString(LocalDate dateAt,String pattern) { 
		if (dateAt == null) { 
			return null; 
		}		
		
		DateTimeFormatter formatter = getDateTimeFormat(pattern);		
		return dateAt.format(formatter);
	}
	
	public static DateTimeFormatter getDateTimeFormat(String pattern) { 
		if (StringUtils.isEmpty(pattern)) { 
			return  DateTimeFormatter.ofPattern("yyyy-MM-dd");
		}
		return DateTimeFormatter.ofPattern(pattern);
	}
	
}
