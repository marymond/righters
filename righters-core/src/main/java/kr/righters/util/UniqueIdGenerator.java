package kr.righters.util;

import com.eaio.uuid.UUIDGen;
import org.springframework.stereotype.Component;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 * Created by yj.nam on 19.10.29..
 */
@Component
public class UniqueIdGenerator {

    public static final String FORMAT_FOR_STRING_ID_EMPTY = "%016d0000000000000000";

    public static final String FORMAT_FOR_STRING_ID = "%016d%s%04d";
    public static final String FORMAT_FOR_TOKEN_ID = "%064x";
    public static final String MESSAGE_DIGEST_ALGORITHM_FOR_TOKEN_ID = "SHA-256";

    private final String macAddress = UUIDGen.getMACAddress();

    private final int maxShort = (int) 0xffff;
    private final Object lock = new Object();

    private final Random random = new Random();

    private volatile long lastTimestamp;
    private volatile int seq;

    /**
     *
     * @return
     */
    public String getStringId() {

        if(seq == maxShort) {
            throw new RuntimeException("Too fast");
        }
        long time;
        synchronized(lock) {
            time = System.currentTimeMillis();
            if(time != lastTimestamp) {
                lastTimestamp = time;
                seq = 0;
            }
            seq++;
            return String.format(FORMAT_FOR_STRING_ID, time, macAddress, seq);
        }
    }

    /**
     *
     * @return
     */
    public String getTokenId() {
        try {
            MessageDigest md = MessageDigest.getInstance(MESSAGE_DIGEST_ALGORITHM_FOR_TOKEN_ID);
            md.update(getStringId().getBytes());
            return String.format(FORMAT_FOR_TOKEN_ID, new java.math.BigInteger(1, md.digest()));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     *
     * @param timestamp
     * @return
     */
    public String getStringId(long timestamp) {
        return String.format(FORMAT_FOR_STRING_ID, timestamp, macAddress, random.nextInt(9999));
    }


    /**
     *
     * @param timestamp
     * @param seq
     * @return
     */
    public String getStringId(long timestamp, int seq) {
        return String.format(FORMAT_FOR_STRING_ID, timestamp, macAddress, seq);
    }

    /**
     * 특정일 기준 ID 취득
     * @param timestamp
     * @return
     */
    public String getStringIdEmpty(long timestamp){
        return String.format(FORMAT_FOR_STRING_ID_EMPTY, timestamp);
    }

}
