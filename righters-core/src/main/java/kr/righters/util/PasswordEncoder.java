package kr.righters.util;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Component;

@Component
public class PasswordEncoder{

    public String encodePassword(String password){
        return BCrypt.hashpw(password, BCrypt.gensalt(10));
    }

    public boolean isMatch(String password, String hashed){
        return BCrypt.checkpw(password, hashed);
    }
}
