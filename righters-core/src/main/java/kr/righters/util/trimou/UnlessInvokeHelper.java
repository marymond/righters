package kr.righters.util.trimou;

import org.trimou.handlebars.Options;
import org.trimou.util.Checker;

/**
 * Created by yj.nam on 19.10.29..
 */
public class UnlessInvokeHelper extends IfInvokeHelper {
	@Override
	protected boolean isMatching(Object value, Options options) {
		return Checker.isFalsy(evaluate(options));
	}
}
