package kr.righters.util.trimou;

import org.apache.commons.lang3.StringUtils;
import org.trimou.handlebars.BasicValueHelper;
import org.trimou.handlebars.Options;
/**
 * Created by yj.nam on 19.10.29..
 */
public class EllipseHelper extends BasicValueHelper {
	@Override
	public void execute(Options options) {
		Object valueObject = options.getParameters().get(0);
		String valueStr = String.valueOf(valueObject);

		Object lengthObject = options.getParameters().get(1);
		Integer lengthInt = Integer.valueOf(String.valueOf(lengthObject));

		if (valueStr == null || "null".equals(valueStr)) {
			options.append("");
		} else {
			if (StringUtils.length(valueStr) > lengthInt) {
				options.append(StringUtils.substring(valueStr, 0, lengthInt) + "...");
			} else {
				options.append(valueStr);
			}
		}
	}
}