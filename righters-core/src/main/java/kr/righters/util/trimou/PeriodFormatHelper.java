package kr.righters.util.trimou;

import kr.righters.util.LocalDateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.trimou.handlebars.BasicValueHelper;
import org.trimou.handlebars.Options;

/**
 * Created by yj.nam on 19.10.29..
 */
public class PeriodFormatHelper extends BasicValueHelper {

	@Autowired
	LocalDateUtils localDateUtil;
	
	@Override
	public void execute(Options options) {
		Object startStr = options.getParameters().get(0);
		Object endStr = options.getParameters().get(1);
		Object pattern = options.getParameters().get(2);
		
		StringBuffer sb = new StringBuffer(); 
		if (startStr != null) { 		
			sb.append(convertLocalDateToString(startStr.toString(),pattern.toString()));
			sb.append(" - ");
			
			if (endStr == null) { 
				sb.append("미정");  
			} else {				
				sb.append(convertLocalDateToString(endStr.toString(),pattern.toString()));				
			}			
		} else { 
			sb.append("미정");
		}	
		options.append(sb.toString());		
	}
	
	public String convertLocalDateToString(String dateStr,String pattern) { 
		return LocalDateUtils.getLocalDateToString(LocalDateUtils.getLocalDate(dateStr),pattern);		
	}

}