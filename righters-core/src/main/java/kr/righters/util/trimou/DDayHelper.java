package kr.righters.util.trimou;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import kr.righters.util.LocalDateUtils;
import org.trimou.handlebars.BasicValueHelper;
import org.trimou.handlebars.Options;

/**
 * Created by yj.nam on 19.10.29..
 */
public class DDayHelper extends BasicValueHelper {
	@Override
	public void execute(Options options) {
		Object valueObject = options.getParameters().get(0);		
		LocalDate thisAt = LocalDateUtils.getLocalDate(String.valueOf(valueObject));
		options.append(String.valueOf(ChronoUnit.DAYS.between(LocalDate.now(), thisAt)));		
	}
}