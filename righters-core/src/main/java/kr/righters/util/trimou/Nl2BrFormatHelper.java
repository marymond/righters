package kr.righters.util.trimou;

import kr.righters.util.FormatterUtils;
import org.trimou.handlebars.BasicValueHelper;
import org.trimou.handlebars.Options;

/**
 * Created by yj.nam on 19.10.29..
 */
public class Nl2BrFormatHelper  extends BasicValueHelper {
	@Override
	public void execute(Options options) {
		Object valueObject = options.getParameters().get(0);
		String valueStr = valueObject == null ? "" : String.valueOf(valueObject);

		try { 
			options.append(FormatterUtils.toNl2Br(valueStr));
		} catch (Exception e) {
			options.append("");
		}
	}
}
