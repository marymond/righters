package kr.righters.util.trimou;

import java.text.DecimalFormat;

import org.trimou.handlebars.BasicValueHelper;
import org.trimou.handlebars.Options;

/**
 * Created by yj.nam on 19.10.29..
 */
public class CurrencyFormatHelper extends BasicValueHelper {
	/**
	 * 숫자 콤마표시
	 * @param options
	 */
	@Override
	public void execute(Options options) {
		Object valueObject = options.getParameters().get(0);
		String valueStr = String.valueOf(valueObject);

		try {
			Long value = Long.valueOf(valueStr);
			String format = (value == null) ? "" : new DecimalFormat("#,##0").format(value);
			options.append(format);
		} catch (NumberFormatException e) {
			options.append(valueStr);
		}
	}
}
