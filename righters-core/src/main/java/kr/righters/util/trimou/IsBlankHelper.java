package kr.righters.util.trimou;

import org.apache.commons.lang3.StringUtils;
import org.trimou.handlebars.BasicSectionHelper;
import org.trimou.handlebars.Options;
/**
 * Created by yj.nam on 19.10.29..
 */
public class IsBlankHelper extends BasicSectionHelper {
	@Override
	public void execute(Options options) {
		Object valueObject = options.getParameters().get(0);
		String valueStr = String.valueOf(valueObject);

		if (StringUtils.isBlank(valueStr) || "null".equals(valueStr.trim())) {
			options.fn();
		}
	}
}
