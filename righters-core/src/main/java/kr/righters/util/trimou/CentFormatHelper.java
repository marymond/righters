package kr.righters.util.trimou;

import org.trimou.handlebars.BasicValueHelper;
import org.trimou.handlebars.Options;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Created by yj.nam on 19.10.29..
 */
public class CentFormatHelper extends BasicValueHelper {
	/**
	 * 숫자 콤마표시
	 * @param options
	 */
	@Override
	public void execute(Options options) {
		Object valueObject = options.getParameters().get(0);
		String valueStr = String.valueOf(valueObject);

		Object rateObject = null;
		String rateStr = null;
		if(options.getParameters().size() > 2){
			rateObject = options.getParameters().get(1);
			rateStr = String.valueOf(rateObject);
		}
		try {
			BigDecimal value = new BigDecimal(valueStr);

			if(rateStr != null){
				BigDecimal rate = new BigDecimal(rateStr);
				value = value.multiply(rate.divide(new BigDecimal(100),0,BigDecimal.ROUND_DOWN));
			}

			double v = value.divide(new BigDecimal(100),2,BigDecimal.ROUND_DOWN).doubleValue();

			String format = (value == null) ? "" : new DecimalFormat("#.##").format(v);

			options.append("$" + format);
		} catch (NumberFormatException e) {
			options.append(valueStr);
		}
	}
}
