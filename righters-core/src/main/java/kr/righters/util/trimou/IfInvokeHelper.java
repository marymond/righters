package kr.righters.util.trimou;

import static org.trimou.handlebars.OptionsHashKeys.CLASS;
import static org.trimou.handlebars.OptionsHashKeys.M;
import static org.trimou.handlebars.OptionsHashKeys.METHOD;
import static org.trimou.handlebars.OptionsHashKeys.ON;

import java.lang.reflect.Method;

import org.trimou.exception.MustacheException;
import org.trimou.exception.MustacheProblem;
import org.trimou.handlebars.IfHelper;
import org.trimou.handlebars.Options;
import org.trimou.util.Checker;
/**
 * Created by yj.nam on 19.10.29..
 */
public class IfInvokeHelper extends IfHelper {
	@Override
	protected boolean isMatching(Object value, Options options) {
		return !Checker.isFalsy(evaluate(options));
	}

	protected Object evaluate(Options options) {
		Class<?> clazz = null;
		Object methodName = options.getHash().get(M);
		if (methodName == null) {
			methodName = options.getHash().get(METHOD);
		}

		Object instance = options.getHash().get(ON);
		if (instance == null) {
			clazz = loadClassIfNeeded(options);
			if (clazz == null) {
				instance = options.peek();
			}
		}

		if (clazz == null) {
			clazz = instance.getClass();
		}

		Method method = null;
		for (Method m : clazz.getMethods()) {
			if (m.getName().equals(methodName)) {
				method = m;
				break;
			}
		}

		if (method == null) {
			throw new MustacheException(MustacheProblem.RENDER_HELPER_INVALID_OPTIONS, "Unable to find unambiguous method with name \"%s\" and parameter types %s on class %s [%s]",
					methodName, options.getHash(), clazz.getName(), options.getTagInfo());
		}

		try {
			return method.invoke(instance, options.getParameters().toArray());
		} catch (Exception e) {
			throw new MustacheException(MustacheProblem.RENDER_GENERIC_ERROR, e);
		}
	}

	private Class<?> loadClassIfNeeded(Options options) {
		Class<?> clazz = null;
		try {
			Object clazzValue = options.getHash().get(CLASS);
			if (clazzValue != null) {
				if (clazzValue instanceof Class<?>) {
					clazz = (Class<?>) clazzValue;
				} else {
					clazz = this.getClass().getClassLoader().loadClass(clazzValue.toString());
				}
			}
		} catch (ClassNotFoundException ignored) {
		}

		return clazz;
	}
}
