package kr.righters.util.trimou;

import org.apache.commons.lang3.StringUtils;
import org.trimou.handlebars.BasicSectionHelper;
import org.trimou.handlebars.Options;

/**
 * Created by yj.nam on 19.10.29..
 */
public class StartWithHelper extends BasicSectionHelper {
	@Override
	public void execute(Options options) {
		Object valueObject1 = options.getParameters().get(0);
		Object valueObject2 = options.getParameters().get(1);
		String valueStr1 = String.valueOf(valueObject1);
		String valueStr2 = String.valueOf(valueObject2);

		if (StringUtils.startsWith(valueStr1, valueStr2)) {
			options.fn();
		}
	}
}
