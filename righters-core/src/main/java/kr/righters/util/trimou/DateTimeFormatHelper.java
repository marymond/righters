package kr.righters.util.trimou;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.trimou.engine.MustacheTagInfo;
import org.trimou.handlebars.i18n.AbstractTimeFormatHelper;
/**
 * Created by yj.nam on 19.10.29..
 */
public class DateTimeFormatHelper extends AbstractTimeFormatHelper<Object, Integer> {

	@Override
	protected String defaultFormat(Object value, Locale locale, TimeZone timeZone) {
		if (value == null) {
			return "";
		}

		return format(value, DateFormat.MEDIUM, locale, timeZone);
	}

	@Override
	protected String format(Object value, Integer style, Locale locale, TimeZone timeZone) {
		if (value == null) {
			return "";
		}

		DateFormat dateFormat = DateFormat.getDateTimeInstance(style, style, locale);
		dateFormat.setTimeZone(timeZone);
		return dateFormat.format(value);
	}

	@Override
	protected String format(Object value, String pattern, Locale locale, TimeZone timeZone) {
		if (value == null) {
			return "";
		}

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, locale);
		simpleDateFormat.setTimeZone(timeZone);
		return simpleDateFormat.format(value);
	}

	protected Object getFormattableObject(Object value, Locale locale, TimeZone timeZone, MustacheTagInfo tagInfo) {
		if (value == null) {
			return null;
		}

		if (value instanceof Date || value instanceof Number) {
			return value;
		} else if (value instanceof Calendar) {
			return ((Calendar) value).getTime();
		} else if (value instanceof LocalDateTime) {
			return Date.from(((LocalDateTime) value).atZone(ZoneId.systemDefault()).toInstant());
		} else if (value instanceof LocalDate) {
			return Date.from(((LocalDate) value).atStartOfDay(ZoneId.systemDefault()).toInstant());
		} else {
			throw valueNotAFormattableObject(value, tagInfo);
		}
	}

	protected Integer parseStyle(String style, MustacheTagInfo tagInfo) {
		if ("full".equals(style)) {
			return DateFormat.FULL;
		} else if ("long".equals(style)) {
			return DateFormat.LONG;
		} else if ("short".equals(style)) {
			return DateFormat.SHORT;
		} else if ("medium".equals(style)) {
			return DateFormat.MEDIUM;
		} else {
			throw unknownStyle(style, tagInfo);
		}
	}
}