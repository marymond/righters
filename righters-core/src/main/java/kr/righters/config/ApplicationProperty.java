package kr.righters.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by yj.nam on 19.10.29..
 */
@Component
@ConfigurationProperties(prefix="kr.righters")
@Data
public class ApplicationProperty {

    private String name;
    private String version;
    private String environment;
    private String applicationDomain;
    private String applicationHost;
    private String staticHost;
    private String apiHost;
    private String wwwHost;
    private String cmsHost;

}
