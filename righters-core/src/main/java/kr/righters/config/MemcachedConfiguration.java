package kr.righters.config;

import lombok.extern.slf4j.Slf4j;
import net.spy.memcached.spring.MemcachedClientFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * Created by yj.nam on 19.10.29..
 */
@Slf4j
@Configuration
public class MemcachedConfiguration {

    @Autowired
    Environment environment;

    @Bean
    public MemcachedClientFactoryBean memcachedClient() {

        MemcachedClientFactoryBean bean = new MemcachedClientFactoryBean();
        bean.setServers(environment.getRequiredProperty("memcached.hosts"));

        return bean;
    }
}
