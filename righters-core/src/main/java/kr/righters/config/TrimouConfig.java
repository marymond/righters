package kr.righters.config;

import kr.righters.util.trimou.*;
import org.springframework.context.annotation.Configuration;
import org.trimou.engine.MustacheEngineBuilder;
import org.trimou.extension.spring.starter.TrimouConfigurer;
import org.trimou.handlebars.HelpersBuilder;

/**
 * Created by yj.nam on 19.10.29..
 */
@Configuration
public class TrimouConfig implements TrimouConfigurer {
	@Override
	public void configure(MustacheEngineBuilder engineBuilder) {
		engineBuilder.registerHelpers(HelpersBuilder.extra().build());
		engineBuilder.registerHelper("formatDateTime", new DateTimeFormatHelper());
		engineBuilder.registerHelper("formatCent", new CentFormatHelper());
		engineBuilder.registerHelper("formatCurrency", new CurrencyFormatHelper());
		engineBuilder.registerHelper("ellipse", new EllipseHelper());
		engineBuilder.registerHelper("startWith", new StartWithHelper());
		engineBuilder.registerHelper("notStartWith", new NotStartWithHelper());
		engineBuilder.registerHelper("isBlank", new IsBlankHelper());
		engineBuilder.registerHelper("isNotBlank", new IsNotBlankHelper());
		engineBuilder.registerHelper("ifInvoke", new IfInvokeHelper());
		engineBuilder.registerHelper("unlessInvoke", new UnlessInvokeHelper());
		engineBuilder.registerHelper("dday", new DDayHelper());
		engineBuilder.registerHelper("formatPeriod", new PeriodFormatHelper());
		engineBuilder.registerHelper("formatPhone", new PhoneNumberFormatHelper());	
		engineBuilder.registerHelper("nl2br", new Nl2BrFormatHelper());
	}
}