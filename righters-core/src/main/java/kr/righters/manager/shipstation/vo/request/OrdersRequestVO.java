package kr.righters.manager.shipstation.vo.request;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@Setter
@Getter
public class OrdersRequestVO {

    private String customerName; //이름(선택사항)
    private String itemKeyword; //검색키워드(선택사항)
    private String createDateStart;
    private String createDateEnd;
    private String modifyDateStart;
    private String modifyDateEnd;
    private String orderDateStart;
    private String orderDateEnd;
    private String orderNumber;
    private OrderStatus orderStatus;
    private String paymentDateStart;
    private String paymentDateEnd;
    private Long storeId;
    private SortBy sortBy;
    private String sortDir;
    private String page;
    private String pageSize;

    @Builder
    public OrdersRequestVO(String customerName, String itemKeyword, String createDateStart, String createDateEnd, String modifyDateStart, String modifyDateEnd,
                           String orderDateStart, String orderDateEnd, String orderNumber, OrderStatus orderStatus, String paymentDateStart, String paymentDateEnd,
                           Long storeId, SortBy sortBy, String sortDir, String page, String pageSize) {
        this.customerName = customerName;
        this.itemKeyword = itemKeyword;
        this.createDateStart = createDateStart;
        this.createDateEnd = createDateEnd;
        this.modifyDateStart = modifyDateStart;
        this.modifyDateEnd = modifyDateEnd;
        this.orderDateStart = orderDateStart;
        this.orderDateEnd = orderDateEnd;
        this.orderNumber = orderNumber;
        this.orderStatus = orderStatus;
        this.paymentDateStart = paymentDateStart;
        this.paymentDateEnd = paymentDateEnd;
        this.storeId = storeId;
        this.sortBy = sortBy;
        this.sortDir = sortDir;
        this.page = page;
        this.pageSize = pageSize;
    }

    public MultiValueMap<String, Object> getPrameters(){
        MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
        if(StringUtils.isNotBlank(customerName)){
            parameters.add("customerName", customerName);
        }
        if(StringUtils.isNotBlank(itemKeyword)){
            parameters.add("itemKeyword", itemKeyword);
        }
        if(StringUtils.isNotBlank(createDateStart)){
            parameters.add("createDateStart", createDateStart);
        }
        if(StringUtils.isNotBlank(createDateEnd)){
            parameters.add("createDateEnd", createDateEnd);
        }
        if(StringUtils.isNotBlank(modifyDateStart)){
            parameters.add("modifyDateStart", modifyDateStart);
        }
        if(StringUtils.isNotBlank(modifyDateEnd)){
            parameters.add("modifyDateEnd", modifyDateEnd);
        }
        if(StringUtils.isNotBlank(orderDateStart)){
            parameters.add("orderDateStart", orderDateStart);
        }
        if(StringUtils.isNotBlank(orderDateEnd)){
            parameters.add("orderDateEnd", orderDateEnd);
        }
        if(StringUtils.isNotBlank(orderNumber)){
            parameters.add("orderNumber", orderNumber);
        }
        if(orderStatus != null){
            parameters.add("orderStatus", orderStatus.desc);
        }
        if(StringUtils.isNotBlank(paymentDateStart)){
            parameters.add("paymentDateStart", paymentDateStart);
        }
        if(StringUtils.isNotBlank(paymentDateEnd)){
            parameters.add("paymentDateEnd", paymentDateEnd);
        }
        if(storeId != null){
            parameters.add("storeId", storeId);
        }
        if(sortBy != null){
            parameters.add("sortBy", sortBy.desc);
        }
        if(StringUtils.isNotBlank(sortDir)){
            parameters.add("sortDir", sortDir);
        }
        if(StringUtils.isNotBlank(page)){
            parameters.add("page", page);
        }
        if(StringUtils.isNotBlank(pageSize)){
            parameters.add("pageSize", pageSize);
        }

        return parameters;
    }

    public enum OrderStatus {
        awaiting_payment("awaiting_payment"),
        awaiting_shipment("awaiting_shipment"),
        pending_fulfillment("pending_fulfillment"),
        shipped("shipped"),
        on_hold("on_hold"),
        cancelled("cancelled");

        @Getter
        private String desc;

        private OrderStatus(String desc) {
            this.desc = desc;
        }
    }

    public enum SortBy {
        OrderDate("OrderDate"),
        ModifyDate("ModifyDate"),
        CreateDate("CreateDate");

        @Getter
        private String desc;

        private SortBy(String desc) {
            this.desc = desc;
        }
    }
}
