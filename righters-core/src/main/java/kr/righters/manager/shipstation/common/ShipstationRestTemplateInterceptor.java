package kr.righters.manager.shipstation.common;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;

/**
 * Created by yjnam on 2020. 03. 18..
 */
@Slf4j
public class ShipstationRestTemplateInterceptor implements ClientHttpRequestInterceptor {
    @Override
    public ClientHttpResponse intercept(HttpRequest httpRequest, byte[] bytes, ClientHttpRequestExecution clientHttpRequestExecution) throws IOException {
        URI uri = httpRequest.getURI();
        traceRequest(httpRequest, bytes);
        ClientHttpResponse response = clientHttpRequestExecution.execute(httpRequest, bytes);
        traceResponse(response, uri);
        return response;

    }

    private void traceRequest(HttpRequest request, byte[] body) throws IOException {
        log.debug("Request Method : {} , Uri : {}",request.getMethod(), request.getURI() );
    }

    private void traceResponse(ClientHttpResponse response, URI uri) throws IOException {
        log.debug("Response Status Code : {}",response.getStatusCode());
        if(log.isDebugEnabled()) {
            InputStream is = response.getBody();
            byte[] bodyData = IOUtils.toByteArray(is);

            log.debug("response body : {}",new String(bodyData),StandardCharsets.UTF_8);

        }
    }

}
