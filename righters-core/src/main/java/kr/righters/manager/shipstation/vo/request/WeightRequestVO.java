package kr.righters.manager.shipstation.vo.request;

import lombok.Data;

@Data
public class WeightRequestVO {
    private Double value; //무게
    private String units; // pounds, ounces, grams
}
