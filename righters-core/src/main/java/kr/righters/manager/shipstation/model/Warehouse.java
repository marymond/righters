package kr.righters.manager.shipstation.model;

import lombok.Data;

@Data
public class Warehouse {
    private String warehouseId;
    private String warehouseName;
    private Address originAddress;
    private Address returnAddress;
    private Boolean isDefault;

}
