package kr.righters.manager.shipstation.common;

/**
 * Created by yjnam on 2020. 03. 18..
 */
public class ShipstationConstants {

    //주문내역취득(GET)
    public static final String API_ORDERS = "/orders";

    //주문한개취득(GET), 주문한개삭제(DELETE)
    public static final String API_ORDERS_ORDERID ="/orders/{orderId}";

    //주문생성(POST)
    public static final String API_ORDERS_CREATEORDER = "/orders/createorder";

    //주문라벨생성(POST)
    public static final String API_ORDERS_CREATELABELFORORDER = "/orders/createlabelfororder";

    //입고내역취득(GET)
    public static final String API_WAREHOUSES = "/warehouses";

    //쉽먼트내역취득(GET)
    public static final String API_SHIPMENTS = "/shipments";

}

