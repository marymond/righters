package kr.righters.manager.shipstation.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class Shipment {
    private Long            shipment;
    private Long            orderId;
    private String          orderKey;
    private String          userId;
    private String          orderNumber;
    private String          createDate;
    private String          shipData;
    private Double shipmentCost;
    private Long insuranceCost;
    private String trackingNumber;
    private Boolean isReturnLabel;
    private String batchNumber;
    private String carrierCode;
    private String serviceCode;
    private String packageCode;
    private String confirmation;
    private Long warehouseId;
    private Boolean voided;
    private String voidDate;
    private Boolean marketplaceNotified;
    private String notifyErrorMessage;
    private Address shipTo;
    private Weight weight;
    private Dimensions dimensions;
    private InsuranceOptions insuranceOptions;
    private AdvancedOptions advancedOptions;
    private List<OrderItem> shipmentItems;
    private String labelData;
    private String formData;
}
