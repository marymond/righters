package kr.righters.manager.shipstation.model;

import lombok.Data;

@Data
public class InsuranceOptions {
    private String provider; //"shipsurance", "carrier", "provider"
    private Boolean insureShipment; // 배송물이 보험되었는지 여부
    private Double insuredValue; // 보험 가격
}
