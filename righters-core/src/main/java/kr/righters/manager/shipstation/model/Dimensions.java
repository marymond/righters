package kr.righters.manager.shipstation.model;

import lombok.Data;

@Data
public class Dimensions {
    private Integer length; //길이
    private Integer width; //너비
    private Integer height; //높이
    private String units; // "inches", "centimeters"
}
