package kr.righters.manager.shipstation.model;

import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class AdvancedOptions {
    private Long warehouseId; //창고 아이디
    private Boolean nonMachinable = false; //주문 가공할 수 있는지 여부
    private Boolean saturdayDelivery = false; //토요일에 배송가능한지 여부
    private Boolean containsAlcohol = false; //술 포함됐나

    //read-only
    private Boolean mergedOrSplit = false; //오더가 합병됐는지 나눠졌는지 여부

    //read-only
    private List<Long> mergedIds = new ArrayList<>(); // 오더아이디 리스트들..

    //read-only
    private Long parentId; //만약 오더가 나눠졌다면 오더의 부모 id를 리턴함. 나눠지지 않았다면 null리턴

    private Long storeId; //스토어 아이디
    private String customField1;
    private String customField2;
    private String customField3;
    private String source; //original source 인지 marketplace 인지 구분

    private String billToParty; // "my_account", "my_other_account", "recipient", "third_party"
    private String billToAccount;
    private String billToPostalCode;
    private String billToCountryCode;

    @Builder
    public AdvancedOptions(Long warehouseId, Long storeId) {
        this.warehouseId = warehouseId;
        this.storeId = storeId;
    }
}
