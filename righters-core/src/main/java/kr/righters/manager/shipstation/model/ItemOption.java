package kr.righters.manager.shipstation.model;

import lombok.Data;

@Data
public class ItemOption {
    private String name; // 옵션이름 ex) "Size"
    private String value; // 옵션값 ex) "Medium"
}
