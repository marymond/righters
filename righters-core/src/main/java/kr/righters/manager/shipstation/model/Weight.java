package kr.righters.manager.shipstation.model;

import lombok.Data;

@Data
public class Weight {
    private Double value; //무게
    private String units; // pounds, ounces, grams

    //read-only
    private Integer WeightUnits; //대문자로 시작하는 거 맞음.
}
