package kr.righters.manager.shipstation.vo.response;

import kr.righters.manager.shipstation.model.Order;
import lombok.Getter;

import java.util.List;

@Getter
public class OrdersResponseVO {
    private List<Order> orders;
    private Integer     total;
    private Integer     page;
    private Integer     pages;
}
