package kr.righters.manager.shipstation.model;

import lombok.Data;

import java.util.List;


//https://www.shipstation.com/developer-api/#/reference/model-internationaloptions
@Data
public class InternationalOptions {
    private String contents; //국제배송의 내용물, "merchandise", "documents", "gift", "returned_goods", "sample"
    private List<CustomsItem> customsItems; //커스텀 아이템들
    private String nonDelivery; //"return_to_sender", "treat_as_abandoned"

}
