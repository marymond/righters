package kr.righters.manager.shipstation;

import kr.righters.manager.shipstation.common.ShipstationConstants;
import kr.righters.manager.shipstation.common.ShipstationResponseErrorHandler;
import kr.righters.manager.shipstation.common.ShipstationRestTemplateInterceptor;
import kr.righters.manager.shipstation.config.ShipstationConfig;
import kr.righters.manager.shipstation.model.Order;
import kr.righters.manager.shipstation.vo.request.CreateLabelForOrderRequestVO;
import kr.righters.manager.shipstation.vo.request.OrderRequestVO;
import kr.righters.manager.shipstation.vo.request.OrdersRequestVO;
import kr.righters.manager.shipstation.vo.response.CreateLabelForOrderResponseVO;
import kr.righters.manager.shipstation.vo.response.DeleteOrderResponseVO;
import kr.righters.manager.shipstation.vo.response.OrdersResponseVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;

/**
 * Created by yjnam on 2019. 10. 4..
 */
@Slf4j
@Service
public class ShipstationManager {

    @Autowired
    ShipstationConfig shipstationConfig;

    private final RestTemplate restTemplate;

    /**
     *
     * @param restTemplateBuilder
     */
    public ShipstationManager(RestTemplateBuilder restTemplateBuilder, ShipstationConfig shipstationConfig) {
        this.shipstationConfig = shipstationConfig;

        this.restTemplate = restTemplateBuilder.basicAuthorization(shipstationConfig.getApiKey(), shipstationConfig.getApiSecret()).build();
        this.restTemplate.setErrorHandler(new ShipstationResponseErrorHandler());
        this.restTemplate.setInterceptors(Collections.singletonList(new ShipstationRestTemplateInterceptor()));
        this.restTemplate.setRequestFactory(new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory()));
    }

    /**
     * 주문내역 검색
     * @param ordersRequestVO 검색조건
     * @return
     */
    public OrdersResponseVO getAllOrders(OrdersRequestVO ordersRequestVO){

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(ordersRequestVO.getPrameters(), new HttpHeaders());

        OrdersResponseVO ordersResponseVO= restTemplate.exchange(
                UriComponentsBuilder.fromHttpUrl(shipstationConfig.getApiHost() + ShipstationConstants.API_ORDERS).build().encode().toUri()
                , HttpMethod.GET
                , requestEntity
                , new ParameterizedTypeReference<OrdersResponseVO>() {
                }).getBody();

        return ordersResponseVO;
    }

    /**
     * 주문 취득
     * @param orderId 주문ID
     * @return
     */
    public Order getOrder(String orderId){
        HttpHeaders httpHeaders = new HttpHeaders();

        HttpEntity<?> requestEntity = new HttpEntity<>(httpHeaders);

        Order order = restTemplate.exchange(
                UriComponentsBuilder.fromHttpUrl(shipstationConfig.getApiHost() + ShipstationConstants.API_ORDERS_ORDERID.replace("{orderId}",orderId)).build().encode().toUri()
                , HttpMethod.GET
                , requestEntity
                , new ParameterizedTypeReference<Order>() {
                }).getBody();

        return order;
    }

    /**
     * 주문 생성
     * @param orderRequestVO
     * @return
     */
    public Order createOrder(OrderRequestVO orderRequestVO){
        HttpHeaders httpHeaders = new HttpHeaders();

        HttpEntity<?> requestEntity = new HttpEntity<>(httpHeaders);

        Order order = restTemplate.exchange(
                UriComponentsBuilder.fromHttpUrl(shipstationConfig.getApiHost() + ShipstationConstants.API_ORDERS_CREATEORDER).build().encode().toUri()
                , HttpMethod.POST
                , requestEntity
                , new ParameterizedTypeReference<Order>() {
                }).getBody();

        return order;
    }

    /**
     * 주문 삭제
     * @param orderId
     * @return
     */
    public DeleteOrderResponseVO deleteOrder(String orderId){
        HttpHeaders httpHeaders = new HttpHeaders();

        HttpEntity<?> requestEntity = new HttpEntity<>(httpHeaders);

        DeleteOrderResponseVO deleteOrderResponseVO = restTemplate.exchange(
                UriComponentsBuilder.fromHttpUrl(shipstationConfig.getApiHost() + ShipstationConstants.API_ORDERS_ORDERID.replace("{orderId}",orderId)).build().encode().toUri()
                , HttpMethod.DELETE
                , requestEntity
                , new ParameterizedTypeReference<DeleteOrderResponseVO>() {
                }).getBody();

        return deleteOrderResponseVO;
    }

    /**
     * 주문 라벨생성
     * @param createLabelForOrderRequestVO
     * @return
     */
    public CreateLabelForOrderResponseVO createLabelForOrder(CreateLabelForOrderRequestVO createLabelForOrderRequestVO){

        HttpEntity<?> requestEntity = new HttpEntity<>(createLabelForOrderRequestVO, new HttpHeaders());

        CreateLabelForOrderResponseVO createLabelForOrderResponseVO = restTemplate.exchange(
                UriComponentsBuilder.fromHttpUrl(shipstationConfig.getApiHost() + ShipstationConstants.API_ORDERS_CREATELABELFORORDER).build().encode().toUri()
                , HttpMethod.POST
                , requestEntity
                , new ParameterizedTypeReference<CreateLabelForOrderResponseVO>() {
                }).getBody();

        return createLabelForOrderResponseVO;
    }

    /**
     * Webhook 으로 받은 URL로 필효한 데이터 정보 취득
     * @param url
     * @return
     */
    public OrdersResponseVO webhookResponse(String url) {
        HttpHeaders httpHeaders = new HttpHeaders();

        HttpEntity<?> requestEntity = new HttpEntity<>(httpHeaders);

        OrdersResponseVO ordersResponseVO = restTemplate.exchange(
                UriComponentsBuilder.fromHttpUrl(url).build().encode().toUri()
                , HttpMethod.GET
                , requestEntity
                , new ParameterizedTypeReference<OrdersResponseVO>() {
                }).getBody();

        return ordersResponseVO;
    }
}
