package kr.righters.manager.shipstation.vo.request;

import kr.righters.manager.shipstation.model.*;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CreateLabelForOrderRequestVO {
    /**  필수*/
/**/private Long orderId;
/**/private String carrierCode; //택배사 코드 ex) stamps_com, fedex ...
/**/private String serviceCode; //택배사의 서비스 코드
                                // ex) 택배사가  stamps.com 일 경우 usps_first_class_mail, usps_media_mail 등..
                                //     택배사가 페덱스일 경우 fedex_2day 등이 있다.
/**/private String packageCode; //택배 종류 선택하는 것 같음.
                                // ex) package, cubic, letter ...
/**/private String confirmation; //null, delivery, signature, Adult Signature 4개 중 하나
/**/private String     shipDate; //배송 시작 날짜? 과거로 설정할 수 없다.
    private Weight     weight; //무게
    private Dimensions           dimensions; //부피
    private InsuranceOptions     insuranceOptions; //보험옵션
    private InternationalOptions internationalOptions; //국제택배옵션
    private AdvancedOptions      advancedOptions; //추가옵션
    private Boolean              testLabel; //테스트라벨인지 여부
                               // false : 진짜 라벨
                               // true : 가짜 라벨

    @Builder
    public CreateLabelForOrderRequestVO(Long orderId, String carrierCode, String serviceCode, String packageCode, String confirmation, String shipDate,
                                        Weight weight, Dimensions dimensions, InsuranceOptions insuranceOptions, InternationalOptions internationalOptions, AdvancedOptions advancedOptions,
                                        Boolean testLabel) {
        this.orderId = orderId;
        this.carrierCode = carrierCode;
        this.serviceCode = serviceCode;
        this.packageCode = packageCode;
        this.confirmation = confirmation;
        this.shipDate = shipDate;
        this.weight = weight;
        this.dimensions = dimensions;
        this.insuranceOptions = insuranceOptions;
        this.internationalOptions = internationalOptions;
        this.advancedOptions = advancedOptions;
        this.testLabel = testLabel;
    }
}
