package kr.righters.manager.shipstation.model;

import lombok.Data;

@Data
public class ProductTag {
    private Long tagId;
    private String name;
}
