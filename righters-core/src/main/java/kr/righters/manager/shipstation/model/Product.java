package kr.righters.manager.shipstation.model;

import lombok.Data;

//https://www.shipstation.com/developer-api/#/reference/model-product
@Data
public class Product {

    //read-only
    private Long productId;

    private String sku;
    private String name;
    private Double price; //개당 가격
    private Double defaultCost; // seller's cost for thie product.
    private Integer length; //길이
    private Integer width; //너비
    private Integer height; //높이
    private Integer weightOz; //개당 무게
    private String internalNotes; //판매자의 제품의 대한 비밀 메모
    private String fulfillmentSku; //Stock keeping Unit for the fulfillment of that product by a 3rd party.

    //read-only
    private String createDate;

    //read-only
    private String modifyDate;

    private Boolean active;
    private ProductCategory productCategory;
    private String productType; //
    private String warehouseLocation;
    private String defaultCarrierCode;
    private String defaultServiceCode;
    private String defaultPackageCode;
    private String defaultIntlCarrierCode; //국제 쉬핑 캐리어
    private String defaultIntlServiceCode;
    private String defaultIntlPackageCode;
    private String defaultConfirmation;
    private String defaultIntlConfirmation;
    private String customsDescription;
    private Double customsValue;
    private String customsTariffNo;
    private String customsCountryCode;
    private Boolean noCustoms;
    private ProductTag tags;

}
