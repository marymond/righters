package kr.righters.manager.shipstation.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
public class OrderItem {
    //** 필수(아마도)


    //read-only
    private long orderItemId;

    private String lineItemKey; //OrderItem 식별자
/**/private String sku; //stock keeping unit
/**/private String name; //이름
    private String imageUrl; //상품 이미지 url
    private Weight Weight; //무게
/**/private Integer quantity; //개수
/**/private Double unitPrice; //개당 가격
    private Double taxAmount; //개당 세금
    private Double shippingAmount; //개당 택배비
    private String warehouseLocation; //창고에서 위치 ex) Aisle 3, Shelf A, Bin 5
    private List<ItemOption> options;
    private Long productId;
    private String fulfillmentSku; //할인코드
    private Boolean adjustment; //할인코드 있는지 여부
    private String upc; //universal product code

    //read-only
    private String createDate;

    //read-only
    private String modifyDate;

    @Builder
    public OrderItem(String sku, String name, Integer quantity, Double unitPrice) {
        this.sku = sku;
        this.name = name;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
    }
}
