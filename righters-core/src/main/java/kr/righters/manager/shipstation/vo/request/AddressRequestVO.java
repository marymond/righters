package kr.righters.manager.shipstation.vo.request;

import lombok.Builder;
import lombok.Data;

@Data
public class AddressRequestVO {
    private String name; // 이름
    private String company; //회사
    private String street1; //first line of 주소 ex) "1600 Pennsylvania Ave"
    private String street2; //second line of 주소 ex) "Oval Office"
    private String street3; //third line of 주소 null로 해도 됨.
    private String city; //도시 ex) "Washington"
    private String state; //주 ex) "DC"
    private String postalCode; //우편번호 ex) "20500"
    private String country; //국가 ex) "US"
    private String phone; // ex) "555-555-5555"
    private Boolean residential; //거주 여부

    @Builder
    public AddressRequestVO(String name, String street1, String street2, String city, String state, String postalCode, String country, String phone) {
        this.name = name;
        this.street1 = street1;
        this.street2 = street2;
        this.city = city;
        this.state = state;
        this.postalCode = postalCode;
        this.country = country;
        this.phone = phone;
    }
}
