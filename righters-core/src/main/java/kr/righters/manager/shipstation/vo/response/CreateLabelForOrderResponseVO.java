package kr.righters.manager.shipstation.vo.response;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CreateLabelForOrderResponseVO {
    private Long shipmentId; //배송Id
    private Double shipmentCost; //배송비
    private Double insuranceCost; //보험료
    private String trackingNumber; //운송장번호
    private String labelData; //잘 모르겠음.
    private String formData;
}
