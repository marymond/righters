package kr.righters.manager.shipstation.model;

import lombok.Data;

@Data
public class CustomsItem {
    private String customsItemId;
    private String description;
    private Integer quantity;
    private Double value;
    private String harmonizedTariffCode;
    private String countryOfOrigin;
}
