package kr.righters.manager.shipstation.model;

import lombok.Data;

@Data
public class ProductCategory {
    private Long categoryId;
    private String name;
}
