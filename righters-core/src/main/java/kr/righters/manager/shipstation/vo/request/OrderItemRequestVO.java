package kr.righters.manager.shipstation.vo.request;

import kr.righters.manager.shipstation.model.ItemOption;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
public class OrderItemRequestVO {

    private String lineItemKey; //OrderItem 식별자
/**/private String sku; //stock keeping unit
/**/private String name; //이름
    private String imageUrl; //상품 이미지 url
    private WeightRequestVO Weight; //무게
/**/private Integer quantity; //개수
/**/private Double unitPrice; //개당 가격
    private Double taxAmount = 0.0; //개당 세금
    private Double shippingAmount = 0.0; //개당 택배비
    private String           warehouseLocation; //창고에서 위치 ex) Aisle 3, Shelf A, Bin 5
    private List<ItemOption> options;
    private Long             productId;
    private String fulfillmentSku; //Stock keeping Unit for the fulfillment of that product by a 3rd party.
    private Boolean adjustment = false; //할인코드 있는지 여부
    private String upc; //universal product code

    @Builder
    public OrderItemRequestVO(String sku, String name, Integer quantity, Double unitPrice
                            , String fulfillmentSku) {
        this.sku = sku;
        this.name = name;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.fulfillmentSku = fulfillmentSku;
    }
}
