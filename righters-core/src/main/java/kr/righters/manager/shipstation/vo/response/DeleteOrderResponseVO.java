package kr.righters.manager.shipstation.vo.response;

import lombok.Data;

@Data
public class DeleteOrderResponseVO {
    String success;
    String message;
}
