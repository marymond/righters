package kr.righters.manager.shipstation.model;

import lombok.Data;

@Data
public class Webhook {
    private String resource_url; //트리거된 resource. 이 주소에 리퀘스트 날리면 order response 한다.
    private String resource_type; // "ORDER_NOTIFY", "ITEM_ORDER_NOTIFY", "SHIP_NOTIFY", "ITEM_SHIP_NOTIFY"
}
