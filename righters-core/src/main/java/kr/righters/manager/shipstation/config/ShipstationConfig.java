package kr.righters.manager.shipstation.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by yjnam on 2020. 03. 18..
 */
@Component
@ConfigurationProperties(prefix = "shipstation")
@Data
public class ShipstationConfig {

    private String apiHost;
    private String apiKey;
    private String apiSecret;
}
