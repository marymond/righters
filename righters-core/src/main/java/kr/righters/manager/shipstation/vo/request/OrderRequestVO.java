package kr.righters.manager.shipstation.vo.request;

import kr.righters.manager.shipstation.model.AdvancedOptions;
import kr.righters.manager.shipstation.model.Dimensions;
import kr.righters.manager.shipstation.model.InsuranceOptions;
import kr.righters.manager.shipstation.model.InternationalOptions;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
public class OrderRequestVO {

/**/private String orderNumber; // 유저가 정의한 오더 넘버(orderStatus가 같아도 중복가능)
    private String orderKey; //전체 order중에 유일한 값이어야 함.(orderStatus가 달라도 orderKey값이 같으면 안됨.)
/**/private String orderDate; //주문한 날짜

    private String paymentDate; //지불한 날짜
    private String shipByDate; //배송되기 전 또는 진행중인 날짜
/**/private String orderStatus; ///awaiting_payment, awaiting_shipment, shipped, on_hold, cancelled

    //read-only 이나 기존 고객의 경우 입력해도 되는 듯.
    private Long customerId; //고객 id, null로 하면 customer 자동 생성된다. response에는 null인데 shipstation가보면 생성되있음.

    private String customerUsername; //고객의 유저이름(고객의 마리몬드 아이디 넣기)
    private String          customerEmail; //고객 이메일
/**/private AddressRequestVO         billTo; // 카드 청구서 주소
/**/private AddressRequestVO         shipTo; //받는 주소
    private List<OrderItemRequestVO> items; //구매한 아이템 목록

    private Double amountPaid = 0.0; //토탈 금액
    private Double taxAmount = 0.0; //토탈 세금
    private Double shippingAmount = 0.0; //택배료
    private String customerNotes; // 주문시 고객이 작성한 메모
    private String internalNotes; //판매자에게만 보이는 비밀 노트
    private Boolean gift; //선물 여부
    private String giftMessage; //선물 메세지
    private String paymentMethod; // 고객이 사용한 구매 방법 ex) "Credit Card"
    private String requestedShippingService; // 고객이 선택한 배송 서비스 ex) "Priority Mail"
    private String carrierCode; //택배사코드 ex) "fedex"
    private String serviceCode; // 택배사의 서비스코드 ex) "fedex_2day"
    private String packageCode; //패키지타입 코드 ex) "package"
    private String confirmation; //none, delivery, signature, adult_signature, direct_signature
    private String shipDate; //배송 완료 날짜
    private WeightRequestVO weight; //무게
    private Dimensions           dimensions; //부피
    private InsuranceOptions     insuranceOptions; //보험 옵션
    private InternationalOptions internationalOptions; //국제 옵션
    private AdvancedOptions      advancedOptions;
    private List<Long> tagIds; //오더와 관련된 태그들

    @Builder
    public OrderRequestVO(String orderNumber, String orderDate, String orderStatus, String customerUsername, String customerEmail,
                          AddressRequestVO billTo, AddressRequestVO shipTo, List<OrderItemRequestVO> items,
                          Double shippingAmount, String customerNotes, AdvancedOptions advancedOptions){
        this.orderNumber = orderNumber;
        this.orderDate = orderDate;
        this.orderStatus = orderStatus;
        this.billTo = billTo;
        this.shipTo = shipTo;
        this.items = items;
        this.customerUsername = customerUsername;
        this.customerEmail = customerEmail;
        this.shippingAmount = shippingAmount;
        this.customerNotes = customerNotes;
        this.advancedOptions = advancedOptions;
    }
}
