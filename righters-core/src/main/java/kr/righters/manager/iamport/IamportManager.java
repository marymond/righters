package kr.righters.manager.iamport;


import kr.righters.manager.iamport.common.IamportConstants;
import kr.righters.manager.iamport.common.IamportResponseErrorHandler;
import kr.righters.manager.iamport.common.IamportRestTemplateInterceptor;
import kr.righters.manager.iamport.confing.IamportConfig;
import kr.righters.manager.iamport.vo.BillingKeyVO;
import kr.righters.manager.iamport.vo.PaymentResponseVO;
import kr.righters.manager.iamport.vo.PaymentsCancelResponseVO;
import kr.righters.manager.iamport.vo.TokenVO;
import kr.righters.manager.iamport.vo.common.IamportResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;

/**
 * Created by yjnam on 2019. 10. 4..
 */
@Slf4j
@Service
public class IamportManager {

    private static final String IAMPORT_HEADER_NAME = "X-ImpTokenHeader";

    @Autowired
    IamportConfig iamportConfig;

    private final RestTemplate restTemplate;
    private String currentAccessToken;
    private Long currentExpiredAt;

    /**
     *
     * @param restTemplateBuilder
     */
    public IamportManager(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
        this.restTemplate.setErrorHandler(new IamportResponseErrorHandler());
        this.restTemplate.setInterceptors(Collections.singletonList(new IamportRestTemplateInterceptor()));
        this.restTemplate.setRequestFactory(new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory()));
    }

    /**
     * 헤더정보 취득
     *
     * @return
     */
    private MultiValueMap getHeaderInfo() {
        //AccessToken이 없거나 만료되었을경우 취득
        if(currentAccessToken == null || currentAccessToken.isEmpty() || LocalDateTime.now().atZone(ZoneId.of("GMT")).toInstant().toEpochMilli() >= currentExpiredAt) {
            IamportResponse<TokenVO> response = postUsersGetToken();
            if (response.getCode() == 0) {
                currentAccessToken = response.getResponse().getAccessToken();
                currentExpiredAt = response.getResponse().getExpiredAt();
            }
        }

        //헤더정보
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(IAMPORT_HEADER_NAME, currentAccessToken);

        return headers;
    }

    /**
     * 토큰 취득
     * @return
     */
    public IamportResponse<TokenVO> postUsersGetToken() {

        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
        parameters.add("imp_key", iamportConfig.getApiKey());
        parameters.add("imp_secret", iamportConfig.getSecretKey());

        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(parameters, null);
        IamportResponse<TokenVO> iamportResponse = restTemplate.exchange(
                UriComponentsBuilder.fromHttpUrl(iamportConfig.getApiHost() + IamportConstants.API_USERS_GETTOKEN).build().encode().toUri()
                , HttpMethod.POST
                , requestEntity
                , new ParameterizedTypeReference<IamportResponse<TokenVO>>() {
                }).getBody();

        return iamportResponse;
    }

    /**
     * 결제 내역 조회
     * @param impUid
     * @return
     */
    public IamportResponse<PaymentResponseVO> getPaymentImpUid(String impUid){

        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(null, this.getHeaderInfo());
        IamportResponse<PaymentResponseVO> iamportResponse = restTemplate.exchange(
                UriComponentsBuilder.fromHttpUrl(iamportConfig.getApiHost() + IamportConstants.API_PAYMENTS_IMPUID.replace("{imp_uid}",impUid)).build().encode().toUri()
                , HttpMethod.GET
                , requestEntity
                , new ParameterizedTypeReference<IamportResponse<PaymentResponseVO>>() {
                }).getBody();

        return iamportResponse;
    }

    /**
     * 결제방법 추가
     * @param paymentMethodUid
     * @param cardNumber
     * @param expiry
     * @param birth
     * @param pw2digit
     * @return
     */
    public IamportResponse<BillingKeyVO> postSubscribeCustomersCustomerUid(String paymentMethodUid, String cardNumber, String expiry, String birth, String pw2digit) {

        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
        parameters.add("card_number", cardNumber);
        parameters.add("expiry", expiry);
        parameters.add("birth", birth);
        parameters.add("pwd_2digit", pw2digit);

        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(parameters, this.getHeaderInfo());
        IamportResponse<BillingKeyVO> iamportResponse = restTemplate.exchange(
                UriComponentsBuilder.fromHttpUrl(iamportConfig.getApiHost() + IamportConstants.API_SUBSCRIBE_CUSTOMERS_CUSTOMERUID.replace("{customer_uid}",paymentMethodUid)).build().encode().toUri()
                , HttpMethod.POST
                , requestEntity
                , new ParameterizedTypeReference<IamportResponse<BillingKeyVO>>() {
                }).getBody();

        return iamportResponse;
    }

    /**
     * 저장된 빌링키로 재결제를 하는 경우 사용됩니다.
     * @param paymentMethodUid
     * @param merchantUid
     * @param amount
     * @param name
     * @param buyerName
     * @param buyerTel
     * @param buyerAddr
     * @return
     */
    public IamportResponse<PaymentResponseVO> postSubscribePaymentsAgain(String paymentMethodUid, String merchantUid, Integer amount, String name, String buyerName, String buyerTel, String buyerAddr) {

        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
        parameters.add("customer_uid", paymentMethodUid);
        parameters.add("merchant_uid", merchantUid);
        parameters.add("amount", String.valueOf(amount));
        parameters.add("name", name);
        parameters.add("buyer_name", buyerName);
//        parameters.add("buyer_email", );
        parameters.add("buyer_tel", buyerTel);
        parameters.add("buyer_addr", buyerAddr);


        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(parameters, this.getHeaderInfo());
        IamportResponse<PaymentResponseVO> iamportResponse = restTemplate.exchange(
                UriComponentsBuilder.fromHttpUrl(iamportConfig.getApiHost() + IamportConstants.API_SUBSCRIBE_PAYMENTS_AGAIN).build().encode().toUri()
                , HttpMethod.POST
                , requestEntity
                , new ParameterizedTypeReference<IamportResponse<PaymentResponseVO>>() {
                }).getBody();

        return iamportResponse;
    }


    /**
     * 결제 취소처리
     * @param impUid
     * @param reason
     * @return
     */
    public IamportResponse<PaymentsCancelResponseVO> postPaymentsCancel(String impUid, String reason) {

        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
        parameters.add("imp_uid", impUid);
        parameters.add("reason", reason);

        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(parameters, this.getHeaderInfo());
        IamportResponse<PaymentsCancelResponseVO> iamportResponse = restTemplate.exchange(
                UriComponentsBuilder.fromHttpUrl(iamportConfig.getApiHost() + IamportConstants.API_PAYMENTS_CANCEL).build().encode().toUri()
                , HttpMethod.POST
                , requestEntity
                , new ParameterizedTypeReference<IamportResponse<PaymentsCancelResponseVO>>() {
                }).getBody();

        return iamportResponse;
    }
}
