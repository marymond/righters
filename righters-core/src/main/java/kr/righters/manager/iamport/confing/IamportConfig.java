package kr.righters.manager.iamport.confing;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by yj.nam 2019.10.4
 */
@Component
@ConfigurationProperties(prefix = "iamport")
@Data
public class IamportConfig {

    private String apiHost;
    private String merchantId;
    private String apiKey;
    private String secretKey;
}
