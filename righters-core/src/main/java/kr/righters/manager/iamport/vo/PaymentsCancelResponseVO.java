package kr.righters.manager.iamport.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

@Data
public class PaymentsCancelResponseVO {
    @JsonProperty("imp_uid")
    private String impUid = "";

    @JsonProperty("merchant_uid")
    private String merchantUid = "";

    @JsonProperty("pay_method")
    String              payMethod;

    String              channel;

    @JsonProperty("pg_provider")
    String              pgProvider;

    @JsonProperty("pg_tid")
    String              pgTid;

    @JsonProperty("pg_id")
    String              pgId;

    boolean             escrow;

    @JsonProperty("apply_num")
    String              applyNum;

    @JsonProperty("bank_code")
    String              bankCode;

    @JsonProperty("bank_name")
    String              bankName;

    @JsonProperty("card_code")
    String              cardCode;

    @JsonProperty("card_name")
    String              cardName;

    @JsonProperty("card_quota")
    int                 cardQuota;

    @JsonProperty("vbank_code")
    String              vbankCode;

    @JsonProperty("vbank_name")
    String              vbankName;

    @JsonProperty("vbank_num")
    String              vbankNum;

    @JsonProperty("vbank_holder")
    String              vbankHolder;

    @JsonProperty("vbank_date")
    LocalDateTime       vbankDate;

    String              name;

    int                 amount;

    @JsonProperty("cancel_amount")
    int                 cancelAmount;

    String              currency;

    @JsonProperty("buyer_name")
    String              buyerName;

    @JsonProperty("buyer_email")
    String              buyerEmail;

    @JsonProperty("buyer_tel")
    String              buyerTel;

    @JsonProperty("buyer_addr")
    String              buyerAddr;

    @JsonProperty("buyer_postcode")
    String              buyerPostcode;

    @JsonProperty("custom_data")
    String              customData;

    @JsonProperty("user_agent")
    String              userAgent;

    String              status;

    @JsonProperty("paid_at")
    LocalDateTime       paidAt;

    @JsonProperty("failed_at")
    LocalDateTime       failedAt;

    @JsonProperty("cancelled_at")
    LocalDateTime       cancelledAt;

    @JsonProperty("fail_reason")
    String              failReason;

    @JsonProperty("cancel_reason")
    String              cancelReason;

    @JsonProperty("receipt_url")
    String              receiptUrl;

    @JsonProperty("cancel_history")
    List<CancelHistoryVO> cancelHistory;

    @JsonProperty("cancel_receipt_urls")
    List<String>        cancelReceiptUrls;

    @JsonProperty("cash_receipt_issued")
    boolean             cashReceiptIssued;

    public void setVbankDate(int vbankDate){
        //set 메소드 재정의 date형 변환을 위해
        this.vbankDate = LocalDateTime.ofInstant(Instant.ofEpochSecond(vbankDate), ZoneId.of("Asia/Seoul"));
    }

    public void setPaidAt(int paidAt){
        //set 메소드 재정의 date형 변환을 위해
        this.paidAt = LocalDateTime.ofInstant(Instant.ofEpochSecond(paidAt), ZoneId.of("Asia/Seoul"));
    }

    public void setFailedAt(int failedAt){
        //set 메소드 재정의 date형 변환을 위해
        this.failedAt = LocalDateTime.ofInstant(Instant.ofEpochSecond(failedAt), ZoneId.of("Asia/Seoul"));
    }

    public void setCancelledAt(int cancelledAt){
        //set 메소드 재정의 date형 변환을 위해
        this.cancelledAt = LocalDateTime.ofInstant(Instant.ofEpochSecond(cancelledAt), ZoneId.of("Asia/Seoul"));
    }

    @Data
    public static class CancelHistoryVO{

        @JsonProperty("pg_iid")
        String pgIid;

        int amount;

        @JsonProperty("cancelled_at")
        LocalDateTime cancelledAt;

        String reason;

        @JsonProperty("receipt_url")
        String receiptUrl;

        public void setCancelledAt(int cancelledAt)
        {
            this.cancelledAt = LocalDateTime.ofInstant(Instant.ofEpochSecond(cancelledAt), ZoneId.of("Asia/Seoul"));
        }
    }
}
