package kr.righters.manager.iamport.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class PaymentResponseVO {
    @JsonProperty("imp_uid")
    private String impUid;

    @JsonProperty("merchant_uid")
    private String merchantUid; //가맹점에서 생성/관리하는 고유 주문번호

    @JsonProperty("pay_method")
    private String payMethod;

    @JsonProperty("pg_provider")
    private String pgProvider;

    @JsonProperty("pg_tid")
    private String pgTid;

    private Boolean escrow;

    @JsonProperty("apply_num")
    private String applyNum;

    @JsonProperty("card_name")
    private String cardName;

    @JsonProperty("card_quota")
    private Integer cardQuota;

    @JsonProperty("vbank_name")
    private String vbankName;

    @JsonProperty("vbank_num")
    private String vbankNum;

    @JsonProperty("vbank_holder")
    private String vbankHolder;

    @JsonProperty("vbank_date")
    private Integer vbankDate;

    private String name; //주문명

    private Long amount; //결제할 금액

    @JsonProperty("cancel_amount")
    private String cancelAmount;

    @JsonProperty("buyer_name")
    private String buyerName;

    @JsonProperty("buyer_email")
    private String buyerEmail;

    @JsonProperty("buyer_tel")
    private String buyerTel;

    @JsonProperty("buyer_addr")
    private String buyerAddr;

    @JsonProperty("buyer_postcode")
    private String buyerPostcode;

    @JsonProperty("custom_data")
    private String customData;

    @JsonProperty("user_agent")
    private String userAgent;

    private String status; //결제상태 (ready(미결제), paid(결제완료), cancelled(결제취소, 부분취소포함), failed(결제실패))

    @JsonProperty("paid_at")
    private Integer paidAt;

    @JsonProperty("failed_at")
    private Integer failedAt;

    @JsonProperty("cancelled_at")
    private Integer cancelledAt;

    @JsonProperty("fail_reason")
    private String failReason;

    @JsonProperty("cancel_reason")
    private String cancelReason;

    @JsonProperty("receipt_url")
    private String receiptUrl;

    @JsonProperty("cancel_receipt_urls")
    private String[] cancelReceiptUrls;
}
