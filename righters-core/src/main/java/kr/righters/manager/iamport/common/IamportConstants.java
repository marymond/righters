package kr.righters.manager.iamport.common;

/**
 * Created by yjnam on 2019. 10. 4..
 */
public class IamportConstants {

    //REST API사용을 위한 인증(access_token취득)
    public static final String API_USERS_GETTOKEN = "/users/getToken"; //access_token취득(POST)

    //카카오페이전용 API

    //네이버페이전용 API

    //payments: 결제내역조회 및 결제취소
    public static final String API_PAYMENTS_IMPUID = "/payments/{imp_uid}"; //아임포트 고유번호로 결제내역을 확인

    public static final String API_PAYMENTS_CANCEL ="/payments/cancel"; //결제취소

    //subscribe: ActiveX없는 간편결제, 정기예약결제기능
    public static final String API_SUBSCRIBE_PAYMENTS_AGAIN = "/subscribe/payments/again";//저장된 빌링키로 재결제를 하는 경우 사용됩니다. /subscribe/payments/onetime 또는 /subscribe/customers/{customer_uid} 로 등록된 빌링키가 있을 때 매칭되는 customer_uid로 재결제를 진행할 수 있습니다.

    //subscribe확장기능. 구매자빌키관리
    public static final String API_SUBSCRIBE_CUSTOMERS_CUSTOMERUID = "/subscribe/customers/{customer_uid}"; //구매자에 대해 빌링키 발급 및 저장(POST), 삭제(DELETE), 조회(GET)


}

