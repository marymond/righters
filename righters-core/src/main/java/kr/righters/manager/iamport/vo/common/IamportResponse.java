package kr.righters.manager.iamport.vo.common;

import lombok.Data;

@Data
public class IamportResponse<T> {
    private T response;
    private Integer code;
    private String message;
}
