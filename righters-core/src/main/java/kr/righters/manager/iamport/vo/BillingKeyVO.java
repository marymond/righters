package kr.righters.manager.iamport.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class BillingKeyVO {
    @JsonProperty("customer_uid")
    private String customerUid; //구매자 고유번호

    @JsonProperty("card_name")
    private String cardName; //카드번호

    private Integer inserted; //기한
    private Integer updated;
}
