package kr.righters.manager.iamport.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TokenVO {
    @JsonProperty("access_token")
    private String    accessToken; //접근토큰

    private Long now;

    @JsonProperty("expired_at")
    private Long expiredAt; //기한
}
