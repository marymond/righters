package kr.righters.manager.sweettracker.common;

/**
 * Created by yj.nam on 2020. 09. 10..
 */
public class SweettrackerConstants {

    //택배사 조회
    public static final String API_V1_COMPANYLIST = "/api/v1/companylist";

    //추천 택배사 정보가져오기
    public static final String API_V1_RECOMMEND = "/api/v1/recommend";

    //운송장 번호 조회
    public static final String API_V1_TRACKINGINFO = "/api/v1/trackingInfo";
}
