package kr.righters.manager.sweettracker.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

@Data
public class TrackingInfoResponseVO {
    @JsonProperty("result")
    private String result; //조회결과

    @JsonProperty("senderName")
    private String senderName;

    @JsonProperty("receiverName")
    private String receiverName;

    @JsonProperty("itemName")
    private String itemName; //상품이름

    @JsonProperty("invoiceNo")
    private String invoiceNo; // 운송장 번호

    @JsonProperty("receiverAddr")
    private String receiverAddr;

    @JsonProperty("orderNumber")
    private String orderNumber; //주문번호

    @JsonProperty("adUrl")
    private String adUrl; // 택배사에서 광고용으로 사용하는 주소

    @JsonProperty("estimate")
    private String estimate; // 배송예정시간

    @JsonProperty("level")
    private Integer level; //진행단계 [level 1: 배송준비중, 2: 집화완료, 3: 배송중, 4: 지점 도착, 5: 배송출발, 6:배송 완료]

    @JsonProperty("complete")
    private boolean complete; // 배송완료 여부(true, false)

    @JsonProperty("recipient")
    private String recipient;

    @JsonProperty("itemImage")
    private String itemImage; //상품이미지URL

    @JsonProperty("trackingDetails")
    private List<Detail> trackingDetails;

    @JsonProperty("productInfo")
    private String productInfo; //상품정보

    @JsonProperty("zipCode")
    private String zipCode; //zipCode

    @JsonProperty("lastDetail")
    private Detail lastDetail;

    @JsonProperty("lastStateDetail")
    private Detail lastStateDetail;

    @JsonProperty("firstDetail")
    private Detail firstDetail;

    @JsonProperty("completeYN")
    private String completeYN; // 배송완료여부 (Y, N)

    @Data
    public static class Detail{
        @JsonProperty("time")
        LocalDateTime time;
        @JsonProperty("timeString")
        String timeString;
        @JsonProperty("code")
        String code;
        @JsonProperty("where")
        String where;
        @JsonProperty("kind")
        String kind;
        @JsonProperty("telno")
        String telno;
        @JsonProperty("telno2")
        String telno2;
        @JsonProperty("remark")
        String remark;
        @JsonProperty("level")
        Integer level;
        @JsonProperty("manName")
        String manName;
        @JsonProperty("manPic")
        String manPic;

        public void setTime(int time){
            this.time = LocalDateTime.ofInstant(Instant.ofEpochSecond(time), ZoneId.of("Asia/Seoul"));
        }
    }
}
