package kr.righters.manager.sweettracker;

import kr.righters.manager.sweettracker.common.SweettrackerConstants;
import kr.righters.manager.sweettracker.common.SweettrackerResponseErrorHandler;
import kr.righters.manager.sweettracker.common.SweettrackerRestTemplateInterceptor;
import kr.righters.manager.sweettracker.config.SweettrackerConfiguration;
import kr.righters.manager.sweettracker.vo.CompanyListResponseVO;
import kr.righters.manager.sweettracker.vo.RecommendResponseVO;
import kr.righters.manager.sweettracker.vo.TrackingInfoResponseVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;

/**
 * Created by yjnam on 2019. 10. 4..
 */
@Slf4j
@Service
public class SweettrackerManager {

    private static final String IAMPORT_HEADER_NAME = "X-ImpTokenHeader";

    @Autowired
    SweettrackerConfiguration sweettrackerConfiguration;
    private final RestTemplate restTemplate;

    /**
     *
     * @param restTemplateBuilder
     */
    public SweettrackerManager(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
        this.restTemplate.setErrorHandler(new SweettrackerResponseErrorHandler());
        this.restTemplate.setInterceptors(Collections.singletonList(new SweettrackerRestTemplateInterceptor()));
        this.restTemplate.setRequestFactory(new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory()));
    }


    /**
     * 상품 배송정보 취득
     * @param invoice
     * @param code
     * @return
     */
    public TrackingInfoResponseVO getTracingInfo(String invoice, String code) {

        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
        parameters.add("t_key", sweettrackerConfiguration.getApiKey());
        parameters.add("t_invoice", invoice);
        parameters.add("t_code",code);

        TrackingInfoResponseVO response = restTemplate.exchange(
                UriComponentsBuilder.fromHttpUrl(sweettrackerConfiguration.getApiHost() + SweettrackerConstants.API_V1_TRACKINGINFO).queryParams(parameters).build().encode().toUri()
                , HttpMethod.GET
                , null
                , new ParameterizedTypeReference<TrackingInfoResponseVO>() {
                }).getBody();

        return response;
    }

    /**
     * 택배사 리스트 취득
     * @return
     */
    public CompanyListResponseVO getCompanyList() {

        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
        parameters.add("t_key", sweettrackerConfiguration.getApiKey());

        CompanyListResponseVO response = restTemplate.exchange(
                UriComponentsBuilder.fromHttpUrl(sweettrackerConfiguration.getApiHost() + SweettrackerConstants.API_V1_COMPANYLIST).queryParams(parameters).build().encode().toUri()
                , HttpMethod.GET
                , null
                , new ParameterizedTypeReference<CompanyListResponseVO>() {
                }).getBody();

        return response;
    }

    /**
     * 택배사 리스트 취득
     * @param invoice
     * @return
     */
    public RecommendResponseVO getRecommend(String invoice) {

        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
        parameters.add("t_key", sweettrackerConfiguration.getApiKey());
        parameters.add("t_invoice", invoice);

        RecommendResponseVO response = restTemplate.exchange(
                UriComponentsBuilder.fromHttpUrl(sweettrackerConfiguration.getApiHost() + SweettrackerConstants.API_V1_RECOMMEND).queryParams(parameters).build().encode().toUri()
                , HttpMethod.GET
                , null
                , new ParameterizedTypeReference<RecommendResponseVO>() {
                }).getBody();

        return response;
    }

}
