package kr.righters.manager.sweettracker.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by yj.nam on 2020. 09. 10..
 */
@Component
@ConfigurationProperties(prefix="sweettracker")
@Data
public class SweettrackerConfiguration {

    private String apiKey;
    private String apiHost;
}
