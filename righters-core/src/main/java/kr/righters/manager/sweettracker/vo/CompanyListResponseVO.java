package kr.righters.manager.sweettracker.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class CompanyListResponseVO {
    @JsonProperty("Company")
    private List<DeliveryCompanyInfo> company;

    @Data
    public static class DeliveryCompanyInfo{
        @JsonProperty("Code")
        String code;
        @JsonProperty("Name")
        String name;
    }
}
