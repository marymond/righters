package kr.righters.manager.sweettracker.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class RecommendResponseVO {
    @JsonProperty("Recommend")
    private List<DeliveryCompanyInfo> recommend;

    @Data
    public static class DeliveryCompanyInfo{
        @JsonProperty("Code")
        String code;
        @JsonProperty("Name")
        String name;
    }
}
