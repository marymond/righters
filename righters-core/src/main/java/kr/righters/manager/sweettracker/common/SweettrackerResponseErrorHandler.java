package kr.righters.manager.sweettracker.common;

import kr.righters.exception.CustomException;
import org.apache.commons.io.IOUtils;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by yj.nam on 2020. 09. 10..
 */
public class SweettrackerResponseErrorHandler implements ResponseErrorHandler {

    private org.springframework.web.client.ResponseErrorHandler errorHandler = new DefaultResponseErrorHandler();

    @Override
    public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
        return errorHandler.hasError(clientHttpResponse);
    }

    @Override
    public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {

        StringWriter writer = new StringWriter();
        IOUtils.copy(clientHttpResponse.getBody(), writer, "UTF-8");
        String theString = writer.toString();

        CustomException     customException = new CustomException();
        Map<String, Object> properties      = new HashMap<String, Object>();
        properties.put("code", clientHttpResponse.getStatusCode().toString());
        properties.put("body", clientHttpResponse.getBody().toString());
        properties.put("header", clientHttpResponse.getHeaders());
        customException.setMessage(theString);
        throw customException;
    }
}
