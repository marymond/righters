package kr.righters.manager;

import kr.righters.config.ApplicationProperty;
import kr.righters.domain.entity.*;
import kr.righters.exception.CustomJsonException;
import kr.righters.service.OrderedService;
import com.stripe.Stripe;
import com.stripe.exception.SignatureVerificationException;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import com.stripe.model.Event;
import com.stripe.model.Refund;
import com.stripe.model.checkout.Session;
import com.stripe.net.Webhook;
import com.stripe.param.RefundCreateParams;
import com.stripe.param.checkout.SessionCreateParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by yj.nam on 20.01.31..
 */
@Slf4j
@Service
public class StripeManager {

    @Value("${stripe.keys.public}")
    private String publicKey; //공개키

    @Value("${stripe.keys.secret}")
    private String secretKey; //비밀키

    @Value("${stripe.endpointSecret}")
    private String endpointSecret; //

    @Autowired
    private ApplicationProperty applicationProperty;

    @Autowired
    private OrderedService orderedService;

    @PostConstruct
    public void initialize(){
        //스트라이프 초기화(비밀키 설정)
        Stripe.apiKey = secretKey;
    }

    /**
     * 세션상태 조회하기
     * @param sessionId
     * @return
     */
    public String getPaymentIntent(String sessionId){
        Session session = null;
        try {
            session = Session.retrieve(sessionId);
        } catch (StripeException e) {
            log.error(String.format("Session Retrieve error: {}", sessionId));
        }

        return session != null ? session.getPaymentIntent() : null;
    }

    /**
     * 공개키 취득
     * @return
     */
    public String getPublicKey(){
        return publicKey;
    }

    /**
     * 스트라이프 세션 생성 (결제요청시 사용)
     * @param carts 카트내용
     * @return
     * @throws StripeException
     */
    public String createSession(List<Cart> carts, UserAddress shippingAddress, User user, boolean buynowFlag) {
        List<SessionCreateParams.LineItem> lineItems = new ArrayList<>();
        BigDecimal            totalSalePrice = new BigDecimal("0");
        BigDecimal            totalShippingFee = new BigDecimal("0");
        Map<String, Shipping> shippingMap      = new HashMap<>();
        for(Cart cart : carts){
            BigDecimal salePrice = new BigDecimal(cart.getProduct().getSalePrice());
            BigDecimal priceCent = salePrice.multiply(new BigDecimal("100"));

            SessionCreateParams.LineItem item = new SessionCreateParams.LineItem.Builder()
                    .setName(cart.getProduct().getName() + " / " + cart.getProductOption().getName())//제품이름(필수)
                    .setAmount(priceCent.longValue())//제품 한개가격(필수)
                    .setQuantity((long)cart.getQuantity())//제품 갯수(필수)
                    .setCurrency("usd")//통화(필수)
                    .addImage(cart.getProduct().getMainImageUrl()) //제품 이미지URL
                    .build();

            lineItems.add(item);

            //총합계산
            BigDecimal quantityBig = new BigDecimal(cart.getQuantity());
            totalSalePrice = totalSalePrice.add(salePrice.multiply(quantityBig));

            Shipping shipping = shippingMap.get(cart.getProduct().getShipping().getId());
            if(shipping == null){
                totalShippingFee = totalShippingFee.add(new BigDecimal(cart.getProduct().getShipping().getShippingFee()));
            }

            shippingMap.put(cart.getProduct().getShipping().getId(), shipping);
        }

        //배송료
        if(totalShippingFee.intValue() != 0){
            SessionCreateParams.LineItem item = new SessionCreateParams.LineItem.Builder()
                    .setName("Shipping Fee")
                    .setAmount(totalShippingFee.multiply(new BigDecimal("100")).longValue())
                    .setQuantity(1L)//제품 갯수(필수)
                    .setCurrency("usd")//통화(필수)
//                .addImage() //제품 이미지URL
                    .build();

            lineItems.add(item);
        }

        SessionCreateParams sessionCreateParams = new SessionCreateParams.Builder()
                .setSuccessUrl(applicationProperty.getApplicationHost() + "/stripe/success?session_id={CHECKOUT_SESSION_ID}")//성공후 호출URL(필수)
                .setCancelUrl(applicationProperty.getApplicationHost() +"/canceled?session_id={CHECKOUT_SESSION_ID}")//취소시 호출URL(필수)
                //결제했었던 고객은 stripe에 고객으로 등록이 되어 있다.
                //.setCustomer("cus_GUrlABRgSrwIKR")
                .addAllLineItem(lineItems)
                .addPaymentMethodType(SessionCreateParams.PaymentMethodType.CARD)//결제형태(필수)
                //.setMode(SessionCreateParams.Mode.SETUP)
                .build();

        Session session = null;
        try {
            session = Session.create(sessionCreateParams);
        } catch (StripeException e) {
            log.error(String.format("Session Create error carts"));
            e.printStackTrace();
            throw new CustomJsonException("An error occurred during payment.");
        }

        if(session != null){
            //Order 저장처리
            orderedService.createOrder(Ordered.PaymentType.STRIPE, carts, shippingAddress, totalSalePrice.longValue(),totalShippingFee.longValue(), totalSalePrice.add(totalShippingFee).longValue(),session.getId(), user, buynowFlag);
        }
        return session != null ? session.getId() : null;

    }

    /**
     * 스트라이프 환불
     * @param paymentIntentId
     * @param refundAmountStr 환불금액
     */
    public void refund(String paymentIntentId, String refundAmountStr){
        long refundAmount = new BigDecimal(refundAmountStr).multiply(new BigDecimal(100)).longValue();
        try {
            Refund refund = Refund.create(RefundCreateParams.builder()
                    .setPaymentIntent(paymentIntentId)
                    .setAmount(refundAmount) //환불금액(제품별환불의 경우 부분적으로 진행) 단위:Cent
                    .build());

            if("succeeded".equals(refund.getStatus())){
                //환불완료처리
                orderedService.refund(refund.getPaymentIntent());
            }

        } catch (StripeException e) {
            log.error(String.format("refund create error paymentIntentId: {}, refundAmount: {}", paymentIntentId, refundAmount));
        }
    }

    /**
     * 웹훅처리
     * @param payload
     * @param sigHeader
     */
    public void webhook(String payload, String sigHeader){
        Event event = null;
        try {
            log.debug("Request payload : {}", payload);
            event = Webhook.constructEvent(payload, sigHeader, endpointSecret);
        } catch (SignatureVerificationException e) {
            log.error("webhook construct event error");
            return;
        }

        switch (event.getType()) {
            case "checkout.session.completed":
                //결제완료 처리
                Session session = (Session)event.getDataObjectDeserializer().getObject().get();
                orderedService.checkOutComplete(session.getId(), session.getPaymentIntent());

                break;
            case "charge.refunded":
                Charge charge = (Charge)event.getDataObjectDeserializer().getObject().get();

                if("succeeded".equals(charge.getStatus())){
                    //환불완료처리
                    orderedService.refund(charge.getPaymentIntent());
                }
                break;
            default:
                log.info(String.format("처리내용이 없는 Webhook입니다. webhook타입: {}", event.getType()));
        }
    }


}
