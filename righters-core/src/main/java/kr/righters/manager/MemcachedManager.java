package kr.righters.manager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import kr.righters.domain.common.ManagerToken;

import kr.righters.domain.common.UserToken;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.spy.memcached.MemcachedClient;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by yj.nam on 19.10.29..
 */
@Service
public class MemcachedManager {

    @Autowired
    MemcachedClient memcachedClient;

    //------------------------------------관리자 토큰관련------------------------------------------------
    public void setManagerToken(ManagerToken managerToken) {

        Gson         gson = new GsonBuilder().create();
        StringBuffer key  = new StringBuffer(Key.MANAGER_TOKEN.getPrefix()).append(managerToken.getToken());

        memcachedClient.set(key.toString(), Key.MANAGER_TOKEN.expiration, gson.toJson(managerToken));

    }

    public ManagerToken getManagerToken(String token) {

        Gson gson = new GsonBuilder().create();

        StringBuffer key  = new StringBuffer(Key.MANAGER_TOKEN.getPrefix()).append(token);
        String json = (String) memcachedClient.get(key.toString());
        if (StringUtils.isEmpty(json)) {
            return null;
        }
        return gson.fromJson(json, ManagerToken.class);
    }

    public void deleteManagerToken(String token) {

        StringBuffer key  = new StringBuffer(Key.MANAGER_TOKEN.getPrefix()).append(token);
        memcachedClient.delete(key.toString());

    }

    //------------------------------------사용자 토큰관련------------------------------------------------
    public void setUserToken(UserToken userTokenVO) {

        Gson gson = new GsonBuilder().create();
        StringBuffer key  = new StringBuffer(Key.USER_TOKEN.getPrefix()).append(userTokenVO.getToken());

        memcachedClient.set(key.toString(), Key.USER_TOKEN.expiration, gson.toJson(userTokenVO));

    }

    public UserToken getUserToken(String token) {

        Gson gson = new GsonBuilder().create();

        StringBuffer key  = new StringBuffer(Key.USER_TOKEN.getPrefix()).append(token);
        String json = (String) memcachedClient.get(key.toString());
        if (StringUtils.isEmpty(json)) {
            return null;
        }
        return gson.fromJson(json, UserToken.class);
    }

    public void deleteUserToken(String token) {

        StringBuffer key  = new StringBuffer(Key.USER_TOKEN.getPrefix()).append(token);
        memcachedClient.delete(key.toString());

    }

    @AllArgsConstructor
    @Getter
    public enum Key {

        MANAGER_TOKEN("MTK:",60*60*24,"관리자 토큰")
        ,USER_TOKEN("UTK:",60*60,"사용자 토큰");

        private String prefix;
        private int expiration; //만료기간(관리자 : 하루, 사용자:1시간)
        private String description;

    }
}
