package kr.righters.manager.paypal;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import kr.righters.domain.entity.*;
import kr.righters.service.OrderedService;
import com.paypal.http.HttpResponse;
import com.paypal.orders.*;
import com.paypal.payments.CapturesRefundRequest;
import com.paypal.payments.RefundRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by yj.nam on 20.01.31..
 */
@Slf4j
@Service
public class PaypalManager {

    @Autowired
    private PaypalClient payPalClient;

    @Autowired
    private OrderedService orderedService;

    /**
     * 주문ID 생성(결제전)
     * @param carts
     * @return
     */
    public String createOrder(List<Cart> carts, UserAddress shippingAddress, User user, boolean buynowFlag) {

        OrdersCreateRequest request = new OrdersCreateRequest();
        request.prefer("return=representation");

        OrderRequest orderRequest = new OrderRequest();
        orderRequest.checkoutPaymentIntent("CAPTURE");

//        ApplicationContext applicationContext = new ApplicationContext().brandName("MARIMOND ^L^").landingPage("BILLING").shippingPreference("SET_PROVIDED_ADDRESS");
//        orderRequest.applicationContext(applicationContext);

        //아이템 목록들
        List<Item> items = new ArrayList<>();
        BigDecimal            totalSalePrice = new BigDecimal("0");
        BigDecimal            totalShippingFee = new BigDecimal("0");
        Map<String, Shipping> shippingMap    = new HashMap<>();
        for(Cart cart : carts){
            items.add(new Item()
                    .name(cart.getProduct().getName()) //제품이름
                    .sku(cart.getProductOption().getName()) //옵션이름
                    .unitAmount(new Money().currencyCode("USD").value(String.valueOf(cart.getProduct().getSalePrice()))) //제품세일가격
                    .quantity(String.valueOf(cart.getQuantity())) //제품갯수
            );

            //총합계산
            BigDecimal salePrice   = new BigDecimal(cart.getProduct().getSalePrice());
            BigDecimal quantityBig = new BigDecimal(cart.getQuantity());
            totalSalePrice = totalSalePrice.add(salePrice.multiply(quantityBig));

            Shipping shipping = shippingMap.get(cart.getProduct().getShipping().getId());
            if(shipping == null){
                totalShippingFee = totalShippingFee.add(new BigDecimal(cart.getProduct().getShipping().getShippingFee()));
            }

            shippingMap.put(cart.getProduct().getShipping().getId(), shipping);
        }

        //총 가격
        AmountWithBreakdown amountWithBreakdown = new AmountWithBreakdown()
                .currencyCode("USD")
                .value(totalSalePrice.add(totalShippingFee).toPlainString())
                .amountBreakdown(new AmountBreakdown()
                                .itemTotal(new Money().currencyCode("USD").value(totalSalePrice.toPlainString())) //아이템들의 개별 가격 * 수량의 합
                                .shipping(new Money().currencyCode("USD").value(totalShippingFee.toPlainString()))
                );

        List<PurchaseUnitRequest> purchaseUnitRequests = new ArrayList<PurchaseUnitRequest>();
        PurchaseUnitRequest purchaseUnitRequest = new PurchaseUnitRequest()
                .amountWithBreakdown(amountWithBreakdown)
                .items(items);

        purchaseUnitRequests.add(purchaseUnitRequest);
        orderRequest.purchaseUnits(purchaseUnitRequests);

        request.requestBody(orderRequest);

        //3. Call PayPal to set up the transaction
        //v2/checkout/orders 호출
        HttpResponse<Order> response = null;
        try{
            response = payPalClient.client().execute(request);
        }catch (Exception e){
            log.error(String.format("Paypal Create Order Error"));
            e.printStackTrace();
        }

        GsonBuilder builder = new GsonBuilder();
        Gson            gson    = builder.create();
        log.info("Order Create Response : {}", gson.toJson(response));

        if (response.statusCode() == 201) {
            //정상 주문장생성완료시 처리
            //Order 저장처리
            orderedService.createOrder(Ordered.PaymentType.PAYPAL, carts, shippingAddress, totalSalePrice.longValue(),totalShippingFee.longValue(), totalSalePrice.add(totalShippingFee).longValue(),response.result().id(), user, buynowFlag);

        }
        return response !=null ? response.result().id() : null;
    }


    /**
     * 결제 처리
     * @param orderId
     * @return
     */
    public boolean captureOrder(String orderId) {
        OrdersCaptureRequest request = new OrdersCaptureRequest(orderId);
        request.requestBody(new OrderRequest());

        //3. Call PayPal to capture an order
        HttpResponse<Order> response = null;
        try {
            response = payPalClient.client().execute(request);
        }catch (Exception e){
            orderedService.checkOutFail(orderId);
            log.error(String.format("Paypal Capture Order Error"));
            e.printStackTrace();
            return false;
        }

        GsonBuilder builder = new GsonBuilder();
        Gson            gson    = builder.create();
        log.info("Capture Order Response : {}", gson.toJson(response));

        if (response != null && response.statusCode() == 201) {
            String captureId = null;
            for (PurchaseUnit purchaseUnit : response.result().purchaseUnits()) {
                for (Capture capture : purchaseUnit.payments().captures()) {
                    captureId = capture.id();
                }
            }
            if(captureId != null){
                orderedService.checkOutComplete(orderId,captureId);
            }
        }

        return true;
    }

    /**
     * 주문 조회
     * @param orderId
     * @return
     */
    public Order getOrder(String orderId){
        OrdersGetRequest request = new OrdersGetRequest(orderId);
        //3. Call PayPal to get the transaction
        HttpResponse<Order> response = null;
        try {
            response = payPalClient.client().execute(request);
            //4. Save the transaction in your database. Implement logic to save transaction to your database for future reference.
        } catch (Exception e) {
            log.error(String.format("Paypal Get Order Error"));
            e.printStackTrace();
        }

        GsonBuilder builder = new GsonBuilder();
        Gson            gson    = builder.create();
        log.info("Get Order Response : {}", gson.toJson(response));

        return response.result();
    }

    /**
     * Method to Refund the Capture. valid capture Id should be passed.
     *
     * @param captureId
     * @return
     */
    public void refund(String captureId, String refundAmountStr){

        CapturesRefundRequest request = new CapturesRefundRequest(captureId);
        request.prefer("return=representation");
        request.requestBody(new RefundRequest().amount(new com.paypal.payments.Money().currencyCode("USD").value(refundAmountStr)));

        HttpResponse<com.paypal.payments.Refund> response = null;

        try {
            response = payPalClient.client().execute(request);
        }catch (Exception e){
            log.error(String.format("Paypal Capture Order Error"));
            e.printStackTrace();
            return;
        }

        GsonBuilder builder = new GsonBuilder();
        Gson            gson    = builder.create();
        log.info("Refund Order Response : {}", gson.toJson(response));

        if (response.statusCode() == 201) {
            //환불정상처리 되었을경우 처리
            orderedService.refund(captureId);
        }
    }


    /**
     * 웹훅 처리
     * @param requestMap
     */
    public void webhook(Map<String, Object> requestMap){

        Map<String,Object> resource = (Map<String,Object>) requestMap.get("resource");
        String captureId = (String)resource.get("id");

        String event = (String) requestMap.get("event_type");

        switch (event) {
            case "PAYMENT.CAPTURE.COMPLETED":
                //TODO 결제완료 처리(확인후 추가)
//                orderedService.checkOutComplete(session.getId(), captureId);

                break;

            default:
                log.info(String.format("처리내용이 없는 Webhook입니다. webhook타입: {}", event));
        }
    }

}
