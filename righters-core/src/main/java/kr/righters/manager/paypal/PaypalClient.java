package kr.righters.manager.paypal;

import kr.righters.config.ApplicationProperty;
import com.paypal.core.PayPalEnvironment;
import com.paypal.core.PayPalHttpClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by yj.nam on 20.02.03..
 */
@Slf4j
@Component
public class PaypalClient {

    @Autowired
    ApplicationProperty applicationProperty;

    @Value("${paypal.client.id}")
    private String clientId; //클라이언트ID

    @Value("${paypal.client.secret}")
    private String clientSecret; //비밀키

    /**
     *Set up the PayPal Java SDK environment with PayPal access credentials.
     *This sample uses SandboxEnvironment. In production, use LiveEnvironment.
     */
    private PayPalEnvironment environment;
    /**
     *PayPal HTTP client instance with environment that has access
     *credentials context. Use to invoke PayPal APIs.
     */
    private PayPalHttpClient  paypalClient;

    private PayPalEnvironment getEnvironment(){
        if("Production".equals(applicationProperty.getEnvironment())){
            environment = new PayPalEnvironment.Live(clientId, clientSecret);
        }else{
            environment = new PayPalEnvironment.Sandbox(clientId, clientSecret);
        }
        return environment;
    }

    /**
     *Method to get client object
     *
     *@return PayPalHttpClient client
     */
    public PayPalHttpClient client() {
        paypalClient = new PayPalHttpClient(getEnvironment());
        return this.paypalClient;
    }

    /**
     * 클라이언트 ID취득
     * @return
     */
    public String getClientId(){
        return clientId;
    }
}
