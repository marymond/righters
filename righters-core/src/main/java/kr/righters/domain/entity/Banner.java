package kr.righters.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by yj.nam on 19.11.28..
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Banner implements Serializable{

    private static final long serialVersionUID = -8471039021014039324L;

    @Id
    private String id;
    @Column
    private String name;
    @Column
    private String imageUrl;
    @Column
    private String mobileImageUrl;

    @Enumerated(EnumType.STRING)
    private Site site;

    @Column(name = "\"order\"")
    private Integer order = 99;

    @Enumerated(EnumType.STRING)
    private Status status = Status.HIDE;

    @CreatedDate
    @Column(updatable = false,insertable = false)
    private LocalDateTime registrationDate;

    public enum Site {
        MAIN("메인화면");

        @Getter
        private String desc;

        private Site(String desc) {
            this.desc = desc;
        }
    }

    public enum Status {
        SHOW("표시","숨기기"),
        HIDE("숨김","표시하기"),
        DELETED("삭제","");

        @Getter
        private String desc;
        @Getter
        private String btn;

        private Status(String desc, String btn) {
            this.desc = desc;
            this.btn = btn;
        }
    }


}
