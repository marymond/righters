package kr.righters.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Cart implements Serializable{

    private static final long serialVersionUID = -5288842834740114001L;

    @Id
    private String id;
    @Column
    private String userId;

    @Column
    private String productId;

    @Column
    private String productOptionId;

    @Column
    private Integer quantity;

    @Enumerated(EnumType.STRING)
    private Status status = Status.BEFORE_PAYMENT;

    @CreatedDate
    @Column(updatable = false,insertable = false)
    private LocalDateTime registrationDate;

    @Transient
    private Product product;

    @Transient
    private ProductOption productOption;

    public enum Status {
        BEFORE_PAYMENT("결제전"),
        PAYED("결제완료");

        @Getter
        private String desc;

        private Status(String desc) {
            this.desc = desc;
        }
    }


}
