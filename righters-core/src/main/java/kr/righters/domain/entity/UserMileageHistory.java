package kr.righters.domain.entity;

import kr.righters.domain.converter.CurrencyConverter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class UserMileageHistory implements Serializable{

    private static final long serialVersionUID = -6003452134644038966L;

    @Id
    private String id;
    @Column
    private String userId;
    @Column
    private String reason;
    @Column
    private Long amount;
    @Column
    private Long beforeMileage;
    @Column
    private Long afterMileage;

    @CreatedDate
    @Column(updatable = false,insertable = false)
    private LocalDateTime registrationDate;

    public enum Type {
        DEPOSIT("Deposit"),
        WITHDRAW("Withdraw");

        @Getter
        private String desc;

        private Type(String desc) {
            this.desc = desc;
        }
    }
}
