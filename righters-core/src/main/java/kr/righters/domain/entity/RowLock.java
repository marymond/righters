package kr.righters.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by yj.nam on 19.11.28..
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class RowLock implements Serializable{

    private static final long serialVersionUID = -886936093544801618L;

    @Id
    private String id;
}
