package kr.righters.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class UserAddress implements Serializable{

    private static final long serialVersionUID = 4592184732769153147L;
    @Id
    private String id;
    @Column
    private String userId;
    @Column
    private String fullName;
    @Column
    private String address1;
    @Column
    private String address2;
    @Column
    private String city;
    @Column
    private String state;
    @Column
    private String zipCode;
    @Column
    private String country;
    @Column
    private String phoneNumber;
    @Column
    private String deliveryInstructions;

    @Enumerated(EnumType.STRING)
    private Flag defaultFlag;

    @CreatedDate
    @Column(updatable = false,insertable = false)
    private LocalDateTime registrationDate;

    public enum Flag {
        FALSE("설정안함"),
        TRUE("설정함");

        @Getter
        private String desc;

        private Flag(String desc) {
            this.desc = desc;
        }
    }

}
