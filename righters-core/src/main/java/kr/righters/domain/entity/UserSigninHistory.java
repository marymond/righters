package kr.righters.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by yj.nam on 2019. 12. 18..
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class UserSigninHistory implements Serializable {

    private static final long serialVersionUID = 7388953477363144993L;

    @Id
    private String	id;             // PK
    @Column
    private String userId;              // User 의 id
    @Column
    private String token;           // Signin Token
    @Enumerated(value = EnumType.STRING)
    @Column
    private TokenStatus tokenStatus = TokenStatus.SIGNIN;       // 사인인 토큰 상태
    @Column
    private String ip;              // 사인인한 아이피 주소
    @Column
    private String environment;     // 사인인한 사용자의 환경
    @Column(insertable = false, updatable = false)
    private Timestamp	registrationDate; // 가입일시


    @AllArgsConstructor
    @Getter
    public enum TokenStatus {
        SIGNIN("SIGNIN","Sign in Token"),SIGNOUT("SIGNOUT","Sign out Token"),DELETED("DELETED","Removed Token"),EXPIRED("EXPIRED","Expired Token");
        private String code;
        private String description;
    }
}

