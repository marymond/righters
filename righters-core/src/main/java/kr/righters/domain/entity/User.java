package kr.righters.domain.entity;

import kr.righters.domain.common.Flag;
import kr.righters.domain.converter.CurrencyConverter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class User implements Serializable{

    private static final long serialVersionUID = 1858843958249481569L;
    @Id
    private String id;
    @Column
    private String loginId;
    @Column
    private String email;
    @Column
    private String password;
    @Column
    private String fullName;
    @Column
    private String profileImageUrl;
    @Column
    private Long totalMileage;
    @Column
    private Long balanceMileage;
    @Column
    private Long donationMileage;
    @Column
    private String phoneNumber;
    @Column
    private String uid;
    @Enumerated(EnumType.STRING)
    private Flag selfRewardFlag = Flag.FALSE;

    @Enumerated(EnumType.STRING)
    private Status status = Status.USED;

    @CreatedDate
    @Column(updatable = false,insertable = false)
    private LocalDateTime registrationDate;

    @Transient
    private String passwordConfirm;

    public enum Status {
        UNUSED("사용안함"),
        USED("사용중");

        @Getter
        private String desc;

        private Status(String desc) {
            this.desc = desc;
        }
    }
}
