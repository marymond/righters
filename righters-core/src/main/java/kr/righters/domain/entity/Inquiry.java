package kr.righters.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Inquiry implements Serializable{


    private static final long serialVersionUID = -3677915427265526637L;

    @Id
    private String id;
    @Column
    private String fullName;
    @Column
    private String email;
    @Column
    private String userId;
    @Column
    private String title;
    @Column
    private String content;
    @Column
    private String answer;

    @Enumerated(EnumType.STRING)
    private Status status = Status.STAND_BY;

    @CreatedDate
    @Column(updatable = false,insertable = false)
    private LocalDateTime registrationDate;

    public enum Site {
        MAIN("메인화면");

        @Getter
        private String desc;

        private Site(String desc) {
            this.desc = desc;
        }
    }

    public enum Status {
        STAND_BY("대기"),
        REFUSAL("거절"),
        COMPLETE("완료");

        @Getter
        private String desc;

        private Status(String desc) {
            this.desc = desc;
        }
    }


}
