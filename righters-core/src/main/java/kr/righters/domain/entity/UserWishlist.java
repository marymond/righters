package kr.righters.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class UserWishlist implements Serializable{

    private static final long serialVersionUID = -5312695995558974842L;

    @Id
    private String id;
    @Column
    private String userId;
    @Column
    private String productId;

    @ManyToOne
    @JoinColumn(name = "productId", insertable=false, updatable = false)
    private Product product;

    @CreatedDate
    @Column(updatable = false,insertable = false)
    private LocalDateTime registrationDate;

}
