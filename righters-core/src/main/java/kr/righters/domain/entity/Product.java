package kr.righters.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Lists;
import kr.righters.domain.converter.ArrayConverter;
import kr.righters.domain.converter.CurrencyConverter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Product implements Serializable{

    private static final long serialVersionUID = 1647615146521237683L;

    @Id
    private String id;
    @Column
    private String name;

    @OneToOne
    private ProductCategory category;
    @OneToOne
    private ProductCategory subCategory;

    @Enumerated(EnumType.STRING)
    private DealTypeFilter dealType;
    @Enumerated(EnumType.STRING)
    private BrandFilter brand;

    @Column
    private Long price;

    @Column
    private String description;

    @Column
    private Long salePrice;

    @Column
    private Integer saleRate = 0;
    @Column
    private Integer shareRewardRate = 0;
    @Column
    private String mainImageUrl;

    @Convert(converter = ArrayConverter.class)
    private String[] detailImageUrls;

    @OneToOne
    private Shipping shipping;

    @Enumerated(EnumType.STRING)
    private Status status = Status.HIDE;

    @CreatedDate
    @Column(updatable = false,insertable = false)
    private LocalDateTime registrationDate;

    @OneToMany(mappedBy = "product")
    @JsonIgnore
    private List<ProductRelation> relationProducts = Lists.newArrayList();

    @OneToMany(mappedBy = "relationProduct")
    @JsonIgnore
    private List<ProductRelation> products = Lists.newArrayList();

    @OneToMany(mappedBy = "product")
    @JsonIgnore
    private List<ProductTab> productTabs = Lists.newArrayList();

    @OneToMany(mappedBy = "product")
    @JsonIgnore
    private List<ProductOption> productOptions = Lists.newArrayList();

    @Transient
    private boolean wishListFlag;

    @Transient
    private String shareUid;

    public enum Status {
        SHOW("표시","숨기기"),
        HIDE("숨김","표시하기"),
        DELETED("삭제","");

        @Getter
        private String desc;
        @Getter
        private String btn;

        private Status(String desc, String btn) {
            this.desc = desc;
            this.btn = btn;
        }
    }

    public enum DealTypeFilter {
        SPECIAL_SALE("Special Sale"),
        DEAL_OF_DAY("Deal of Day");

        @Getter
        private String desc;

        private DealTypeFilter(String desc) {
            this.desc = desc;
        }
    }

    public enum BrandFilter {
        MARYMOND("Marymond"),
        SPIGEN("Spigen");

        @Getter
        private String desc;

        private BrandFilter(String desc) {
            this.desc = desc;
        }
    }

    public enum PriceFilter{
        UNDER$10("Under 10$", "-1,100"),
        $10TO$50("$10 to $50", "10,50"),
        $50TO$100("$50 to $100", "50,100"),
        OVER$100("Over $100", "100,-1");

        @Getter
        private String desc;
        @Getter
        private String filter;

        private PriceFilter(String desc, String filter) {
            this.desc = desc;
            this.filter = filter;
        }
    }


}
