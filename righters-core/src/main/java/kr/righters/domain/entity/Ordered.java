package kr.righters.domain.entity;

import com.google.common.collect.Lists;
import kr.righters.domain.converter.CurrencyConverter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Ordered implements Serializable{

    private static final long serialVersionUID = -6844841943267211275L;

    @Id
    private String id;
    @Column
    private String userId;
    @Column
    private String orderNumber;

    @Enumerated(EnumType.STRING)
    private Status status = Status.BEFORE_PAYMENT;

    @Column
    private LocalDateTime orderDate;
    @Column
    private String transationId;
    @Column
    private String pgOrderId;
    @Column
    private String pgReceiptId;

    @Column
    private Long totalAmount;
    @Column
    private Long totalShippingFee;
    @Column
    private Long totalProductAmount;

    @Column
    private String buyerFullName;
    @Column
    private String buyerAddress1;
    @Column
    private String buyerAddress2;
    @Column
    private String buyerCity;
    @Column
    private String buyerState;
    @Column
    private String buyerZipCode;
    @Column
    private String buyerCountry;
    @Column
    private String buyerPhoneNumber;
    @Column
    private String buyerDeliveryInstructions;

    @Enumerated(EnumType.STRING)
    private PaymentType paymentType;
    @Column
    private String shareUserId;
    @Column
    private String shareUserFullName;
    @Column
    private Long shareUserRewardMileage;

    @Column
    private BuynowFlag buynowFlag;
    @Column
    private String shipStationId;
    @Column
    private LocalDateTime shippingEndDate;

    @Column
    private LocalDateTime cancelDate;

    @CreatedDate
    @Column(updatable = false,insertable = false)
    private LocalDateTime registrationDate;

    @OneToMany(mappedBy = "ordered")
    private List<OrderedProduct> orderedProducts = Lists.newArrayList();

    @Transient
    private Boolean updatableFlag = true;

    @Transient
    private User user;

    @Transient
    private User shareUser;

    public enum Status {
        BEFORE_PAYMENT("결제전"),
        FAIL_PAYMENT("결제실패"),
        SUCCESS_PAYMENT("결제완료"),
        REFUNDED("환불"),
        COMPLETE("완료");

        @Getter
        private String desc;

        private Status(String desc) {
            this.desc = desc;
        }
    }

    public enum PaymentType {
        PAYPAL("Papal"),
        STRIPE("Stripe"),
        IAMPORT("아이엠포트");

        @Getter
        private String desc;

        private PaymentType(String desc) {
            this.desc = desc;
        }
    }

    public enum BuynowFlag {
        FALSE("카트구매"),
        TRUE("직접구매");

        @Getter
        private String desc;

        private BuynowFlag(String desc) {
            this.desc = desc;
        }
    }


}
