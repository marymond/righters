package kr.righters.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class ProductCategory implements Serializable{

    private static final long serialVersionUID = -5622165957880355666L;

    @Id
    private String id;
    @Column
    private String name;

    @Column
    private String parentId;

    @Enumerated(EnumType.STRING)
    private Status status = Status.HIDE;

    @CreatedDate
    @Column(updatable = false,insertable = false)
    private LocalDateTime registrationDate;

    public enum Status {
        SHOW("표시","숨기기"),
        HIDE("숨김","표시하기"),
        DELETED("삭제","");

        @Getter
        private String desc;
        @Getter
        private String btn;

        private Status(String desc, String btn) {
            this.desc = desc;
            this.btn = btn;
        }
    }
}
