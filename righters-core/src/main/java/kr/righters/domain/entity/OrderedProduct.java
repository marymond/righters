package kr.righters.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import kr.righters.domain.converter.CurrencyConverter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class OrderedProduct implements Serializable{

    private static final long serialVersionUID = 7957628441237876976L;

    @Id
    private String id;
    @Column
    private String orderId;
    @Column
    private String productId;
    @Column
    private String productOptionId;
    @Column
    private String productName;
    @Column
    private String productMainImageUrl;
    @Column
    private String productOptionName;
    @Column
    private Long productPrice;
    @Column
    private Long productSalePrice;
    @Column
    private Integer productSaleRate;
    @Column
    private Integer productShareRewardRate;
    @Column
    private Integer quantity;
    @Column
    private String shippingId;
    @Column
    private String shippingName;

    @Column
    private Long shippingFee;
    @Column
    private String trackingNumber;

    @Enumerated(EnumType.STRING)
    private Status status;

    @Enumerated(EnumType.STRING)
    private ShippingStatus shippingStatus;

    @Column
    private LocalDateTime shippingEndDate;

    @Column
    private LocalDateTime cancleDate;

    @CreatedDate
    @Column(updatable = false,insertable = false)
    private LocalDateTime registrationDate;

    @ManyToOne
    @JoinColumn(name = "orderId", insertable=false, updatable = false)
    @JsonIgnore
    private Ordered ordered;

    public enum Status {
        FAIL_PAYMENT("결제실패"), //결제실패
        SUCCESS_PAYMENT("결제완료"), //결제완료
        REFUNDED("결제취소"), //환불(주문취소)
        RETURN_REQUEST("반품/교환 요청"), //반품요청
        RETURND("반품/교환 완료"); //반품완료

        @Getter
        private String desc;

        private Status(String desc) {
            this.desc = desc;
        }
    }

    public enum ShippingStatus {
        REVIEWING("배송대기중"), //검토중
        PREPARING_ORDER("배송준비"), //주문준비
        SHIPPED("배송중"), //배송중
        DELIVERED("배송완료"); //배송완료

        @Getter
        private String desc;

        private ShippingStatus(String desc) {
            this.desc = desc;
        }
    }
}
