package kr.righters.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by yj.nam on 19.10.29..
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Manager implements Serializable{

    private static final long serialVersionUID = 6488830922353659135L;

    @Id
    private String id;
    @Column
    private String loginId;
    @Column
    private String email;
    @Column
    private String name;
    @Column
    private String nickname;
    @Column
    private String password;

    @Enumerated(EnumType.STRING)
    private Status status = Status.USED;
    @Enumerated(EnumType.STRING)
    private Level level = Level.ROLE_ADMIN;
    @Column
    private String accessRights;
    @Column
    private String mobileNumber;

    @Transient
    private String newPassword;
    @Transient
    private String newPasswordRetry;

    @CreatedDate
    @Column(updatable = false,insertable = false)
    private LocalDateTime registrationDate;

    public enum Status {
        UNUSED("사용안함"),
        USED("사용중");

        @Getter
        private String desc;

        private Status(String desc) {
            this.desc = desc;
        }
    }

    public enum Level {
        ROLE_ADMIN("관리자");

        @Getter
        private String desc;

        private Level(String desc) {
            this.desc = desc;
        }
    }

}
