package kr.righters.domain.entity;

import kr.righters.domain.common.Flag;
import kr.righters.domain.converter.CurrencyConverter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Shipping implements Serializable{

    private static final long serialVersionUID = 1608113207160364094L;

    @Id
    private String id;
    @Column
    private String name;
    @Column
    private Long shippingFee;
    @Column
    private String email;
    @Column
    private String phoneNumber;

    @Column
    private String sweettrackerCode;

    @Enumerated(EnumType.STRING)
    private Status        status         = Status.HIDE;

    @Enumerated(EnumType.STRING)
    private Flag          shipStationFlag  = Flag.FALSE;

    @CreatedDate
    @Column(updatable = false,insertable = false)
    private LocalDateTime registrationDate;

    public enum Status {
        SHOW("표시","숨기기"),
        HIDE("숨김","표시하기"),
        DELETED("삭제","");

        @Getter
        private String desc;
        @Getter
        private String btn;

        private Status(String desc, String btn) {
            this.desc = desc;
            this.btn = btn;
        }
    }
}
