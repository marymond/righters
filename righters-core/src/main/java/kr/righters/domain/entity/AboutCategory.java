package kr.righters.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by yj.nam on 20.10.14..
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class AboutCategory implements Serializable{

    private static final long serialVersionUID = -5622165957880355666L;

    @Id
    private String id;
    @Column
    private String name;

    @Column
    private String parentId;

    @Enumerated(EnumType.ORDINAL)
    private ChildrenFlag childrenFlag = ChildrenFlag.FALSE;

    @Column
    private Integer sortNumber;

    @Enumerated(EnumType.STRING)
    private Type type;

    @Enumerated(EnumType.STRING)
    private Status status = Status.HIDE;

    @CreatedDate
    @Column(updatable = false,insertable = false)
    private LocalDateTime registrationDate;

    @OneToMany(mappedBy = "parentCategory")
    @JsonIgnore
    private List<AboutCategory> childrenCategories = Lists.newArrayList();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parentId", insertable=false, updatable = false)
    @JsonIgnore
    private AboutCategory parentCategory;

    public enum Type{
        THUMBNAIL("썸네일"),
        IMAGE_VIEW("이미지뷰"),
        BULLETIN_BOARD("게시판");

        @Getter
        private String desc;

        private Type(String desc){
            this.desc = desc;
        }
    }
    public enum Status {
        SHOW("표시","숨기기"),
        HIDE("숨김","표시하기"),
        DELETED("삭제","");

        @Getter
        private String desc;
        @Getter
        private String btn;

        private Status(String desc, String btn) {
            this.desc = desc;
            this.btn = btn;
        }
    }

    public enum ChildrenFlag{
        FALSE("없음"),
        TRUE("있음");

        @Getter
        private String desc;

        private ChildrenFlag(String desc) {
            this.desc = desc;
        }
    }
}
