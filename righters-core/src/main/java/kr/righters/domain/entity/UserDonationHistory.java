package kr.righters.domain.entity;

import kr.righters.domain.converter.CurrencyConverter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class UserDonationHistory implements Serializable{

    private static final long serialVersionUID = -6003452134644038966L;

    @Id
    private String id;
    @Column
    private String userId;
    @Column
    private String donationId;
    @Column
    private Long amount;

    @CreatedDate
    @Column(updatable = false,insertable = false)
    private LocalDateTime registrationDate;

    @OneToOne
    @JoinColumn(name = "donationId", insertable=false, updatable = false)
    private Donation donation;

    @OneToOne
    @JoinColumn(name = "userId", insertable=false, updatable = false)
    private User user;
}
