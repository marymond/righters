package kr.righters.domain.converter;


import org.apache.commons.lang3.StringUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by yj.nam on 2020. 1. 8..
 */
@Converter
public class ArrayConverter implements AttributeConverter<String[], String> {

    @Override
    public String convertToDatabaseColumn(String[] attribute) {
        if (attribute == null || attribute.length == 0) {
            return null;
        }
        return StringUtils.join(attribute, "|");
    }

    @Override
    public String[] convertToEntityAttribute(String column) {
        if (StringUtils.isEmpty(column)) {
            return new String[0];
        }

        return column.split("\\|");
    }
}
