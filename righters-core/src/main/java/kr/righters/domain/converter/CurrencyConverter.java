package kr.righters.domain.converter;


import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.math.BigDecimal;

/**
 * Created by yj.nam on 2020. 1. 8..
 */
@Converter
public class CurrencyConverter implements AttributeConverter<String, Long> {

    @Override
    public Long convertToDatabaseColumn(String attribute) {
        if(attribute == null){
            return 0L;
        }
        //Cent단위로 저장
        BigDecimal value = new BigDecimal(attribute);
        BigDecimal column = value.multiply(new BigDecimal(100));

        return column.longValue();
    }

    @Override
    public String convertToEntityAttribute(Long column) {
        //달러 스트링으로 반환
        if(column == null){
            return "0";
        }
        BigDecimal value = new BigDecimal(column);

        BigDecimal dollarBig = value.divide(new BigDecimal(100),2,BigDecimal.ROUND_DOWN);

        return dollarBig.toPlainString();
    }
}
