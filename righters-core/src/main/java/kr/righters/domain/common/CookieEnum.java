package kr.righters.domain.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by yj.nam on 19.10.29..
 */
@AllArgsConstructor
@Getter
public enum CookieEnum {

    MANAGER_TOKEN("MTK",60*60*24,"관리자 토큰")
    ,USER_TOKEN("UTK",60*60,"사용자 토큰")
    ,SHARE_PRODUT("",60*60, "공유제품");

    private String cookieName;
    private int expiration;
    private String description;

}
