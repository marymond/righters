package kr.righters.domain.common;

import lombok.Getter;

public enum Flag {
	FALSE("미적용"),
	TRUE("적용");

	@Getter
	private String desc;

	private Flag(String desc) {
		this.desc = desc;
	}
}
