package kr.righters.domain.common;

import kr.righters.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by yj.nam 2019.12.05..
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserToken implements Serializable {

    private static final long serialVersionUID = 2988879461045632744L;

    private String    token;
    private User      user;
    private Timestamp createdAt;

}
