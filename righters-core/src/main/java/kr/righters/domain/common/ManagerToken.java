package kr.righters.domain.common;

import kr.righters.domain.entity.Manager;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by yj.nam on 19.10.29..
 */
@Data
public class ManagerToken implements Serializable {

    private String token;
    private Manager manager;

}
