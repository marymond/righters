package kr.righters.domain.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by yj.nam on 19.10.29..
 */
@AllArgsConstructor
@Getter
public enum StatusEnum {

    SUCCESS("SUCCESS"),
    AUTHENTICATION_FAILED("AUTHENTICATION_FAILED"),
    REQUEST_REQUIRE_PARAMS("REQUEST_REQUIRE_PARAMS"),
    FAILED("FAILED"),
    ERROR("ERROR");


    private String description;

}
