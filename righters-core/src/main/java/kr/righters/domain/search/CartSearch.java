package kr.righters.domain.search;

import kr.righters.domain.entity.Cart;
import kr.righters.domain.entity.QCart;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
public class CartSearch {
    private String id;
    private String      userId; //로그인시 사용
    private Cart.Status status;
    private String productId;
    private String productOptionId;

    public Predicate precidate() {
        QCart          qCart = QCart.cart;
        BooleanBuilder builder = new BooleanBuilder();

        if (StringUtils.isNotBlank(id)) {
            builder.and(qCart.id.eq(this.id));
        }

        if (StringUtils.isNotBlank(userId)) {
            builder.and(qCart.userId.eq(this.userId));
        }

        if (status != null) {
            builder.and(qCart.status.eq(this.status));
        }

        if (StringUtils.isNotBlank(productId)) {
            builder.and(qCart.productId.eq(this.productId));
        }

        if (StringUtils.isNotBlank(productOptionId)) {
            builder.and(qCart.productOptionId.eq(this.productOptionId));
        }

        return builder.getValue();
    }

}