package kr.righters.domain.search;
import kr.righters.domain.entity.QManager;
import org.apache.commons.lang3.StringUtils;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;

import lombok.Data;

/**
 * Created by yj.nam on 19.10.29..
 */
@Data
public class ManagerSearch {
    private String loginId;
    private String password; //로그인시 사용
    private String email;
    private String name;
    private String nickname;
    private String mobileNumber;

    public Predicate precidate() {
        QManager       qManager = QManager.manager;
        BooleanBuilder builder  = new BooleanBuilder();

        if (StringUtils.isNotBlank(loginId)) {
            builder.and(qManager.loginId.like("%"+this.loginId+"%"));
        }

        if (StringUtils.isNotBlank(email)) {
            builder.and(qManager.email.eq(this.email));
        }

        if (StringUtils.isNotBlank(name)) {
            builder.and(qManager.name.like("%"+this.name+"%"));
        }

        if (StringUtils.isNotBlank(nickname)) {
            builder.and(qManager.nickname.like("%"+this.nickname+"%"));
        }

        if (StringUtils.isNotBlank(mobileNumber)) {
            builder.and(qManager.mobileNumber.eq(this.mobileNumber));
        }

        return builder.getValue();
    }

}