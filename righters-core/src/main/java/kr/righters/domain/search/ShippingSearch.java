package kr.righters.domain.search;

import kr.righters.domain.entity.QShipping;
import kr.righters.domain.entity.Shipping;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
public class ShippingSearch {
    private String     id;
    private Shipping.Status status;
    private Shipping.Status notStatus;

    public Predicate precidate() {
        QShipping      qShipping = QShipping.shipping;
        BooleanBuilder builder     = new BooleanBuilder();

        if (StringUtils.isNotBlank(id)) {
            builder.and(qShipping.id.eq(this.id));
        }

        if(status != null){
            builder.and(qShipping.status.eq(this.status));
        }

        if(notStatus != null){
            builder.and(qShipping.status.ne(this.notStatus));
        }

        return builder.getValue();
    }

}