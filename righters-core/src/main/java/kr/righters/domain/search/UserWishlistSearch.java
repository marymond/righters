package kr.righters.domain.search;

import kr.righters.domain.entity.QUserWishlist;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
public class UserWishlistSearch {
    private String     id;
    private String userId;
    private String productId;

    public Predicate precidate() {
        QUserWishlist  qUserWishlist   = QUserWishlist.userWishlist;
        BooleanBuilder builder = new BooleanBuilder();

        if (StringUtils.isNotBlank(id)) {
            builder.and(qUserWishlist.id.eq(this.id));
        }

        if (StringUtils.isNotBlank(userId)) {
            builder.and(qUserWishlist.userId.eq(this.userId));
        }

        if (StringUtils.isNotBlank(productId)) {
            builder.and(qUserWishlist.productId.eq(this.productId));
        }



        return builder.getValue();
    }

}