package kr.righters.domain.search;

import kr.righters.domain.entity.QOrderedProduct;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
public class OrderedProductSearch {
    private String     id;

    public Predicate precidate() {
        QOrderedProduct  qOrderedProduct  = QOrderedProduct.orderedProduct;
        BooleanBuilder builder = new BooleanBuilder();

        if (StringUtils.isNotBlank(id)) {
            builder.and(qOrderedProduct.id.eq(this.id));
        }


        return builder.getValue();
    }

}