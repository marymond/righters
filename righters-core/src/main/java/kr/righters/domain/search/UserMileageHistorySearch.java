package kr.righters.domain.search;

import kr.righters.domain.entity.QUserMileageHistory;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
public class UserMileageHistorySearch {
    private String     id;

    public Predicate precidate() {
        QUserMileageHistory qUserMileageHistory   = QUserMileageHistory.userMileageHistory;
        BooleanBuilder      builder = new BooleanBuilder();

        if (StringUtils.isNotBlank(id)) {
            builder.and(qUserMileageHistory.id.eq(this.id));
        }


        return builder.getValue();
    }

}