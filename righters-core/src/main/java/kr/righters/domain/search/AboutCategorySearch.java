package kr.righters.domain.search;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import kr.righters.domain.entity.AboutCategory;
import kr.righters.domain.entity.QAboutCategory;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by yj.nam on 20.10.14..
 */
@Data
public class AboutCategorySearch {
    private String     id;
    private String                name;
    private AboutCategory.Status  status;
    private AboutCategory.Status neStatus;
    private String parentId;
    private boolean rootFlag;
    private AboutCategory.ChildrenFlag childrenFlag;


    public Predicate precidate() {
        QAboutCategory qAboutCategory = QAboutCategory.aboutCategory;
        BooleanBuilder builder          = new BooleanBuilder();

        if (StringUtils.isNotBlank(id)) {
            builder.and(qAboutCategory.id.eq(this.id));
        }

        if (StringUtils.isNotBlank(name)) {
            builder.and(qAboutCategory.name.like("%"+this.name+"%"));
        }

        if (status != null) {
            builder.and(qAboutCategory.status.eq(this.status));
        }

        if (neStatus != null) {
            builder.and(qAboutCategory.status.ne(this.neStatus));
        }

        if (StringUtils.isNotBlank(parentId)) {
            builder.and(qAboutCategory.parentId.eq(this.parentId));
        }

        if(rootFlag){
            builder.and(qAboutCategory.parentId.isNull());
        }

        if (childrenFlag != null) {
            builder.and(qAboutCategory.childrenFlag.eq(this.childrenFlag));
        }
        return builder.getValue();
    }

}