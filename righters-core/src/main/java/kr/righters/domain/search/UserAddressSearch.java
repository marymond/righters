package kr.righters.domain.search;

import kr.righters.domain.entity.QUserAddress;
import kr.righters.domain.entity.UserAddress;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
public class UserAddressSearch {
    private String     id;
    private String     userId;
    private UserAddress.Flag defaultFlag;

    public Predicate precidate() {
        QUserAddress   qUserAddress   = QUserAddress.userAddress;
        BooleanBuilder builder = new BooleanBuilder();

        if (StringUtils.isNotBlank(id)) {
            builder.and(qUserAddress.id.eq(this.id));
        }

        if(StringUtils.isNotBlank(userId)){
            builder.and(qUserAddress.userId.eq(this.userId));
        }

        if(defaultFlag != null){
            builder.and(qUserAddress.defaultFlag.eq(this.defaultFlag));
        }

        return builder.getValue();
    }

}