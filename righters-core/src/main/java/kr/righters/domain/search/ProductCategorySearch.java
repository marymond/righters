package kr.righters.domain.search;

import kr.righters.domain.entity.ProductCategory;
import kr.righters.domain.entity.QProduct;
import kr.righters.domain.entity.QProductCategory;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
public class ProductCategorySearch {
    private String     id;
    private String     name;
    private ProductCategory.Status status;
    private ProductCategory.Status neStatus;
    private String parentId;


    public Predicate precidate() {
        QProductCategory qProductCategory = QProductCategory.productCategory;
        BooleanBuilder   builder  = new BooleanBuilder();

        if (StringUtils.isNotBlank(id)) {
            builder.and(qProductCategory.id.eq(this.id));
        }

        if (StringUtils.isNotBlank(name)) {
            builder.and(qProductCategory.name.like("%"+this.name+"%"));
        }

        if (status != null) {
            builder.and(qProductCategory.status.eq(this.status));
        }

        if (neStatus != null) {
            builder.and(qProductCategory.status.ne(this.neStatus));
        }

        if (StringUtils.isNotBlank(parentId)) {
            builder.and(qProductCategory.parentId.eq(this.parentId));
        }else{
            builder.and(qProductCategory.parentId.isNull());
        }

        return builder.getValue();
    }

}