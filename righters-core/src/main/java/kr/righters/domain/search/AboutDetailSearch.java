package kr.righters.domain.search;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import kr.righters.domain.entity.AboutDetail;
import kr.righters.domain.entity.QAboutDetail;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
public class AboutDetailSearch {
    private String     id;

    private String            aboutCategoryId;
    private AboutDetail.Status status;
    private AboutDetail.Status notStatus;

    public Predicate precidate() {
        QAboutDetail qAboutDetail = QAboutDetail.aboutDetail;
        BooleanBuilder     builder     = new BooleanBuilder();

        if (StringUtils.isNotBlank(id)) {
            builder.and(qAboutDetail.id.eq(this.id));
        }
        if (StringUtils.isNotBlank(aboutCategoryId)) {
            builder.and(qAboutDetail.aboutCategoryId.eq(this.aboutCategoryId));
        }
        if(status != null){
            builder.and(qAboutDetail.status.eq(this.status));
        }

        if(notStatus != null){
            builder.and(qAboutDetail.status.ne(this.notStatus));
        }

        return builder.getValue();
    }

}