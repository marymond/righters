package kr.righters.domain.search;

import kr.righters.domain.entity.QUserMileageHistory;
import kr.righters.domain.entity.QUserSigninHistory;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
public class UserSigninHistorySearch {
    private String     id;

    public Predicate precidate() {
        QUserSigninHistory qUserSigninHistory   = QUserSigninHistory.userSigninHistory;
        BooleanBuilder      builder = new BooleanBuilder();

        if (StringUtils.isNotBlank(id)) {
            builder.and(qUserSigninHistory.id.eq(this.id));
        }


        return builder.getValue();
    }

}