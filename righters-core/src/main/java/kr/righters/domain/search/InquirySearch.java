package kr.righters.domain.search;

import kr.righters.domain.entity.Inquiry;
import kr.righters.domain.entity.QInquiry;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
public class InquirySearch {
    private String     id;
    private String fullName;
    private String email;
    private String         title;
    private Inquiry.Status status;

    public Predicate precidate() {
        QInquiry       qInquiry    = QInquiry.inquiry;
        BooleanBuilder builder = new BooleanBuilder();

        if (StringUtils.isNotBlank(id)) {
            builder.and(qInquiry.id.eq(this.id));
        }

        if (StringUtils.isNotBlank(fullName)) {
            builder.and(qInquiry.fullName.like("%" + this.fullName + "%"));
        }

        if (StringUtils.isNotBlank(email)) {
            builder.and(qInquiry.email.eq(this.email));
        }

        if (StringUtils.isNotBlank(title)) {
            builder.and(qInquiry.title.like("%" + this.title + "%"));
        }

        if (status != null) {
            builder.and(qInquiry.status.eq(this.status));
        }

        return builder.getValue();
    }

}