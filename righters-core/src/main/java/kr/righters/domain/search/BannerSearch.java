package kr.righters.domain.search;

import kr.righters.domain.entity.Banner;
import kr.righters.domain.entity.QBanner;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
public class BannerSearch {
    private String id;
    private Banner.Site site; //로그인시 사용
    private Banner.Status status;
    private Banner.Status notStatus;

    public Predicate precidate() {
        QBanner        qBanner = QBanner.banner;
        BooleanBuilder builder  = new BooleanBuilder();

        if (StringUtils.isNotBlank(id)) {
            builder.and(qBanner.id.eq(this.id));
        }

        if (site != null) {
            builder.and(qBanner.site.eq(this.site));
        }

        if (status != null) {
            builder.and(qBanner.status.eq(this.status));
        }

        if (notStatus != null) {
            builder.and(qBanner.status.ne(this.notStatus));
        }

        return builder.getValue();
    }

}