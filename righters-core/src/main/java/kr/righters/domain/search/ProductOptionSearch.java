package kr.righters.domain.search;

import kr.righters.domain.entity.ProductOption;
import kr.righters.domain.entity.QProductOption;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
public class ProductOptionSearch {
    private String         id;
    private String productId;
    private ProductOption.Status       status;
    private ProductOption.Status notStatus;

    public Predicate precidate() {
        QProductOption qProductOption = QProductOption.productOption;
        BooleanBuilder builder  = new BooleanBuilder();

        if (StringUtils.isNotBlank(id)) {
            builder.and(qProductOption.id.eq(this.id));
        }
        if (StringUtils.isNotBlank(productId)) {
            builder.and(qProductOption.productId.eq(this.productId));
        }
        if(status != null){
            builder.and(qProductOption.status.eq(this.status));
        }

        if(notStatus != null){
            builder.and(qProductOption.status.ne(this.notStatus));
        }

        return builder.getValue();
    }

}