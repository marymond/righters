package kr.righters.domain.search;

import kr.righters.domain.entity.Donation;
import kr.righters.domain.entity.QDonation;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
public class DonationSearch {
    private String          id;
    private Donation.Status status;
    private Donation.Status  notStatus;

    public Predicate precidate() {
        QDonation      qDonation = QDonation.donation;
        BooleanBuilder builder = new BooleanBuilder();

        if (StringUtils.isNotBlank(id)) {
            builder.and(qDonation.id.eq(this.id));
        }

        if (status != null) {
            builder.and(qDonation.status.eq(this.status));
        }

        if(notStatus != null){
            builder.and(qDonation.status.ne(this.notStatus));
        }

        return builder.getValue();
    }

}