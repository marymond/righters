package kr.righters.domain.search;

import kr.righters.domain.entity.Faq;
import kr.righters.domain.entity.QFaq;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
public class FaqSearch {
    private String     id;
    private String title;
    private Faq.Status status;

    public Predicate precidate() {
        QFaq qFaq = QFaq.faq;
        BooleanBuilder builder = new BooleanBuilder();

        if (StringUtils.isNotBlank(id)) {
            builder.and(qFaq.id.eq(this.id));
        }

        if (StringUtils.isNotBlank(title)) {
            builder.and(qFaq.title.like("%" + this.title + "%"));
        }

        if (status != null) {
            builder.and(qFaq.status.eq(this.status));
        }

        return builder.getValue();
    }

}