package kr.righters.domain.search;

import kr.righters.domain.entity.Ordered;
import kr.righters.domain.entity.QOrdered;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
public class OrderedSearch {
    private String     id;
    private String       userId;
    private String         shareUserId;
    private Ordered.Status status;
    private String         buyerFullName;
    private String buyerPhoneNumber;

    private String pgOrderId;
    private String pgReceiptId;

    private List<Ordered.Status> statuses;

    public Predicate precidate() {
        QOrdered       qOrdered  = QOrdered.ordered;
        BooleanBuilder builder = new BooleanBuilder();

        if (StringUtils.isNotBlank(id)) {
            builder.and(qOrdered.id.eq(this.id));
        }

        if (StringUtils.isNotBlank(userId)) {
            builder.and(qOrdered.userId.eq(userId));
        }

        if (StringUtils.isNotBlank(buyerFullName)) {
            builder.and(qOrdered.buyerFullName.like("%" + this.buyerFullName + "%"));
        }

        if (StringUtils.isNotBlank(buyerPhoneNumber)) {
            builder.and(qOrdered.buyerPhoneNumber.eq(buyerPhoneNumber));
        }

        if (StringUtils.isNotBlank(shareUserId)) {
            builder.and(qOrdered.shareUserId.eq(shareUserId));
        }

        if (status != null) {
            builder.and(qOrdered.status.eq(this.status));
        }

        if (StringUtils.isNotBlank(pgOrderId)) {
            builder.and(qOrdered.pgOrderId.eq(pgOrderId));
        }

        if (StringUtils.isNotBlank(pgReceiptId)) {
            builder.and(qOrdered.pgReceiptId.eq(pgReceiptId));
        }

        if(statuses != null){
            builder.and(qOrdered.status.in(statuses));
        }

        return builder.getValue();
    }

}