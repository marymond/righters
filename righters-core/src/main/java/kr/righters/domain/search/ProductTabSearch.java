package kr.righters.domain.search;

import kr.righters.domain.entity.ProductTab;
import kr.righters.domain.entity.QProductTab;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
public class ProductTabSearch {
    private String     id;

    private String            productId;
    private ProductTab.Status status;
    private ProductTab.Status notStatus;

    public Predicate precidate() {
        QProductTab qProductTab = QProductTab.productTab;
        BooleanBuilder     builder     = new BooleanBuilder();

        if (StringUtils.isNotBlank(id)) {
            builder.and(qProductTab.id.eq(this.id));
        }
        if (StringUtils.isNotBlank(productId)) {
            builder.and(qProductTab.productId.eq(this.productId));
        }
        if(status != null){
            builder.and(qProductTab.status.eq(this.status));
        }

        if(notStatus != null){
            builder.and(qProductTab.status.ne(this.notStatus));
        }

        return builder.getValue();
    }

}