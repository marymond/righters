package kr.righters.domain.search;

import kr.righters.domain.entity.Notice;
import kr.righters.domain.entity.QNotice;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
public class NoticeSearch {
    private String     id;
    private String        title;
    private Notice.Status status;

    public Predicate precidate() {
        QNotice        qNotice    = QNotice.notice;
        BooleanBuilder builder = new BooleanBuilder();

        if (StringUtils.isNotBlank(id)) {
            builder.and(qNotice.id.eq(this.id));
        }

        if (StringUtils.isNotBlank(title)) {
            builder.and(qNotice.title.like("%" + this.title + "%"));
        }

        if (status != null) {
            builder.and(qNotice.status.eq(this.status));
        }

        return builder.getValue();
    }

}