package kr.righters.domain.search;

import kr.righters.domain.entity.QUserDonationHistory;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
public class UserDonationHistorySearch {
    private String     id;
    private String userId;
    private String donationId;

    public Predicate precidate() {
        QUserDonationHistory qUserDonationHistory   = QUserDonationHistory.userDonationHistory;
        BooleanBuilder       builder = new BooleanBuilder();

        if (StringUtils.isNotBlank(id)) {
            builder.and(qUserDonationHistory.id.eq(this.id));
        }
        if (StringUtils.isNotBlank(userId)) {
            builder.and(qUserDonationHistory.userId.eq(this.userId));
        }

        if (StringUtils.isNotBlank(donationId)) {
            builder.and(qUserDonationHistory.donationId.eq(this.donationId));
        }

        return builder.getValue();
    }

}