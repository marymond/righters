package kr.righters.domain.search;

import kr.righters.domain.entity.ProductRelation;
import kr.righters.domain.entity.QProductRelation;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
public class ProductRelationSearch {
    private String     id;
    private String                 productId;
    private ProductRelation.Status status;
    private ProductRelation.Status      notStatus;

    public Predicate precidate() {
        QProductRelation qProductRelation = QProductRelation.productRelation;
        BooleanBuilder   builder  = new BooleanBuilder();

        if (StringUtils.isNotBlank(id)) {
            builder.and(qProductRelation.id.eq(this.id));
        }
        if (StringUtils.isNotBlank(productId)) {
            builder.and(qProductRelation.productId.eq(this.productId));
        }
        if(status != null){
            builder.and(qProductRelation.status.eq(this.status));
        }

        if(notStatus != null){
            builder.and(qProductRelation.status.ne(this.notStatus));
        }


        return builder.getValue();
    }

}