package kr.righters.domain.search;

import kr.righters.domain.entity.QUser;
import kr.righters.domain.entity.User;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
public class UserSearch {
    private String     id;
    private String loginId;
    private String fullName;
    private String phoneNumber;
    private String uid;
    private User.Status notStatus;
    private User.Status status;

    private List<String> userIds;


    public Predicate precidate() {
        QUser          qUser = QUser.user;
        BooleanBuilder builder   = new BooleanBuilder();

        if (StringUtils.isNotBlank(id)) {
            builder.and(qUser.id.eq(this.id));
        }

        if (StringUtils.isNotBlank(loginId)) {
            builder.and(qUser.loginId.eq(this.loginId));
        }

        if (StringUtils.isNotBlank(fullName)) {
            builder.and(qUser.fullName.eq(this.fullName));
        }

        if (StringUtils.isNotBlank(phoneNumber)) {
            builder.and(qUser.phoneNumber.eq(this.phoneNumber));
        }

        if (StringUtils.isNotBlank(uid)) {
            builder.and(qUser.uid.eq(this.uid));
        }

        if(notStatus != null){
            builder.and(qUser.status.ne(this.notStatus));
        }

        if(status != null){
            builder.and(qUser.status.eq(this.status));
        }

        if(userIds != null){
            builder.and(qUser.id.in(this.userIds));
        }

        return builder.getValue();
    }

}