package kr.righters.domain.search;

import kr.righters.domain.entity.Product;
import kr.righters.domain.entity.ProductCategory;
import kr.righters.domain.entity.QProduct;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by yj.nam on 19.11.29..
 */
@Data
public class ProductSearch {
    private String     id;
    private Product.Status status;
    private Product.Status notStatus;

    private ProductCategory category;
    private ProductCategory subCategory;
    private String keyword;
    private Product.DealTypeFilter dealTypeFilter;
    private Product.BrandFilter brandFilter;
    private Product.PriceFilter priceFilter;

    public Predicate precidate() {
        QProduct       qProduct = QProduct.product;
        BooleanBuilder builder       = new BooleanBuilder();

        if (StringUtils.isNotBlank(id)) {
            builder.and(qProduct.id.eq(this.id));
        }

        if(status != null){
            builder.and(qProduct.status.eq(this.status));
        }

        if(notStatus != null){
            builder.and(qProduct.status.ne(this.notStatus));
        }

        if(category != null){
            builder.and(qProduct.category.eq(this.category));
        }
        if(subCategory != null){
            builder.and(qProduct.subCategory.eq(this.subCategory));
        }

        if(dealTypeFilter != null){
            builder.and(qProduct.dealType.eq(this.dealTypeFilter));
        }
        if(brandFilter != null){
            builder.and(qProduct.brand.eq(this.brandFilter));
        }
        if(priceFilter != null){
            String cond = priceFilter.getFilter();
            String[] conds = cond.split(",");

            if(!"-1".equals(conds[0])){
                builder.and(qProduct.salePrice.goe(Long.valueOf(conds[0])));
            }
            if(!"-1".equals(conds[1])){
                builder.and(qProduct.salePrice.lt(Long.valueOf(conds[1])));
            }
        }
        if(StringUtils.isNotBlank(keyword)){
            builder.and(qProduct.name.like("%"+ this.keyword +"%"));
        }



        return builder.getValue();
    }

}